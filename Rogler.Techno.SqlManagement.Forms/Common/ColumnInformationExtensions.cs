﻿using System.Collections.Generic;
using System.Linq;

namespace Rogler.Techno.SqlManagement.Forms.Common
{
    public static class ColumnInformationExtensions
    {
        public static string SelectStatement(this List<ColumnInformation> sender, string TableName)
        {
            if (sender.Count == 0)
            {
                return $"SELECT * FROM {TableName}";
            }
            else
            {
                return "SELECT " + string.Join(",", sender.Select(col => $"[{col.Name}]").ToArray()) + $" FROM {TableName}";
            }
        }
    }
}

