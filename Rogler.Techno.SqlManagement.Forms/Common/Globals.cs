﻿using Rogler.Techno.SqlManagement.Forms.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace Rogler.Techno.SqlManagement.Forms.Common
{
    public static class Globals
    {
        public static List<MergeFilesModel> ParseIndexFile(string inputFile)
        {
            try
            {
                using (var reader = new StreamReader(inputFile))
                {
                    XmlSerializer outpoutSerializer = new XmlSerializer(typeof(List<MergeFilesModel>));
                    var result = (List<MergeFilesModel>)outpoutSerializer.Deserialize(reader);
                    return result;
                }
            }
            catch
            {
                return new List<MergeFilesModel>();
            }
        }

        public static string GetScript(string sql, TSqlFragment fragment)
        {
            return sql.Substring(fragment.StartOffset, fragment.FragmentLength);
        }

        public static string ToTitleCaseFromPascal(string str)
        {
            string[] split =  Regex.Split(str, @"(?<!^)(?=[A-Z])");
            return string.Join("_", split).ToUpper();
        }

        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            var temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        public static string MakeUnique(string path)
        {
            string dir = Path.GetDirectoryName(path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            string fileExt = Path.GetExtension(path);

            for (int i = 1; ; ++i)
            {
                if (!File.Exists(path))
                    return path;

                path = Path.Combine(dir??"", fileName + " " + i + fileExt);
            }
        }

        public static string GetConnectionStringFriendlyName(string connectionString)
        {
            var builder = new SqlConnectionStringBuilder() { ConnectionString = connectionString };
            return builder.DataSource + "\\" + builder.InitialCatalog;

        }

        public static string ConvertToNewLine(string str)
        {
            return Regex.Replace(str, "(?<!\r)\n", Environment.NewLine);
        }

        public static Encoding GetFileEncoding(string filename)
        {
            using (FileStream fs = File.OpenRead(filename))
            {
                Ude.CharsetDetector cdet = new Ude.CharsetDetector();
                cdet.Feed(fs);
                cdet.DataEnd();
                if (cdet.Charset != null)
                {
                    switch (cdet.Charset)
                    {
                        case Ude.Charsets.UTF8: return Encoding.UTF8;
                        case Ude.Charsets.ASCII: return Encoding.ASCII;
                        case Ude.Charsets.UTF32_BE:
                        case Ude.Charsets.UTF32_LE:
                            return Encoding.UTF32;
                        default: return Encoding.UTF8;
                            
                    }
                }
                else
                {
                    return Encoding.Default;
                }
            }
        }
    }
}
