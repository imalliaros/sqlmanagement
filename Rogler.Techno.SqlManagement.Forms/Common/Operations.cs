﻿using Microsoft.Data.ConnectionUI;
using Rogler.Techno.SqlManagement.Forms.Models.Db;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Rogler.Techno.SqlManagement.Forms.Properties;
using Syncfusion.Data.Extensions;
using Exception = System.Exception;
using View = Microsoft.SqlServer.Management.Smo.View;

namespace Rogler.Techno.SqlManagement.Forms.Common
{
    public class Operations
    {
        public string ServerName { get; set; }
        public string InitialCatalog { get; set; }
        public string ConnectionString { get; set; }

        public List<DbTable> Tables { get; set; } = new List<DbTable>();
        public List<DbTable> Views { get; set; } = new List<DbTable>();
        public List<DbTrigger> Triggers { get; set; } = new List<DbTrigger>();
        public List<DbRoutine> StoredProcedures { get; set; } = new List<DbRoutine>();
        public List<DbRoutine> Functions { get; set; } = new List<DbRoutine>();


        public Operations()
        {

        }

        public Operations(string connectionString)
        {
            ConnectionString = connectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder() { ConnectionString = ConnectionString };
            ServerName = builder.DataSource;
            InitialCatalog = builder.InitialCatalog;
        }

        public bool GetConnection()
        {
            try
            {
                DataConnectionDialog dcd = new DataConnectionDialog();
                LoadConfiguration(dcd);
                dcd.SelectedDataSource = DataSource.SqlDataSource;
                dcd.SelectedDataProvider = DataProvider.SqlDataProvider;

                if (DataConnectionDialog.Show(dcd) == DialogResult.OK)
                {
                    ConnectionString = dcd.ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder() { ConnectionString = ConnectionString };
                    ServerName = builder.DataSource;
                    InitialCatalog = builder.InitialCatalog;
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }

        public void LoadSchema()
        {

            DbProviderFactory factory = DbProviderFactories.GetFactory(DataProvider.SqlDataProvider.Name);
            using (DbConnection connection = factory.CreateConnection())
            {
                if (connection != null)
                {
                    connection.ConnectionString = ConnectionString;
                    connection.Open();

                    DbCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;

                    cmd.CommandText = @"SELECT t.TABLE_NAME as [Name], t.TABLE_SCHEMA as [Schema], t.TABLE_TYPE as TableType FROM INFORMATION_SCHEMA.TABLES t ";
                    DataTable tblTable = new DataTable();
                    tblTable.Load(cmd.ExecuteReader());
                    Tables = tblTable.AsEnumerable().Where(t => t.Field<string>("TableType") == "BASE TABLE").Select(t => new DbTable(t)).ToList();
                    Views = tblTable.AsEnumerable().Where(t => t.Field<string>("TableType") == "VIEW").Select(t => new DbTable(t)).ToList();

                    cmd.CommandText =
                        @"select c.TABLE_SCHEMA as TableSchema, c.TABLE_NAME as TableName, c.COLUMN_NAME as ColumnName, c.DATA_TYPE as DataType, c.IS_NULLABLE as IsNullable, 
                                c.CHARACTER_MAXIMUM_LENGTH as CharacterMaximumLength, c.NUMERIC_PRECISION as NumericPrecision, c.DATETIME_PRECISION as DateTimePrecision, 
                                c.ORDINAL_POSITION as OrdinalPosition 
                                from INFORMATION_SCHEMA.COLUMNS c";
                    DataTable columnTable = new DataTable();
                    columnTable.Load(cmd.ExecuteReader());

                    foreach (DbTable table in Tables)
                    {
                        table.Columns = columnTable.AsEnumerable().Where(c => c.Field<string>("TableName") == table.Name).Select(c => new DbColumn(c)).OrderBy(c => c.OrdinalPosition).ToList();
                    }

                    foreach (DbTable view in Views)
                    {
                        view.Columns = columnTable.AsEnumerable().Where(c => c.Field<string>("TableName") == view.Name).Select(c => new DbColumn(c)).OrderBy(c => c.OrdinalPosition).ToList();
                    }


                    cmd.CommandText = @"
                            SELECT TableName = so2.name, TriggerName = so.name, TriggerText	= sc.text, CreatedDate = so.crdate, AfterAction = e.type_desc 
                            FROM sysobjects so
                            inner JOIN sysobjects so2 ON so.parent_obj = so2.id
                            inner JOIN syscomments sc ON sc.id = so.id
                            inner join sys.events e on e.object_id = so.id
                            WHERE so.type = 'tr'";
                    DataTable triggerTable = new DataTable();
                    triggerTable.Load(cmd.ExecuteReader());
                    Triggers = triggerTable.AsEnumerable().Select(t => new DbTrigger(t)).ToList();

                    cmd.CommandText = @"select r.ROUTINE_SCHEMA as [Schema], r.ROUTINE_NAME as [Name], r.ROUTINE_DEFINITION as ProcedureText, r.CREATED as CreatedDate, r.ROUTINE_TYPE as RoutineType from information_schema.routines  r";
                    var spTable = new DataTable();
                    spTable.Load(cmd.ExecuteReader());
                    StoredProcedures = spTable.AsEnumerable().Where(t => t.Field<string>("RoutineType") == "PROCEDURE").Select(t => new DbRoutine(t)).ToList();
                    Functions = spTable.AsEnumerable().Where(t => t.Field<string>("RoutineType") == "FUNCTION").Select(t => new DbRoutine(t)).ToList();

                }
            }
        }


        public void LoadConfiguration(DataConnectionDialog dialog)
        {
            dialog.DataSources.Add(DataSource.SqlDataSource);
            dialog.DataSources.Add(DataSource.SqlFileDataSource);
            dialog.DataSources.Add(DataSource.OracleDataSource);
            dialog.DataSources.Add(DataSource.AccessDataSource);
            dialog.DataSources.Add(DataSource.OdbcDataSource);

            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.SqlDataProvider);
            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OracleDataProvider);
            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OleDBDataProvider);
            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OdbcDataProvider);
            dialog.DataSources.Add(dialog.UnspecifiedDataSource);
        }

        public override string ToString() => !string.IsNullOrEmpty(ServerName) ? ServerName + "\\" + InitialCatalog : string.Empty;
    }
}

