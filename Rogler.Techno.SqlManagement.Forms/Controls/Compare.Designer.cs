﻿namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    partial class Compare
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeNodeAdvStyleInfo treeNodeAdvStyleInfo1 = new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeNodeAdvStyleInfo();
            Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeNodeAdvSubItemStyleInfo treeNodeAdvSubItemStyleInfo1 = new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeNodeAdvSubItemStyleInfo();
            Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdvStyleInfo treeColumnAdvStyleInfo1 = new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdvStyleInfo();
            Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdv treeColumnAdv1 = new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdv();
            Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdv treeColumnAdv2 = new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdv();
            Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdv treeColumnAdv3 = new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdv();
            this.TreeSplitContainer = new System.Windows.Forms.SplitContainer();
            this.CompareMultiColumnTreeView = new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.MultiColumnTreeView();
            this.CodeSplitContainer = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.TreeSplitContainer)).BeginInit();
            this.TreeSplitContainer.Panel1.SuspendLayout();
            this.TreeSplitContainer.Panel2.SuspendLayout();
            this.TreeSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CompareMultiColumnTreeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeSplitContainer)).BeginInit();
            this.CodeSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // TreeSplitContainer
            // 
            this.TreeSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.TreeSplitContainer.Name = "TreeSplitContainer";
            this.TreeSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // TreeSplitContainer.Panel1
            // 
            this.TreeSplitContainer.Panel1.Controls.Add(this.CompareMultiColumnTreeView);
            // 
            // TreeSplitContainer.Panel2
            // 
            this.TreeSplitContainer.Panel2.Controls.Add(this.CodeSplitContainer);
            this.TreeSplitContainer.Size = new System.Drawing.Size(435, 237);
            this.TreeSplitContainer.SplitterDistance = 113;
            this.TreeSplitContainer.TabIndex = 0;
            // 
            // CompareMultiColumnTreeView
            // 
            this.CompareMultiColumnTreeView.AutoAdjustMultiLineHeight = true;
            this.CompareMultiColumnTreeView.BaseStylePairs.AddRange(new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.StyleNamePair[] {
            new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.StyleNamePair("Standard", treeNodeAdvStyleInfo1),
            new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.StyleNamePair("Standard - SubItem", treeNodeAdvSubItemStyleInfo1),
            new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.StyleNamePair("Standard - Column", treeColumnAdvStyleInfo1)});
            this.CompareMultiColumnTreeView.BeforeTouchSize = new System.Drawing.Size(435, 113);
            treeColumnAdv1.Highlighted = false;
            treeColumnAdv1.Text = "Object";
            treeColumnAdv2.Highlighted = false;
            treeColumnAdv2.Text = "Server";
            treeColumnAdv3.Highlighted = false;
            treeColumnAdv3.Text = "Script";
            this.CompareMultiColumnTreeView.Columns.AddRange(new Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.TreeColumnAdv[] {
            treeColumnAdv1,
            treeColumnAdv2,
            treeColumnAdv3});
            this.CompareMultiColumnTreeView.ColumnsHeaderBackground = new Syncfusion.Drawing.BrushInfo(System.Drawing.SystemColors.Control);
            this.CompareMultiColumnTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // 
            // 
            this.CompareMultiColumnTreeView.HelpTextControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CompareMultiColumnTreeView.HelpTextControl.Location = new System.Drawing.Point(0, 0);
            this.CompareMultiColumnTreeView.HelpTextControl.Name = "m_helpText";
            this.CompareMultiColumnTreeView.HelpTextControl.Size = new System.Drawing.Size(49, 15);
            this.CompareMultiColumnTreeView.HelpTextControl.TabIndex = 0;
            this.CompareMultiColumnTreeView.HelpTextControl.Text = "help text";
            this.CompareMultiColumnTreeView.HotTracking = true;
            this.CompareMultiColumnTreeView.Location = new System.Drawing.Point(0, 0);
            this.CompareMultiColumnTreeView.Name = "CompareMultiColumnTreeView";
            this.CompareMultiColumnTreeView.Size = new System.Drawing.Size(435, 113);
            this.CompareMultiColumnTreeView.TabIndex = 0;
            this.CompareMultiColumnTreeView.Text = "multiColumnTreeView1";
            // 
            // 
            // 
            this.CompareMultiColumnTreeView.ToolTipControl.BackColor = System.Drawing.SystemColors.Info;
            this.CompareMultiColumnTreeView.ToolTipControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CompareMultiColumnTreeView.ToolTipControl.Location = new System.Drawing.Point(0, 0);
            this.CompareMultiColumnTreeView.ToolTipControl.Name = "m_toolTip";
            this.CompareMultiColumnTreeView.ToolTipControl.Size = new System.Drawing.Size(41, 15);
            this.CompareMultiColumnTreeView.ToolTipControl.TabIndex = 1;
            this.CompareMultiColumnTreeView.ToolTipControl.Text = "toolTip";
            // 
            // CodeSplitContainer
            // 
            this.CodeSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CodeSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.CodeSplitContainer.Name = "CodeSplitContainer";
            this.CodeSplitContainer.Size = new System.Drawing.Size(435, 120);
            this.CodeSplitContainer.SplitterDistance = 145;
            this.CodeSplitContainer.TabIndex = 0;
            // 
            // Compare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TreeSplitContainer);
            this.Name = "Compare";
            this.Size = new System.Drawing.Size(435, 237);
            this.TreeSplitContainer.Panel1.ResumeLayout(false);
            this.TreeSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TreeSplitContainer)).EndInit();
            this.TreeSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CompareMultiColumnTreeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeSplitContainer)).EndInit();
            this.CodeSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer TreeSplitContainer;
        private Syncfusion.Windows.Forms.Tools.MultiColumnTreeView.MultiColumnTreeView CompareMultiColumnTreeView;
        private System.Windows.Forms.SplitContainer CodeSplitContainer;
    }
}
