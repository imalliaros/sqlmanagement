﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Dmf;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Extensions;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Models.Db;

namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    public partial class Compare : UserControl
    {
        private Dictionary<string, CompareModel> _model;
        public Compare(List<MergeFilesModel> files, Operations operations)
        {
            InitializeComponent();
            CreateCompareModel(files, operations);
        }

        void CreateCompareModel(List<MergeFilesModel> files, Operations operations)
        {
            _model = new Dictionary<string, CompareModel>();
            foreach (DbRoutine function in operations.Functions)
            {
                _model.Add(function.Name, new CompareModel
                {
                    Name = function.Name,
                    ServerExists = true,
                    ServerText = function.ProcedureText,
                    ObjectType = ObjectTypeEnum.Function
                });
            }
            foreach (DbRoutine sp in operations.StoredProcedures)
            {
                _model.Add(sp.Name, new CompareModel
                {
                    Name = sp.Name,
                    ServerExists = true,
                    ServerText = sp.ProcedureText,
                    ObjectType = ObjectTypeEnum.StoreProcedure
                });
            }
            foreach (DbTable tbl in operations.Tables)
            {
                _model.Add(tbl.Name, new CompareModel
                {
                    Name = tbl.Name,
                    ServerExists = true,                    
                    ObjectType = ObjectTypeEnum.Table,
                    ServerText = tbl.Script
                });
            }
            foreach (DbTable view in operations.Views)
            {
                _model.Add(view.Name, new CompareModel
                {
                    Name = view.Name,
                    ServerExists = true,                    
                    ObjectType = ObjectTypeEnum.View,
                    ServerText = view.Script
                });
            }
            foreach (DbTrigger trigger in operations.Triggers)
            {
                _model.Add(trigger.TriggerName, new CompareModel
                {
                    Name = trigger.TriggerName,
                    ServerExists = true,                    
                    ObjectType = ObjectTypeEnum.View,
                    ServerText = trigger.TriggerText
                });
            }

            foreach (MergeFilesModel file in files)
            {
                CompareModel model = null;
                if (_model.ContainsKey(file.Name))
                {
                    model = _model[file.Name];

                }
                else
                {
                    model = new CompareModel
                    {
                        Name = file.Name,
                        ObjectType = file.NodeType.GetObjectType()
                    };
                }
            }


        }
    }
}
