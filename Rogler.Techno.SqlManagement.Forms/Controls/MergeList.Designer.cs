﻿namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    partial class MergeList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.WinForms.DataGrid.GridButtonColumn gridButtonColumn1 = new Syncfusion.WinForms.DataGrid.GridButtonColumn();
            Syncfusion.WinForms.DataGrid.GridButtonColumn gridButtonColumn2 = new Syncfusion.WinForms.DataGrid.GridButtonColumn();
            Syncfusion.WinForms.DataGrid.GridButtonColumn gridButtonColumn3 = new Syncfusion.WinForms.DataGrid.GridButtonColumn();
            Syncfusion.WinForms.DataGrid.GridButtonColumn gridButtonColumn4 = new Syncfusion.WinForms.DataGrid.GridButtonColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MergeList));
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn1 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn1 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn2 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn3 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn4 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            this.MergeDataGrid = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.MergeDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // MergeDataGrid
            // 
            this.MergeDataGrid.AccessibleName = "Table";
            this.MergeDataGrid.AllowEditing = false;
            this.MergeDataGrid.AllowFiltering = true;
            this.MergeDataGrid.AllowResizingColumns = true;
            this.MergeDataGrid.AutoGenerateColumns = false;
            this.MergeDataGrid.AutoGenerateColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoGenerateColumnsMode.None;
            this.MergeDataGrid.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCellsWithLastColumnFill;
            gridButtonColumn1.AllowDefaultButtonText = false;
            gridButtonColumn1.AllowEditing = false;
            gridButtonColumn1.AllowFiltering = true;
            gridButtonColumn1.AllowResizing = true;
            gridButtonColumn1.AllowSorting = false;
            gridButtonColumn1.ButtonSize = new System.Drawing.Size(0, 0);
            gridButtonColumn1.DefaultButtonText = "";
            gridButtonColumn1.HeaderText = "UpButton";
            gridButtonColumn1.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.up_16;
            gridButtonColumn1.ImageSize = new System.Drawing.Size(0, 0);
            gridButtonColumn1.MappingName = "UpButton";
            gridButtonColumn1.ShowFilterRowOptions = false;
            gridButtonColumn1.Visible = false;
            gridButtonColumn1.Width = 30D;
            gridButtonColumn2.AllowDefaultButtonText = true;
            gridButtonColumn2.AllowEditing = false;
            gridButtonColumn2.AllowFiltering = true;
            gridButtonColumn2.AllowResizing = true;
            gridButtonColumn2.AllowSorting = false;
            gridButtonColumn2.ButtonSize = new System.Drawing.Size(0, 0);
            gridButtonColumn2.DefaultButtonText = "";
            gridButtonColumn2.HeaderText = "DownButton";
            gridButtonColumn2.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.down_16;
            gridButtonColumn2.ImageSize = new System.Drawing.Size(0, 0);
            gridButtonColumn2.MappingName = "DownButton";
            gridButtonColumn2.ShowFilterRowOptions = false;
            gridButtonColumn2.Visible = false;
            gridButtonColumn2.Width = 30D;
            gridButtonColumn3.AllowDefaultButtonText = false;
            gridButtonColumn3.AllowEditing = false;
            gridButtonColumn3.AllowFiltering = true;
            gridButtonColumn3.AllowResizing = true;
            gridButtonColumn3.AllowSorting = false;
            gridButtonColumn3.ButtonSize = new System.Drawing.Size(0, 0);
            gridButtonColumn3.DefaultButtonText = "";
            gridButtonColumn3.HeaderText = " ";
            gridButtonColumn3.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.edit_16;
            gridButtonColumn3.ImageSize = new System.Drawing.Size(0, 0);
            gridButtonColumn3.MappingName = "EditButton";
            gridButtonColumn3.ShowFilterRowOptions = false;
            gridButtonColumn3.Visible = false;
            gridButtonColumn3.Width = 30D;
            gridButtonColumn4.AllowDefaultButtonText = false;
            gridButtonColumn4.AllowEditing = false;
            gridButtonColumn4.AllowFiltering = true;
            gridButtonColumn4.AllowResizing = true;
            gridButtonColumn4.AllowSorting = false;
            gridButtonColumn4.ButtonSize = new System.Drawing.Size(0, 0);
            gridButtonColumn4.DefaultButtonText = "";
            gridButtonColumn4.HeaderText = " ";
            gridButtonColumn4.Image = ((System.Drawing.Image)(resources.GetObject("gridButtonColumn4.Image")));
            gridButtonColumn4.ImageSize = new System.Drawing.Size(0, 0);
            gridButtonColumn4.MappingName = "DeleteButton";
            gridButtonColumn4.ShowFilterRowOptions = false;
            gridButtonColumn4.Visible = false;
            gridButtonColumn4.Width = 30D;
            gridNumericColumn1.AllowEditing = false;
            gridNumericColumn1.AllowFiltering = true;
            gridNumericColumn1.AllowResizing = true;
            gridNumericColumn1.AllowSorting = false;
            gridNumericColumn1.Format = "D";
            gridNumericColumn1.HeaderText = "S/N";
            gridNumericColumn1.MappingName = "Id";
            gridTextColumn1.AllowEditing = false;
            gridTextColumn1.AllowFiltering = true;
            gridTextColumn1.AllowResizing = true;
            gridTextColumn1.AllowSorting = false;
            gridTextColumn1.HeaderText = "Node Type";
            gridTextColumn1.MappingName = "NodeType";
            gridTextColumn2.AllowEditing = false;
            gridTextColumn2.AllowFiltering = true;
            gridTextColumn2.AllowResizing = true;
            gridTextColumn2.AllowSorting = false;
            gridTextColumn2.HeaderText = "Schema Name";
            gridTextColumn2.MappingName = "SchemaName";
            gridTextColumn3.AllowEditing = false;
            gridTextColumn3.AllowFiltering = true;
            gridTextColumn3.AllowGrouping = false;
            gridTextColumn3.AllowResizing = true;
            gridTextColumn3.AllowSorting = false;
            gridTextColumn3.HeaderText = "Name";
            gridTextColumn3.MappingName = "Name";
            gridTextColumn4.AllowEditing = false;
            gridTextColumn4.AllowFiltering = true;
            gridTextColumn4.AllowResizing = true;
            gridTextColumn4.HeaderText = "File Name";
            gridTextColumn4.MappingName = "FileName";
            gridTextColumn4.TextTrimming = System.Drawing.StringTrimming.EllipsisPath;
            this.MergeDataGrid.Columns.Add(gridButtonColumn1);
            this.MergeDataGrid.Columns.Add(gridButtonColumn2);
            this.MergeDataGrid.Columns.Add(gridButtonColumn3);
            this.MergeDataGrid.Columns.Add(gridButtonColumn4);
            this.MergeDataGrid.Columns.Add(gridNumericColumn1);
            this.MergeDataGrid.Columns.Add(gridTextColumn1);
            this.MergeDataGrid.Columns.Add(gridTextColumn2);
            this.MergeDataGrid.Columns.Add(gridTextColumn3);
            this.MergeDataGrid.Columns.Add(gridTextColumn4);
            this.MergeDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MergeDataGrid.GroupCaptionTextFormat = "{Key}:  {ItemsCount} ";
            this.MergeDataGrid.HideEmptyGridViewDefinition = true;
            this.MergeDataGrid.Location = new System.Drawing.Point(0, 0);
            this.MergeDataGrid.Name = "MergeDataGrid";
            this.MergeDataGrid.SelectionMode = Syncfusion.WinForms.DataGrid.Enums.GridSelectionMode.Multiple;
            this.MergeDataGrid.ShowBusyIndicator = true;
            this.MergeDataGrid.ShowGroupDropArea = true;
            this.MergeDataGrid.Size = new System.Drawing.Size(712, 304);
            this.MergeDataGrid.Style.HeaderStyle.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.MergeDataGrid.Style.HeaderStyle.Font.Bold = true;
            this.MergeDataGrid.TabIndex = 0;
            this.MergeDataGrid.ThemeName = "Office2016Colorful";
            this.MergeDataGrid.DrawCell += new Syncfusion.WinForms.DataGrid.Events.DrawCellEventHandler(this.MergeDataGrid_DrawCell);
            this.MergeDataGrid.QueryRowStyle += new Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventHandler(this.MergeDataGrid_QueryRowStyle);
            this.MergeDataGrid.SelectionChanging += new Syncfusion.WinForms.DataGrid.Events.SelectionChangingEventHandler(this.MergeDataGrid_SelectionChanging);
            this.MergeDataGrid.CellButtonClick += new Syncfusion.WinForms.DataGrid.Events.CellButtonClickEventHandler(this.MergeDataGrid_CellButtonClick);
            // 
            // MergeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MergeDataGrid);
            this.Name = "MergeList";
            this.Size = new System.Drawing.Size(712, 304);
            ((System.ComponentModel.ISupportInitialize)(this.MergeDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.WinForms.DataGrid.SfDataGrid MergeDataGrid;
    }
}
