﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Extensions;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;
using Rogler.Techno.SqlManagement.Forms.Windows;
using Syncfusion.Data;
using Syncfusion.Windows.Forms.Tools;
using Syncfusion.WinForms.Core.Utils;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Enums;
using Syncfusion.WinForms.DataGrid.Events;
using Rogler.Techno.SqlManagement.Forms.Providers;
using Syncfusion.WinForms.DataGridConverter;
using System.Xml.Serialization;
using Rogler.Techno.SqlManagement.Forms.Common;

namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    public partial class MergeList : UserControl, IEditableObject
    {
        private readonly MainForm _mainForm;
        private readonly string _indexFile;
        private readonly string _directory;
        public bool CanExport { get; set; } = true;
        public MergeListViewModel ViewModel { get; set; } = new MergeListViewModel();
        private ObservableCollection<MergeFilesModel> _model;
        private ObservableCollection<MergeFilesModel> _oldModel;
        

        public MergeList(List<MergeFilesModel> model, string indexFile, string directory, MainForm mainForm)
        {
            InitializeComponent();

            MergeDataGrid.Columns["NodeType"].Format = "s";
            MergeDataGrid.Columns["NodeType"].FormatProvider = new ObjectTypeFormatProvider();

            MergeDataGrid.CellRenderers.Remove("CaptionSummary");
            MergeDataGrid.CellRenderers.Add("CaptionSummary", new ObjectTypeSummaryCellRenderer());

            for(int i=0; i<model.Count; ++i)
            {
                model[i].Id = i + 1;
            }
            _model = new ObservableCollection<MergeFilesModel>(model);
            MergeDataGrid.DataSource = _model;

            SortColumnDescription sortColumnDescription = new SortColumnDescription {ColumnName = "Id", SortDirection = ListSortDirection.Ascending};
            MergeDataGrid.SortColumnDescriptions.Add(sortColumnDescription);

            _mainForm = mainForm;
            _indexFile = indexFile;
            _directory = directory;
            if (!string.IsNullOrEmpty(_indexFile) && !string.IsNullOrEmpty(_directory)) _directory = null;

            GridViewDefinition mergeDetailsView = new GridViewDefinition {RelationalColumn = "ParseErrors"};

            SfDataGrid childGrid = new SfDataGrid
            {
                AutoGenerateColumns = false,
                AllowResizingColumns = true,
                AllowEditing = false,
                AutoSizeColumnsMode = AutoSizeColumnsMode.AllCellsWithLastColumnFill
            };
            NumberFormatInfo nfi = new NumberFormatInfo
            {
                NumberDecimalDigits = 0,
                NumberGroupSizes = new int[] { }
            };
            childGrid.Columns.Add(new GridNumericColumn() { MappingName = "Number", HeaderText = "Number", NumberFormatInfo = nfi });
            childGrid.Columns.Add(new GridNumericColumn() { MappingName = "Line", HeaderText = "Line", NumberFormatInfo = nfi });
            childGrid.Columns.Add(new GridNumericColumn() { MappingName = "Column", HeaderText = "Column", NumberFormatInfo = nfi });
            childGrid.Columns.Add(new GridTextColumn() { MappingName = "Message", HeaderText = "Message"});
            childGrid.CellDoubleClick += ChildGridOnCellDoubleClick;

            mergeDetailsView.DataGrid = childGrid;

            MergeDataGrid.DetailsViewDefinitions.Add(mergeDetailsView);


        }

        private void ChildGridOnCellDoubleClick(object sender, CellClickEventArgs e)
        {
            if (!(e.DataRow.RowData is ParseFileError error)) return;
            SqlEditor initialEdit = null;

            if (this.Parent is TabPageAdv currentTabPage && currentTabPage.Parent is TabControlAdv tabControl)
            {
                foreach (TabPageAdv tabPage in tabControl.TabPages)
                {
                    if (tabPage.Controls.Count > 0 && tabPage.Controls[0] is SqlEditor editor && editor.FileName == error.FileName)
                    {
                        initialEdit = editor;
                        break;
                    }
                }

                if (initialEdit == null) initialEdit = _mainForm.LoadFile(Encoding.UTF8, error.FileName);
            }

            if (initialEdit != null)
            {
                initialEdit.SqlEditControl.CurrentLine = error.Line;
                initialEdit.SqlEditControl.CurrentColumn = error.Column;
            }
        }


        private void MergeDataGrid_QueryRowStyle(object sender, QueryRowStyleEventArgs e)
        {
            if (e.RowData is MergeFilesModel model)
            {
                if (model.ParseErrors != null && model.ParseErrors.Any())
                {
                    e.Style.TextColor = Color.Red;
                    e.Style.BackColor = Color.AntiqueWhite;
                }
            }
        }

        private void MergeDataGrid_SelectionChanging(object sender, SelectionChangingEventArgs e)
        {
            foreach (var item in e.AddedItems)
            {
                if (item is MergeFilesModel model)
                {
                    if (model.ParseErrors != null && model.ParseErrors.Any()) e.Cancel = true;
                }
            }
        }

        public void SelectAll()
        {
            RecordsList records = MergeDataGrid.View.Records;
            List<MergeFilesModel> selectedRecords = new List<MergeFilesModel>();

            foreach (var record in records)
            {
                if (record.Data is MergeFilesModel model)
                {
                    if (model.ParseErrors == null || !model.ParseErrors.Any())
                        selectedRecords.Add(model);
                }
            }

            MergeDataGrid.SelectedItems = new ObservableCollection<object>(selectedRecords);

        }

        public void ClearAll()
        {
            MergeDataGrid.SelectedItems.Clear();
        }

        public void Create()
        {
            if (MergeDataGrid.SelectedItems.Count == 0 || string.IsNullOrEmpty(Settings.Default.CreateFile)) return;
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog
            {
                Description = Resources.SelectOutputDirectory
            };
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(Path.Combine(folderBrowser.SelectedPath, Settings.Default.CreateFile)))
                {
                    if (MessageBox.Show(string.Format(Resources.FileExists, Path.Combine(folderBrowser.SelectedPath, Settings.Default.CreateFile)), Resources.ApplicationName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        return;
                    File.Delete(Path.Combine(folderBrowser.SelectedPath, Settings.Default.CreateFile));
                }
                if (!string.IsNullOrEmpty(Settings.Default.DeleteFile))
                {
                    if (File.Exists(Path.Combine(folderBrowser.SelectedPath, Settings.Default.DeleteFile)))
                    {
                        if (MessageBox.Show(string.Format(Resources.FileExists, Path.Combine(folderBrowser.SelectedPath, Settings.Default.DeleteFile)), Resources.ApplicationName,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                            return; 
                        File.Delete(Path.Combine(folderBrowser.SelectedPath, Settings.Default.DeleteFile));
                    }
                }

                CreateForm createForm = new CreateForm(MergeDataGrid.SelectedItems.Cast<MergeFilesModel>().ToList(),
                    _indexFile, folderBrowser.SelectedPath);
                DialogResult result = createForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    string createFile = Path.Combine(folderBrowser.SelectedPath, Settings.Default.CreateFile);
                    _mainForm.ShowLog(createForm.SortedFiles, createFile);
                    SqlEditor editor = _mainForm.LoadFile(Encoding.UTF8, createFile);
                    if(Settings.Default.CompileAfterMerge) editor.Compile();
                }
            }
        }

        public void ExportToExcel()
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "Excel File (*.xlsx)|*.xlsx|All Files (*.*)|*.*",
                Title = Resources.ExportFile
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                var options = new ExcelExportingOptions();
                options.CellExporting += Options_CellExporting;
                options.Exporting += Options_Exporting;
                options.ExcludeColumns.Add("EditButton");
                var excelEngine = MergeDataGrid.ExportToExcel(MergeDataGrid.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.Worksheets[0].AutoFilters.FilterRange = workBook.Worksheets[0].UsedRange;
                workBook.Worksheets[0].Range.AutofitColumns();
                workBook.SaveAs(saveFile.FileName);
            }
        }

        private void Options_Exporting(object sender,
            Syncfusion.WinForms.DataGridConverter.Events.DataGridExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.HeaderCell)
            {
                e.CellStyle.FontInfo.Bold = true;
                e.CellStyle.BackGroundColor = Color.LightSkyBlue;
                e.Handled = true;
            }
        }

        private void Options_CellExporting(object sender,
            Syncfusion.WinForms.DataGridConverter.Events.DataGridCellExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "NodeType")
            {
                if (e.CellValue != null && e.CellValue is SqlTreeNodeType orecord)
                {
                    e.Range.Cells[0].Value = orecord.GetDescription();
                    e.Handled = true;
                }
            }
        }


        public void ExportToPdf()
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "PDF File (*.pdf)|*.pdf|All Files (*.*)|*.*",
                Title = Resources.ExportFile
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                PdfExportingOptions options = new PdfExportingOptions { AutoColumnWidth = true };
                options.ExcludeColumns.Add("FileName");
                options.ExcludeColumns.Add("EditButton");
                options.ExcludeColumns.Add("DeleteButton");
                var document = MergeDataGrid.ExportToPdf(options);

                document.Save(saveFile.FileName);

            }
        }

        private MergeFilesModel UpdateLine()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = "SQL Files (*.sql)|*sql",
                Title = Resources.SelectSqlFile
            };
            if(openFileDialog.ShowDialog()==DialogResult.OK)
            {
                IndexForm indexForm = new IndexForm(openFileDialog.FileName);
                indexForm.ShowDialog();
                return indexForm.Model;
            }
            return null;
        }

        private void MergeDataGrid_CellButtonClick(object sender, CellButtonClickEventArgs e)
        {
            if(e.Record is Syncfusion.WinForms.DataGrid.DataRow d && d.RowData is MergeFilesModel row)
            {
                int id = row.Id;
                switch (e.ColumnIndex)
                {
                    case 1:
                        if (id >= 2)
                        {
                            MergeFilesModel line = _model[id - 1];
                            _model[id - 1] = _model[id - 2];
                            _model[id - 1].Id = id;
                            _model[id - 2] = line;
                            _model[id - 2].Id = id - 1;
                        }
                        break;
                    case 2:
                        if (id < _model.Count)
                        {
                            MergeFilesModel line = _model[id - 1];
                            _model[id - 1] = _model[id];
                            _model[id - 1].Id = id;
                            _model[id]=line;
                            _model[id].Id = id + 1;
                        }
                        break;
                    case 3:
                    {
                        MergeFilesModel newline = UpdateLine();
                        if (newline != null)
                        {
                            
                            MergeFilesModel line = _model.FirstOrDefault(m => m.Id == row.Id);

                            if (line != null)
                            {
                                line.FileName = newline.FileName;
                                line.Column = newline.Column;
                                line.ContainsGo = newline.ContainsGo;
                                line.DropCreated = newline.DropCreated;
                                line.Line = newline.Line;
                                line.Offset = newline.Offset;
                                line.OriginalFileName = newline.OriginalFileName;
                                line.NodeType = newline.NodeType;
                                line.ParseErrors = newline.ParseErrors;
                                line.Name = newline.Name;
                                line.SchemaName = newline.SchemaName;
                            }
                        }

                        break;
                    }
                    case 4:
                        if(MessageBox.Show(Resources.DeleteConfirmation, Resources.ApplicationName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)==DialogResult.OK)
                        {
                            _model.Remove(d.RowData as MergeFilesModel);
                            for(int i=0; i<_model.Count; ++i)
                            {
                                _model[i].Id = i + 1;
                            }                            
                        }
                        break;
                }
                MergeDataGrid.SortColumnDescriptions.Clear();
                SortColumnDescription sortColumnDescription = new SortColumnDescription {ColumnName = "Id", SortDirection = ListSortDirection.Ascending};
                MergeDataGrid.SortColumnDescriptions.Add(sortColumnDescription);
            }
        }


        public void New()
        {
            MergeFilesModel newline = UpdateLine();
            if (newline != null)
            {
                newline.Id = _model.Max(m => m.Id) + 1;
                _model.Insert(0, newline);
            }
        }

        public void BeginEdit()
        {
            MergeDataGrid.Columns["EditButton"].Visible = true;
            MergeDataGrid.Columns["DeleteButton"].Visible = true;
            MergeDataGrid.Columns["UpButton"].Visible = true;
            MergeDataGrid.Columns["DownButton"].Visible = true;

            MergeDataGrid.SelectionMode = GridSelectionMode.None;
            ViewModel.IsEditMode = true;
            _oldModel = _model;
        }

        public void EndEdit()
        {
            MergeDataGrid.Columns["EditButton"].Visible = false;
            MergeDataGrid.Columns["DeleteButton"].Visible = false;
            MergeDataGrid.Columns["UpButton"].Visible = false;
            MergeDataGrid.Columns["DownButton"].Visible = false;
            MergeDataGrid.SelectionMode = GridSelectionMode.Multiple;
            XmlSerializer ser = new XmlSerializer(typeof(List<MergeFilesModel>));
            using (TextWriter writer = new StreamWriter(_indexFile))
            {
                ser.Serialize(writer, _model.ToList());
            }

            ViewModel.IsEditMode = false;        }

        public void CancelEdit()
        {
            MergeDataGrid.Columns["EditButton"].Visible = false;
            MergeDataGrid.Columns["DeleteButton"].Visible = false;
            MergeDataGrid.Columns["UpButton"].Visible = false;
            MergeDataGrid.Columns["DownButton"].Visible = false;
            MergeDataGrid.SelectionMode = GridSelectionMode.Multiple;
            ViewModel.IsEditMode = false;
            _model = _oldModel;
        }

        private void MergeDataGrid_DrawCell(object sender, DrawCellEventArgs e)
        {
            if (MergeDataGrid.ShowRowHeader && e.RowIndex != 0)
            {
                if (e.ColumnIndex == 0)
                {
                    e.DisplayText = e.RowIndex.ToString();
                }
            }
        }

        public void Reload()
        {
            CancelEdit();
            MergeForm mergeForm = !string.IsNullOrEmpty(_indexFile) ? new MergeForm(_indexFile, true) : new MergeForm(_directory);
            mergeForm.ShowDialog();
            _model = new ObservableCollection<MergeFilesModel>(mergeForm.Model);
            _oldModel = _model;
            for(int i=0; i<_model.Count; ++i)
            {
                _model[i].Id = i + 1;
            }
            MergeDataGrid.DataSource = _model;
        }
    }


}
