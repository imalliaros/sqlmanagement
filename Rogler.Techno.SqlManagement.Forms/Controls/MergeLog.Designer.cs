﻿namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    partial class MergeLog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn1 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn2 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn3 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn4 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridCheckBoxColumn gridCheckBoxColumn1 = new Syncfusion.WinForms.DataGrid.GridCheckBoxColumn();
            this.LogDataGrid = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.LogDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // LogDataGrid
            // 
            this.LogDataGrid.AccessibleName = "Table";
            this.LogDataGrid.AllowEditing = false;
            this.LogDataGrid.AllowFiltering = true;
            this.LogDataGrid.AutoGenerateColumns = false;
            this.LogDataGrid.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCellsWithLastColumnFill;
            gridTextColumn1.AllowEditing = false;
            gridTextColumn1.AllowFiltering = true;
            gridTextColumn1.HeaderText = "Type";
            gridTextColumn1.MappingName = "NodeType";
            gridTextColumn2.AllowEditing = false;
            gridTextColumn2.AllowFiltering = true;
            gridTextColumn2.HeaderText = "Schema";
            gridTextColumn2.MappingName = "SchemaName";
            gridTextColumn3.AllowEditing = false;
            gridTextColumn3.AllowFiltering = true;
            gridTextColumn3.HeaderText = "Name";
            gridTextColumn3.MappingName = "Name";
            gridTextColumn4.AllowEditing = false;
            gridTextColumn4.AllowFiltering = true;
            gridTextColumn4.HeaderText = "File Name";
            gridTextColumn4.MappingName = "FileName";
            gridCheckBoxColumn1.AllowEditing = false;
            gridCheckBoxColumn1.AllowFiltering = true;
            gridCheckBoxColumn1.HeaderText = "Drop";
            gridCheckBoxColumn1.MappingName = "DropCreated";
            this.LogDataGrid.Columns.Add(gridTextColumn1);
            this.LogDataGrid.Columns.Add(gridTextColumn2);
            this.LogDataGrid.Columns.Add(gridTextColumn3);
            this.LogDataGrid.Columns.Add(gridTextColumn4);
            this.LogDataGrid.Columns.Add(gridCheckBoxColumn1);
            this.LogDataGrid.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.LogDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogDataGrid.GroupCaptionTextFormat = "{Key}:  {ItemsCount} ";
            this.LogDataGrid.Location = new System.Drawing.Point(0, 0);
            this.LogDataGrid.Name = "LogDataGrid";
            this.LogDataGrid.ShowGroupDropArea = true;
            this.LogDataGrid.Size = new System.Drawing.Size(650, 365);
            this.LogDataGrid.TabIndex = 0;
            this.LogDataGrid.ThemeName = "Office2016Colorful";
            // 
            // MergeLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LogDataGrid);
            this.Name = "MergeLog";
            this.Size = new System.Drawing.Size(650, 365);
            ((System.ComponentModel.ISupportInitialize)(this.LogDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.WinForms.DataGrid.SfDataGrid LogDataGrid;
    }
}
