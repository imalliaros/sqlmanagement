﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Providers;
using Rogler.Techno.SqlManagement.Forms.Windows;
using Rogler.Techno.SqlManagement.Forms.Properties;
using Syncfusion.WinForms.DataGridConverter;
using Rogler.Techno.SqlManagement.Forms.Extensions;

namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    public partial class MergeLog : UserControl
    {
        private readonly List<MergeFilesModel> _model;
        private readonly MainForm _mainForm;
        private readonly string _createFile;
        public MergeLog(List<MergeFilesModel> model, MainForm mainForm, string createFile)
        {
            InitializeComponent();
            _model = model;
            _mainForm = mainForm;
            _createFile = createFile;
            LogDataGrid.Columns["NodeType"].Format = "s";
            LogDataGrid.Columns["NodeType"].FormatProvider = new ObjectTypeFormatProvider();

            LogDataGrid.CellRenderers.Remove("CaptionSummary");
            LogDataGrid.CellRenderers.Add("CaptionSummary", new ObjectTypeSummaryCellRenderer());

            LogDataGrid.DataSource = model;
        }

        public void ExportToExcel()
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "Excel File (*.xlsx)|*.xlsx|All Files (*.*)|*.*",
                Title = Resources.ExportFile
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                var options = new ExcelExportingOptions();
                options.CellExporting += Options_CellExporting;
                options.Exporting += Options_Exporting;
                options.ExcludeColumns.Add("EditButton");
                var excelEngine = LogDataGrid.ExportToExcel(LogDataGrid.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.Worksheets[0].AutoFilters.FilterRange = workBook.Worksheets[0].UsedRange;
                workBook.Worksheets[0].Range.AutofitColumns();
                workBook.SaveAs(saveFile.FileName);
            }
        }

        private void Options_Exporting(object sender,
            Syncfusion.WinForms.DataGridConverter.Events.DataGridExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.HeaderCell)
            {
                e.CellStyle.FontInfo.Bold = true;
                e.CellStyle.BackGroundColor = Color.LightSkyBlue;
                e.Handled = true;
            }
        }

        private void Options_CellExporting(object sender,
            Syncfusion.WinForms.DataGridConverter.Events.DataGridCellExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "NodeType")
            {
                if (e.CellValue != null && e.CellValue is SqlTreeNodeType orecord)
                {
                    e.Range.Cells[0].Value = orecord.GetDescription();
                    e.Handled = true;
                }
            }
        }

        public void ExportToPdf()
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "PDF File (*.pdf)|*.pdf|All Files (*.*)|*.*",
                Title = Resources.ExportFile
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                PdfExportingOptions options = new PdfExportingOptions { AutoColumnWidth = true };
                options.ExcludeColumns.Add("FileName");
                options.ExcludeColumns.Add("EditButton");
                options.ExcludeColumns.Add("DeleteButton");
                var document = LogDataGrid.ExportToPdf(options);

                document.Save(saveFile.FileName);

            }
        }

    }
}
