﻿namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    partial class Schema
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Schema));
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn1 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn2 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridCheckBoxColumn gridCheckBoxColumn1 = new Syncfusion.WinForms.DataGrid.GridCheckBoxColumn();
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn1 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn2 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn3 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn4 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridCheckBoxColumn gridCheckBoxColumn2 = new Syncfusion.WinForms.DataGrid.GridCheckBoxColumn();
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn3 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn4 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.Windows.Forms.Edit.Implementation.Config.Config config1 = new Syncfusion.Windows.Forms.Edit.Implementation.Config.Config();
            Syncfusion.Windows.Forms.Edit.Implementation.Config.Config config2 = new Syncfusion.Windows.Forms.Edit.Implementation.Config.Config();
            Syncfusion.Windows.Forms.Edit.Implementation.Config.Config config3 = new Syncfusion.Windows.Forms.Edit.Implementation.Config.Config();
            this.SchemaSplitContainer = new System.Windows.Forms.SplitContainer();
            this.SchemaGroupBar = new Syncfusion.Windows.Forms.Tools.GroupBar();
            this.TablesGroupView = new Syncfusion.Windows.Forms.Tools.GroupView();
            this.SmallImageList = new System.Windows.Forms.ImageList(this.components);
            this.StoredProceduresGroupView = new Syncfusion.Windows.Forms.Tools.GroupView();
            this.TriggersGroupView = new Syncfusion.Windows.Forms.Tools.GroupView();
            this.FunctionsGroupView = new Syncfusion.Windows.Forms.Tools.GroupView();
            this.ViewsGroupView = new Syncfusion.Windows.Forms.Tools.GroupView();
            this.TablesGroupBarItem = new Syncfusion.Windows.Forms.Tools.GroupBarItem();
            this.ViewsGroupBarItem = new Syncfusion.Windows.Forms.Tools.GroupBarItem();
            this.StoredProceduresGroupBarItem = new Syncfusion.Windows.Forms.Tools.GroupBarItem();
            this.FunctionGroupBarItem = new Syncfusion.Windows.Forms.Tools.GroupBarItem();
            this.TriggerGroupBarItem = new Syncfusion.Windows.Forms.Tools.GroupBarItem();
            this.DetailsTabControl = new Rogler.Techno.SqlManagement.Forms.Controls.TablessTabControl();
            this.TablesTabPage = new System.Windows.Forms.TabPage();
            this.TablesDataGrid = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.ViewsTabPage = new System.Windows.Forms.TabPage();
            this.ViewsDataGrid = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.StoredProceduresTabPage = new System.Windows.Forms.TabPage();
            this.StoredProceduresEditControl = new Syncfusion.Windows.Forms.Edit.EditControl();
            this.FunctionsTabPage = new System.Windows.Forms.TabPage();
            this.FunctionsEditControl = new Syncfusion.Windows.Forms.Edit.EditControl();
            this.TriggersTabPage = new System.Windows.Forms.TabPage();
            this.TriggersEditControl = new Syncfusion.Windows.Forms.Edit.EditControl();
            this.TriggersStatusBar = new Syncfusion.Windows.Forms.Tools.Controls.StatusBar.StatusBarExt();
            this.TableNameStatusBarAdvPanel = new Syncfusion.Windows.Forms.Tools.StatusBarAdvPanel();
            this.designTimeTabTypeLoader = new Syncfusion.Reflection.TypeLoader(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.SchemaSplitContainer)).BeginInit();
            this.SchemaSplitContainer.Panel1.SuspendLayout();
            this.SchemaSplitContainer.Panel2.SuspendLayout();
            this.SchemaSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SchemaGroupBar)).BeginInit();
            this.SchemaGroupBar.SuspendLayout();
            this.DetailsTabControl.SuspendLayout();
            this.TablesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TablesDataGrid)).BeginInit();
            this.ViewsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ViewsDataGrid)).BeginInit();
            this.StoredProceduresTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoredProceduresEditControl)).BeginInit();
            this.FunctionsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FunctionsEditControl)).BeginInit();
            this.TriggersTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TriggersEditControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TriggersStatusBar)).BeginInit();
            this.TriggersStatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TableNameStatusBarAdvPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // SchemaSplitContainer
            // 
            this.SchemaSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SchemaSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.SchemaSplitContainer.Name = "SchemaSplitContainer";
            // 
            // SchemaSplitContainer.Panel1
            // 
            this.SchemaSplitContainer.Panel1.Controls.Add(this.SchemaGroupBar);
            // 
            // SchemaSplitContainer.Panel2
            // 
            this.SchemaSplitContainer.Panel2.Controls.Add(this.DetailsTabControl);
            this.SchemaSplitContainer.Size = new System.Drawing.Size(431, 255);
            this.SchemaSplitContainer.SplitterDistance = 143;
            this.SchemaSplitContainer.TabIndex = 0;
            // 
            // SchemaGroupBar
            // 
            this.SchemaGroupBar.AllowDrop = true;
            this.SchemaGroupBar.AnimatedSelection = false;
            this.SchemaGroupBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.SchemaGroupBar.BeforeTouchSize = new System.Drawing.Size(143, 255);
            this.SchemaGroupBar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(198)))));
            this.SchemaGroupBar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SchemaGroupBar.CollapseImage = ((System.Drawing.Image)(resources.GetObject("SchemaGroupBar.CollapseImage")));
            this.SchemaGroupBar.Controls.Add(this.TriggersGroupView);
            this.SchemaGroupBar.Controls.Add(this.FunctionsGroupView);
            this.SchemaGroupBar.Controls.Add(this.StoredProceduresGroupView);
            this.SchemaGroupBar.Controls.Add(this.ViewsGroupView);
            this.SchemaGroupBar.Controls.Add(this.TablesGroupView);
            this.SchemaGroupBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SchemaGroupBar.ExpandButtonToolTip = null;
            this.SchemaGroupBar.ExpandImage = ((System.Drawing.Image)(resources.GetObject("SchemaGroupBar.ExpandImage")));
            this.SchemaGroupBar.FlatLook = true;
            this.SchemaGroupBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.SchemaGroupBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.SchemaGroupBar.GridOfficeScrollBars = Syncfusion.Windows.Forms.OfficeScrollBars.Office2016;
            this.SchemaGroupBar.GroupBarDropDownToolTip = null;
            this.SchemaGroupBar.GroupBarItems.AddRange(new Syncfusion.Windows.Forms.Tools.GroupBarItem[] {
            this.TablesGroupBarItem,
            this.ViewsGroupBarItem,
            this.StoredProceduresGroupBarItem,
            this.FunctionGroupBarItem,
            this.TriggerGroupBarItem});
            this.SchemaGroupBar.IndexOnVisibleItems = true;
            this.SchemaGroupBar.Location = new System.Drawing.Point(0, 0);
            this.SchemaGroupBar.MinimizeButtonToolTip = null;
            this.SchemaGroupBar.Name = "SchemaGroupBar";
            this.SchemaGroupBar.NavigationPaneTooltip = null;
            this.SchemaGroupBar.PopupClientSize = new System.Drawing.Size(0, 0);
            this.SchemaGroupBar.SelectedItem = 4;
            this.SchemaGroupBar.Size = new System.Drawing.Size(143, 255);
            this.SchemaGroupBar.SmartSizeBox = false;
            this.SchemaGroupBar.Splittercolor = System.Drawing.Color.Red;
            this.SchemaGroupBar.TabIndex = 0;
            this.SchemaGroupBar.TextAlign = Syncfusion.Windows.Forms.Tools.TextAlignment.Left;
            this.SchemaGroupBar.VisualStyle = Syncfusion.Windows.Forms.VisualStyle.Office2016White;
            this.SchemaGroupBar.GroupBarItemSelected += new System.EventHandler(this.SchemaGroupBar_GroupBarItemSelected);
            // 
            // TablesGroupView
            // 
            this.TablesGroupView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TablesGroupView.BeforeTouchSize = new System.Drawing.Size(141, 231);
            this.TablesGroupView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TablesGroupView.ButtonView = true;
            this.TablesGroupView.FlatLook = true;
            this.TablesGroupView.Location = new System.Drawing.Point(1, 23);
            this.TablesGroupView.Name = "TablesGroupView";
            this.TablesGroupView.Size = new System.Drawing.Size(141, 231);
            this.TablesGroupView.SmallImageList = this.SmallImageList;
            this.TablesGroupView.SmallImageView = true;
            this.TablesGroupView.Style = Syncfusion.Windows.Forms.Appearance.Office2016;
            this.TablesGroupView.TabIndex = 2;
            this.TablesGroupView.ThemesEnabled = true;
            this.TablesGroupView.GroupViewItemSelected += new System.EventHandler(this.TablesGroupView_GroupViewItemSelected);
            // 
            // SmallImageList
            // 
            this.SmallImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SmallImageList.ImageStream")));
            this.SmallImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.SmallImageList.Images.SetKeyName(0, "data-sheet-16.png");
            this.SmallImageList.Images.SetKeyName(1, "database-view-16.ico");
            this.SmallImageList.Images.SetKeyName(2, "workflow-16.png");
            this.SmallImageList.Images.SetKeyName(3, "square-root-16.png");
            this.SmallImageList.Images.SetKeyName(4, "trigger-16.png");
            // 
            // StoredProceduresGroupView
            // 
            this.StoredProceduresGroupView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.StoredProceduresGroupView.BeforeTouchSize = new System.Drawing.Size(141, 187);
            this.StoredProceduresGroupView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StoredProceduresGroupView.ButtonView = true;
            this.StoredProceduresGroupView.Location = new System.Drawing.Point(1, 67);
            this.StoredProceduresGroupView.Name = "StoredProceduresGroupView";
            this.StoredProceduresGroupView.Size = new System.Drawing.Size(141, 187);
            this.StoredProceduresGroupView.SmallImageList = this.SmallImageList;
            this.StoredProceduresGroupView.SmallImageView = true;
            this.StoredProceduresGroupView.Style = Syncfusion.Windows.Forms.Appearance.Office2016;
            this.StoredProceduresGroupView.TabIndex = 4;
            this.StoredProceduresGroupView.Text = "groupView1";
            this.StoredProceduresGroupView.ThemesEnabled = true;
            this.StoredProceduresGroupView.GroupViewItemSelected += new System.EventHandler(this.StoredProceduresGroupView_GroupViewItemSelected);
            // 
            // TriggersGroupView
            // 
            this.TriggersGroupView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TriggersGroupView.BeforeTouchSize = new System.Drawing.Size(141, 143);
            this.TriggersGroupView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TriggersGroupView.ButtonView = true;
            this.TriggersGroupView.Location = new System.Drawing.Point(1, 111);
            this.TriggersGroupView.Name = "TriggersGroupView";
            this.TriggersGroupView.Size = new System.Drawing.Size(141, 143);
            this.TriggersGroupView.SmallImageList = this.SmallImageList;
            this.TriggersGroupView.SmallImageView = true;
            this.TriggersGroupView.Style = Syncfusion.Windows.Forms.Appearance.Office2016;
            this.TriggersGroupView.TabIndex = 6;
            this.TriggersGroupView.ThemesEnabled = true;
            this.TriggersGroupView.GroupViewItemSelected += new System.EventHandler(this.TriggersGroupView_GroupViewItemSelected);
            // 
            // FunctionsGroupView
            // 
            this.FunctionsGroupView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.FunctionsGroupView.BeforeTouchSize = new System.Drawing.Size(141, 165);
            this.FunctionsGroupView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FunctionsGroupView.ButtonView = true;
            this.FunctionsGroupView.Location = new System.Drawing.Point(1, 89);
            this.FunctionsGroupView.Name = "FunctionsGroupView";
            this.FunctionsGroupView.Size = new System.Drawing.Size(141, 165);
            this.FunctionsGroupView.SmallImageList = this.SmallImageList;
            this.FunctionsGroupView.SmallImageView = true;
            this.FunctionsGroupView.Style = Syncfusion.Windows.Forms.Appearance.Office2016;
            this.FunctionsGroupView.TabIndex = 5;
            this.FunctionsGroupView.ThemesEnabled = true;
            this.FunctionsGroupView.GroupViewItemSelected += new System.EventHandler(this.FunctionsGroupView_GroupViewItemSelected);
            // 
            // ViewsGroupView
            // 
            this.ViewsGroupView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ViewsGroupView.BeforeTouchSize = new System.Drawing.Size(141, 209);
            this.ViewsGroupView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ViewsGroupView.ButtonView = true;
            this.ViewsGroupView.Location = new System.Drawing.Point(1, 45);
            this.ViewsGroupView.Name = "ViewsGroupView";
            this.ViewsGroupView.Size = new System.Drawing.Size(141, 209);
            this.ViewsGroupView.SmallImageList = this.SmallImageList;
            this.ViewsGroupView.SmallImageView = true;
            this.ViewsGroupView.Style = Syncfusion.Windows.Forms.Appearance.Office2016;
            this.ViewsGroupView.TabIndex = 3;
            this.ViewsGroupView.ThemesEnabled = true;
            this.ViewsGroupView.GroupViewItemSelected += new System.EventHandler(this.ViewsGroupView_GroupViewItemSelected);
            // 
            // TablesGroupBarItem
            // 
            this.TablesGroupBarItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.TablesGroupBarItem.Client = this.TablesGroupView;
            this.TablesGroupBarItem.Icon = ((System.Drawing.Icon)(resources.GetObject("TablesGroupBarItem.Icon")));
            this.TablesGroupBarItem.Text = "Tables";
            // 
            // ViewsGroupBarItem
            // 
            this.ViewsGroupBarItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ViewsGroupBarItem.Client = this.ViewsGroupView;
            this.ViewsGroupBarItem.Icon = ((System.Drawing.Icon)(resources.GetObject("ViewsGroupBarItem.Icon")));
            this.ViewsGroupBarItem.Text = "Views";
            // 
            // StoredProceduresGroupBarItem
            // 
            this.StoredProceduresGroupBarItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.StoredProceduresGroupBarItem.Client = this.StoredProceduresGroupView;
            this.StoredProceduresGroupBarItem.Icon = ((System.Drawing.Icon)(resources.GetObject("StoredProceduresGroupBarItem.Icon")));
            this.StoredProceduresGroupBarItem.Text = "Stored Procedures";
            // 
            // FunctionGroupBarItem
            // 
            this.FunctionGroupBarItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.FunctionGroupBarItem.Client = this.FunctionsGroupView;
            this.FunctionGroupBarItem.Icon = ((System.Drawing.Icon)(resources.GetObject("FunctionGroupBarItem.Icon")));
            this.FunctionGroupBarItem.Text = "Functions";
            // 
            // TriggerGroupBarItem
            // 
            this.TriggerGroupBarItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.TriggerGroupBarItem.Client = this.TriggersGroupView;
            this.TriggerGroupBarItem.Icon = ((System.Drawing.Icon)(resources.GetObject("TriggerGroupBarItem.Icon")));
            this.TriggerGroupBarItem.Text = "Triggers";
            // 
            // DetailsTabControl
            // 
            this.DetailsTabControl.Controls.Add(this.TablesTabPage);
            this.DetailsTabControl.Controls.Add(this.ViewsTabPage);
            this.DetailsTabControl.Controls.Add(this.StoredProceduresTabPage);
            this.DetailsTabControl.Controls.Add(this.FunctionsTabPage);
            this.DetailsTabControl.Controls.Add(this.TriggersTabPage);
            this.DetailsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetailsTabControl.Location = new System.Drawing.Point(0, 0);
            this.DetailsTabControl.Name = "DetailsTabControl";
            this.DetailsTabControl.SelectedIndex = 0;
            this.DetailsTabControl.Size = new System.Drawing.Size(284, 255);
            this.DetailsTabControl.TabIndex = 0;
            // 
            // TablesTabPage
            // 
            this.TablesTabPage.Controls.Add(this.TablesDataGrid);
            this.TablesTabPage.Location = new System.Drawing.Point(4, 22);
            this.TablesTabPage.Name = "TablesTabPage";
            this.TablesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.TablesTabPage.Size = new System.Drawing.Size(276, 229);
            this.TablesTabPage.TabIndex = 0;
            this.TablesTabPage.Text = "Tables";
            this.TablesTabPage.UseVisualStyleBackColor = true;
            // 
            // TablesDataGrid
            // 
            this.TablesDataGrid.AccessibleName = "Table";
            this.TablesDataGrid.AllowEditing = false;
            this.TablesDataGrid.AutoGenerateColumns = false;
            this.TablesDataGrid.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            gridTextColumn1.AllowEditing = false;
            gridTextColumn1.HeaderText = "Name";
            gridTextColumn1.MappingName = "ColumnName";
            gridTextColumn2.AllowEditing = false;
            gridTextColumn2.HeaderText = "Data Type";
            gridTextColumn2.MappingName = "DataType";
            gridCheckBoxColumn1.AllowEditing = false;
            gridCheckBoxColumn1.HeaderText = "Nullable";
            gridCheckBoxColumn1.MappingName = "IsNullable";
            gridNumericColumn1.AllowEditing = false;
            gridNumericColumn1.Format = "d";
            gridNumericColumn1.HeaderText = "Maximum Length";
            gridNumericColumn1.MappingName = "CharacterMaximumLength";
            gridNumericColumn2.AllowEditing = false;
            gridNumericColumn2.Format = "d";
            gridNumericColumn2.HeaderText = "Precision";
            gridNumericColumn2.MappingName = "NumericPrecision";
            this.TablesDataGrid.Columns.Add(gridTextColumn1);
            this.TablesDataGrid.Columns.Add(gridTextColumn2);
            this.TablesDataGrid.Columns.Add(gridCheckBoxColumn1);
            this.TablesDataGrid.Columns.Add(gridNumericColumn1);
            this.TablesDataGrid.Columns.Add(gridNumericColumn2);
            this.TablesDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TablesDataGrid.Location = new System.Drawing.Point(3, 3);
            this.TablesDataGrid.Name = "TablesDataGrid";
            this.TablesDataGrid.Size = new System.Drawing.Size(270, 223);
            this.TablesDataGrid.TabIndex = 0;
            // 
            // ViewsTabPage
            // 
            this.ViewsTabPage.Controls.Add(this.ViewsDataGrid);
            this.ViewsTabPage.Location = new System.Drawing.Point(4, 22);
            this.ViewsTabPage.Name = "ViewsTabPage";
            this.ViewsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ViewsTabPage.Size = new System.Drawing.Size(276, 229);
            this.ViewsTabPage.TabIndex = 1;
            this.ViewsTabPage.Text = "Views";
            this.ViewsTabPage.UseVisualStyleBackColor = true;
            // 
            // ViewsDataGrid
            // 
            this.ViewsDataGrid.AccessibleName = "Table";
            this.ViewsDataGrid.AllowEditing = false;
            this.ViewsDataGrid.AutoGenerateColumns = false;
            this.ViewsDataGrid.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            gridTextColumn3.AllowEditing = false;
            gridTextColumn3.HeaderText = "Name";
            gridTextColumn3.MappingName = "ColumnName";
            gridTextColumn4.AllowEditing = false;
            gridTextColumn4.HeaderText = "Data Type";
            gridTextColumn4.MappingName = "DataType";
            gridCheckBoxColumn2.AllowEditing = false;
            gridCheckBoxColumn2.HeaderText = "Nullable";
            gridCheckBoxColumn2.MappingName = "IsNullable";
            gridNumericColumn3.AllowEditing = false;
            gridNumericColumn3.Format = "d";
            gridNumericColumn3.HeaderText = "Maximum Length";
            gridNumericColumn3.MappingName = "CharacterMaximumLength";
            gridNumericColumn4.AllowEditing = false;
            gridNumericColumn4.Format = "d";
            gridNumericColumn4.HeaderText = "Precision";
            gridNumericColumn4.MappingName = "NumericPrecision";
            this.ViewsDataGrid.Columns.Add(gridTextColumn3);
            this.ViewsDataGrid.Columns.Add(gridTextColumn4);
            this.ViewsDataGrid.Columns.Add(gridCheckBoxColumn2);
            this.ViewsDataGrid.Columns.Add(gridNumericColumn3);
            this.ViewsDataGrid.Columns.Add(gridNumericColumn4);
            this.ViewsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewsDataGrid.Location = new System.Drawing.Point(3, 3);
            this.ViewsDataGrid.Name = "ViewsDataGrid";
            this.ViewsDataGrid.Size = new System.Drawing.Size(270, 223);
            this.ViewsDataGrid.TabIndex = 1;
            // 
            // StoredProceduresTabPage
            // 
            this.StoredProceduresTabPage.Controls.Add(this.StoredProceduresEditControl);
            this.StoredProceduresTabPage.Location = new System.Drawing.Point(4, 22);
            this.StoredProceduresTabPage.Name = "StoredProceduresTabPage";
            this.StoredProceduresTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.StoredProceduresTabPage.Size = new System.Drawing.Size(276, 229);
            this.StoredProceduresTabPage.TabIndex = 2;
            this.StoredProceduresTabPage.Text = "Stored Prcedures";
            this.StoredProceduresTabPage.UseVisualStyleBackColor = true;
            // 
            // StoredProceduresEditControl
            // 
            this.StoredProceduresEditControl.ChangedLinesMarkingLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(238)))), ((int)(((byte)(98)))));
            this.StoredProceduresEditControl.CodeSnipptSize = new System.Drawing.Size(100, 100);
            this.StoredProceduresEditControl.Configurator = config1;
            this.StoredProceduresEditControl.ContextChoiceBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.StoredProceduresEditControl.ContextChoiceBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(166)))), ((int)(((byte)(50)))));
            this.StoredProceduresEditControl.ContextChoiceForeColor = System.Drawing.SystemColors.InfoText;
            this.StoredProceduresEditControl.ContextPromptBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))));
            this.StoredProceduresEditControl.ContextTooltipBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(232)))), ((int)(((byte)(236))))));
            this.StoredProceduresEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StoredProceduresEditControl.FileExtensions = new string[] {
        ".sql"};
            this.StoredProceduresEditControl.HighlightCurrentLine = true;
            this.StoredProceduresEditControl.IndicatorMarginBackColor = System.Drawing.Color.Empty;
            this.StoredProceduresEditControl.LineNumbersColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.StoredProceduresEditControl.LineNumbersFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StoredProceduresEditControl.Location = new System.Drawing.Point(3, 3);
            this.StoredProceduresEditControl.MarkChangedLines = true;
            this.StoredProceduresEditControl.Name = "StoredProceduresEditControl";
            this.StoredProceduresEditControl.ReadOnly = true;
            this.StoredProceduresEditControl.RenderRightToLeft = false;
            this.StoredProceduresEditControl.SaveOnClose = false;
            this.StoredProceduresEditControl.ScrollPosition = new System.Drawing.Point(0, 0);
            this.StoredProceduresEditControl.SelectionTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(214)))), ((int)(((byte)(255)))));
            this.StoredProceduresEditControl.Size = new System.Drawing.Size(270, 223);
            this.StoredProceduresEditControl.StatusBarSettings.CoordsPanel.Width = 150;
            this.StoredProceduresEditControl.StatusBarSettings.EncodingPanel.Width = 100;
            this.StoredProceduresEditControl.StatusBarSettings.FileNamePanel.Width = 100;
            this.StoredProceduresEditControl.StatusBarSettings.InsertPanel.Width = 33;
            this.StoredProceduresEditControl.StatusBarSettings.Offcie2007ColorScheme = Syncfusion.Windows.Forms.Office2007Theme.Blue;
            this.StoredProceduresEditControl.StatusBarSettings.Offcie2010ColorScheme = Syncfusion.Windows.Forms.Office2010Theme.Blue;
            this.StoredProceduresEditControl.StatusBarSettings.StatusPanel.Width = 70;
            this.StoredProceduresEditControl.StatusBarSettings.TextPanel.Width = 214;
            this.StoredProceduresEditControl.StatusBarSettings.VisualStyle = Syncfusion.Windows.Forms.Tools.Controls.StatusBar.VisualStyle.Default;
            this.StoredProceduresEditControl.TabIndex = 0;
            this.StoredProceduresEditControl.Text = "";
            this.StoredProceduresEditControl.UseXPStyleBorder = true;
            this.StoredProceduresEditControl.VisualColumn = 1;
            this.StoredProceduresEditControl.VScrollMode = Syncfusion.Windows.Forms.Edit.ScrollMode.Immediate;
            // 
            // FunctionsTabPage
            // 
            this.FunctionsTabPage.Controls.Add(this.FunctionsEditControl);
            this.FunctionsTabPage.Location = new System.Drawing.Point(4, 22);
            this.FunctionsTabPage.Name = "FunctionsTabPage";
            this.FunctionsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.FunctionsTabPage.Size = new System.Drawing.Size(276, 229);
            this.FunctionsTabPage.TabIndex = 3;
            this.FunctionsTabPage.Text = "Functions";
            this.FunctionsTabPage.UseVisualStyleBackColor = true;
            // 
            // FunctionsEditControl
            // 
            this.FunctionsEditControl.ChangedLinesMarkingLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(238)))), ((int)(((byte)(98)))));
            this.FunctionsEditControl.CodeSnipptSize = new System.Drawing.Size(100, 100);
            this.FunctionsEditControl.Configurator = config2;
            this.FunctionsEditControl.ContextChoiceBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.FunctionsEditControl.ContextChoiceBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(166)))), ((int)(((byte)(50)))));
            this.FunctionsEditControl.ContextChoiceForeColor = System.Drawing.SystemColors.InfoText;
            this.FunctionsEditControl.ContextPromptBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))));
            this.FunctionsEditControl.ContextTooltipBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(232)))), ((int)(((byte)(236))))));
            this.FunctionsEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FunctionsEditControl.FileExtensions = new string[] {
        ".sql"};
            this.FunctionsEditControl.HighlightCurrentLine = true;
            this.FunctionsEditControl.IndicatorMarginBackColor = System.Drawing.Color.Empty;
            this.FunctionsEditControl.LineNumbersColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.FunctionsEditControl.LineNumbersFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FunctionsEditControl.Location = new System.Drawing.Point(3, 3);
            this.FunctionsEditControl.MarkChangedLines = true;
            this.FunctionsEditControl.Name = "FunctionsEditControl";
            this.FunctionsEditControl.ReadOnly = true;
            this.FunctionsEditControl.RenderRightToLeft = false;
            this.FunctionsEditControl.SaveOnClose = false;
            this.FunctionsEditControl.ScrollPosition = new System.Drawing.Point(0, 0);
            this.FunctionsEditControl.SelectionTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(214)))), ((int)(((byte)(255)))));
            this.FunctionsEditControl.Size = new System.Drawing.Size(270, 223);
            this.FunctionsEditControl.StatusBarSettings.CoordsPanel.Width = 150;
            this.FunctionsEditControl.StatusBarSettings.EncodingPanel.Width = 100;
            this.FunctionsEditControl.StatusBarSettings.FileNamePanel.Width = 100;
            this.FunctionsEditControl.StatusBarSettings.InsertPanel.Width = 33;
            this.FunctionsEditControl.StatusBarSettings.Offcie2007ColorScheme = Syncfusion.Windows.Forms.Office2007Theme.Blue;
            this.FunctionsEditControl.StatusBarSettings.Offcie2010ColorScheme = Syncfusion.Windows.Forms.Office2010Theme.Blue;
            this.FunctionsEditControl.StatusBarSettings.StatusPanel.Width = 70;
            this.FunctionsEditControl.StatusBarSettings.TextPanel.Width = 214;
            this.FunctionsEditControl.StatusBarSettings.VisualStyle = Syncfusion.Windows.Forms.Tools.Controls.StatusBar.VisualStyle.Default;
            this.FunctionsEditControl.TabIndex = 1;
            this.FunctionsEditControl.Text = "";
            this.FunctionsEditControl.UseXPStyleBorder = true;
            this.FunctionsEditControl.VisualColumn = 1;
            this.FunctionsEditControl.VScrollMode = Syncfusion.Windows.Forms.Edit.ScrollMode.Immediate;
            // 
            // TriggersTabPage
            // 
            this.TriggersTabPage.Controls.Add(this.TriggersEditControl);
            this.TriggersTabPage.Controls.Add(this.TriggersStatusBar);
            this.TriggersTabPage.Location = new System.Drawing.Point(4, 22);
            this.TriggersTabPage.Name = "TriggersTabPage";
            this.TriggersTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.TriggersTabPage.Size = new System.Drawing.Size(276, 229);
            this.TriggersTabPage.TabIndex = 4;
            this.TriggersTabPage.Text = "Triggers";
            this.TriggersTabPage.UseVisualStyleBackColor = true;
            // 
            // TriggersEditControl
            // 
            this.TriggersEditControl.ChangedLinesMarkingLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(238)))), ((int)(((byte)(98)))));
            this.TriggersEditControl.CodeSnipptSize = new System.Drawing.Size(100, 100);
            this.TriggersEditControl.Configurator = config3;
            this.TriggersEditControl.ContextChoiceBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TriggersEditControl.ContextChoiceBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(166)))), ((int)(((byte)(50)))));
            this.TriggersEditControl.ContextChoiceForeColor = System.Drawing.SystemColors.InfoText;
            this.TriggersEditControl.ContextPromptBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))));
            this.TriggersEditControl.ContextTooltipBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(232)))), ((int)(((byte)(236))))));
            this.TriggersEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TriggersEditControl.FileExtensions = new string[] {
        ".sql"};
            this.TriggersEditControl.HighlightCurrentLine = true;
            this.TriggersEditControl.IndicatorMarginBackColor = System.Drawing.Color.Empty;
            this.TriggersEditControl.LineNumbersColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.TriggersEditControl.LineNumbersFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TriggersEditControl.Location = new System.Drawing.Point(3, 3);
            this.TriggersEditControl.MarkChangedLines = true;
            this.TriggersEditControl.Name = "TriggersEditControl";
            this.TriggersEditControl.ReadOnly = true;
            this.TriggersEditControl.RenderRightToLeft = false;
            this.TriggersEditControl.SaveOnClose = false;
            this.TriggersEditControl.ScrollPosition = new System.Drawing.Point(0, 0);
            this.TriggersEditControl.SelectionTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(214)))), ((int)(((byte)(255)))));
            this.TriggersEditControl.Size = new System.Drawing.Size(270, 197);
            this.TriggersEditControl.StatusBarSettings.CoordsPanel.Width = 150;
            this.TriggersEditControl.StatusBarSettings.EncodingPanel.Width = 100;
            this.TriggersEditControl.StatusBarSettings.FileNamePanel.Width = 100;
            this.TriggersEditControl.StatusBarSettings.InsertPanel.Width = 33;
            this.TriggersEditControl.StatusBarSettings.Offcie2007ColorScheme = Syncfusion.Windows.Forms.Office2007Theme.Blue;
            this.TriggersEditControl.StatusBarSettings.Offcie2010ColorScheme = Syncfusion.Windows.Forms.Office2010Theme.Blue;
            this.TriggersEditControl.StatusBarSettings.StatusPanel.Width = 70;
            this.TriggersEditControl.StatusBarSettings.TextPanel.Width = 214;
            this.TriggersEditControl.StatusBarSettings.VisualStyle = Syncfusion.Windows.Forms.Tools.Controls.StatusBar.VisualStyle.Default;
            this.TriggersEditControl.TabIndex = 2;
            this.TriggersEditControl.Text = "";
            this.TriggersEditControl.UseXPStyleBorder = true;
            this.TriggersEditControl.VisualColumn = 1;
            this.TriggersEditControl.VScrollMode = Syncfusion.Windows.Forms.Edit.ScrollMode.Immediate;
            // 
            // TriggersStatusBar
            // 
            this.TriggersStatusBar.BeforeTouchSize = new System.Drawing.Size(270, 26);
            this.TriggersStatusBar.BorderColor = System.Drawing.SystemColors.Control;
            this.TriggersStatusBar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TriggersStatusBar.Controls.Add(this.TableNameStatusBarAdvPanel);
            this.TriggersStatusBar.CustomLayoutBounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.TriggersStatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TriggersStatusBar.Location = new System.Drawing.Point(3, 200);
            this.TriggersStatusBar.Name = "TriggersStatusBar";
            this.TriggersStatusBar.Padding = new System.Windows.Forms.Padding(3);
            this.TriggersStatusBar.Size = new System.Drawing.Size(270, 26);
            this.TriggersStatusBar.Spacing = new System.Drawing.Size(2, 2);
            this.TriggersStatusBar.TabIndex = 0;
            this.TriggersStatusBar.ThemesEnabled = true;
            this.TriggersStatusBar.VisualStyle = Syncfusion.Windows.Forms.Tools.Controls.StatusBar.VisualStyle.Default;
            // 
            // TableNameStatusBarAdvPanel
            // 
            this.TableNameStatusBarAdvPanel.BackColor = System.Drawing.SystemColors.Control;
            this.TableNameStatusBarAdvPanel.BeforeTouchSize = new System.Drawing.Size(100, 20);
            this.TableNameStatusBarAdvPanel.Location = new System.Drawing.Point(0, 2);
            this.TableNameStatusBarAdvPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TableNameStatusBarAdvPanel.Name = "TableNameStatusBarAdvPanel";
            this.TableNameStatusBarAdvPanel.Size = new System.Drawing.Size(100, 20);
            this.TableNameStatusBarAdvPanel.TabIndex = 0;
            this.TableNameStatusBarAdvPanel.Text = null;
            this.TableNameStatusBarAdvPanel.ThemesEnabled = true;
            // 
            // designTimeTabTypeLoader
            // 
            this.designTimeTabTypeLoader.InvokeMemberName = "TabStyleName";
            // 
            // Schema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SchemaSplitContainer);
            this.Name = "Schema";
            this.Size = new System.Drawing.Size(431, 255);
            this.SchemaSplitContainer.Panel1.ResumeLayout(false);
            this.SchemaSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SchemaSplitContainer)).EndInit();
            this.SchemaSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SchemaGroupBar)).EndInit();
            this.SchemaGroupBar.ResumeLayout(false);
            this.DetailsTabControl.ResumeLayout(false);
            this.TablesTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TablesDataGrid)).EndInit();
            this.ViewsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ViewsDataGrid)).EndInit();
            this.StoredProceduresTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StoredProceduresEditControl)).EndInit();
            this.FunctionsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FunctionsEditControl)).EndInit();
            this.TriggersTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TriggersEditControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TriggersStatusBar)).EndInit();
            this.TriggersStatusBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TableNameStatusBarAdvPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer SchemaSplitContainer;
        private Syncfusion.Windows.Forms.Tools.GroupBar SchemaGroupBar;
        private Syncfusion.Windows.Forms.Tools.GroupBarItem TablesGroupBarItem;
        private Syncfusion.Windows.Forms.Tools.GroupBarItem ViewsGroupBarItem;
        private Syncfusion.Windows.Forms.Tools.GroupBarItem StoredProceduresGroupBarItem;
        private Syncfusion.Windows.Forms.Tools.GroupBarItem FunctionGroupBarItem;
        private Syncfusion.Windows.Forms.Tools.GroupBarItem TriggerGroupBarItem;
        private Syncfusion.Windows.Forms.Tools.GroupView TablesGroupView;
        private Syncfusion.Windows.Forms.Tools.GroupView ViewsGroupView;
        private Syncfusion.Windows.Forms.Tools.GroupView TriggersGroupView;
        private Syncfusion.Windows.Forms.Tools.GroupView FunctionsGroupView;
        private Syncfusion.Windows.Forms.Tools.GroupView StoredProceduresGroupView;
        private System.Windows.Forms.ImageList SmallImageList;
        private Syncfusion.Reflection.TypeLoader designTimeTabTypeLoader;
        private TablessTabControl DetailsTabControl;
        private System.Windows.Forms.TabPage TablesTabPage;
        private System.Windows.Forms.TabPage ViewsTabPage;
        private Syncfusion.WinForms.DataGrid.SfDataGrid TablesDataGrid;
        private Syncfusion.WinForms.DataGrid.SfDataGrid ViewsDataGrid;
        private System.Windows.Forms.TabPage StoredProceduresTabPage;
        private Syncfusion.Windows.Forms.Edit.EditControl StoredProceduresEditControl;
        private System.Windows.Forms.TabPage FunctionsTabPage;
        private Syncfusion.Windows.Forms.Edit.EditControl FunctionsEditControl;
        private System.Windows.Forms.TabPage TriggersTabPage;
        private Syncfusion.Windows.Forms.Edit.EditControl TriggersEditControl;
        private Syncfusion.Windows.Forms.Tools.Controls.StatusBar.StatusBarExt TriggersStatusBar;
        private Syncfusion.Windows.Forms.Tools.StatusBarAdvPanel TableNameStatusBarAdvPanel;
    }
}
