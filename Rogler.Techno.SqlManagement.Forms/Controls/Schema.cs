﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Models.Db;
using Syncfusion.Windows.Forms.Edit.Enums;
using Syncfusion.Windows.Forms.Tools;

namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    public partial class Schema : UserControl
    {
        private readonly Operations _operations;

        public Schema(Operations operations)
        {
            InitializeComponent();
            _operations = operations;
            TablesGroupView.GroupViewItems.AddRange(_operations.Tables.Select(t => new GroupViewItem(t.Name, 0)).ToArray());
            ViewsGroupView.GroupViewItems.AddRange(_operations.Views.Select(t => new GroupViewItem(t.Name, 1)).ToArray());
            StoredProceduresGroupView.GroupViewItems.AddRange(_operations.StoredProcedures.Select(t => new GroupViewItem(t.Name, 2)).ToArray());
            FunctionsGroupView.GroupViewItems.AddRange(_operations.Functions.Select(t => new GroupViewItem(t.Name, 3)).ToArray());
            TriggersGroupView.GroupViewItems.AddRange(_operations.Triggers.Select(t => new GroupViewItem(t.TriggerName, 4)).ToArray());
            SchemaGroupBar_GroupBarItemSelected(SchemaGroupBar, EventArgs.Empty);
            StoredProceduresEditControl.ApplyConfiguration(KnownLanguages.SQL);
            FunctionsEditControl.ApplyConfiguration(KnownLanguages.SQL);
            TriggersEditControl.ApplyConfiguration(KnownLanguages.SQL);
        }

        private void SchemaGroupBar_GroupBarItemSelected(object sender, EventArgs e)
        {
            switch (SchemaGroupBar.SelectedItem)
            {
                case 0:
                    TablesGroupView_GroupViewItemSelected(TablesGroupView, EventArgs.Empty);
                    break;
                case 1:
                    ViewsGroupView_GroupViewItemSelected(ViewsGroupView, EventArgs.Empty);
                    break;
                case 2:
                    StoredProceduresGroupView_GroupViewItemSelected(StoredProceduresGroupView, EventArgs.Empty);
                    break;
                case 3:
                    FunctionsGroupView_GroupViewItemSelected(FunctionsGroupView, EventArgs.Empty);
                    break;
                case 4:
                    TriggersGroupView_GroupViewItemSelected(TriggersGroupView, EventArgs.Empty);
                    break;
                
            }
        }

        private void TablesGroupView_GroupViewItemSelected(object sender, EventArgs e)
        {
            DetailsTabControl.SelectedIndex = 0;
            if(TablesGroupView.SelectedItem!=-1)
                TablesDataGrid.DataSource = _operations.Tables[TablesGroupView.SelectedItem].Columns;
        }

        private void ViewsGroupView_GroupViewItemSelected(object sender, EventArgs e)
        {
            DetailsTabControl.SelectedIndex = 1;
            if(ViewsGroupView.SelectedItem!=-1)

                ViewsDataGrid.DataSource = _operations.Views[ViewsGroupView.SelectedItem].Columns;
        }

        private void StoredProceduresGroupView_GroupViewItemSelected(object sender, EventArgs e)
        {
            DetailsTabControl.SelectedIndex = 2;
            if(StoredProceduresGroupView.SelectedItem!=-1)
                StoredProceduresEditControl.Text = _operations.StoredProcedures[StoredProceduresGroupView.SelectedItem].ProcedureText;
            StoredProceduresEditControl.CurrentLine = 1;
            StoredProceduresEditControl.CurrentColumn = 1;
        }

        private void FunctionsGroupView_GroupViewItemSelected(object sender, EventArgs e)
        {
            DetailsTabControl.SelectedIndex = 3;
            if(FunctionsGroupView.SelectedItem!=-1)
                FunctionsEditControl.Text = _operations.Functions[FunctionsGroupView.SelectedItem].ProcedureText;
            FunctionsEditControl.CurrentLine = 1;
            FunctionsEditControl.CurrentColumn = 1;
        }

        private void TriggersGroupView_GroupViewItemSelected(object sender, EventArgs e)
        {
            DetailsTabControl.SelectedIndex = 4;
            if (TriggersGroupView.SelectedItem != -1)
            {
                TriggersEditControl.Text = _operations.Triggers[TriggersGroupView.SelectedItem].TriggerText;
                TableNameStatusBarAdvPanel.Text = _operations.Triggers[TriggersGroupView.SelectedItem].TableName;
            }
            TriggersEditControl.CurrentLine = 1;
            TriggersEditControl.CurrentColumn = 1;
        }
    }
}
