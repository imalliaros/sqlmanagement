﻿namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    partial class SqlEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SqlEditor));
            Syncfusion.Windows.Forms.Edit.Implementation.Config.Config config1 = new Syncfusion.Windows.Forms.Edit.Implementation.Config.Config();
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn1 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn2 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.WinForms.DataGrid.GridNumericColumn gridNumericColumn3 = new Syncfusion.WinForms.DataGrid.GridNumericColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn1 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn2 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn3 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn4 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn5 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            Syncfusion.WinForms.DataGrid.GridCheckBoxColumn gridCheckBoxColumn1 = new Syncfusion.WinForms.DataGrid.GridCheckBoxColumn();
            Syncfusion.WinForms.DataGrid.GridCheckBoxColumn gridCheckBoxColumn2 = new Syncfusion.WinForms.DataGrid.GridCheckBoxColumn();
            Syncfusion.WinForms.DataGrid.GridCheckBoxColumn gridCheckBoxColumn3 = new Syncfusion.WinForms.DataGrid.GridCheckBoxColumn();
            Syncfusion.WinForms.DataGrid.GridTextColumn gridTextColumn6 = new Syncfusion.WinForms.DataGrid.GridTextColumn();
            this.SqlTabSplitterContainer = new Syncfusion.Windows.Forms.Tools.TabSplitterContainer();
            this.EditorTabSplitterPage = new Syncfusion.Windows.Forms.Tools.TabSplitterPage();
            this.SqlEditControl = new Syncfusion.Windows.Forms.Edit.EditControl();
            this.ErrorsTabSplitterPage = new Syncfusion.Windows.Forms.Tools.TabSplitterPage();
            this.ErrorsDataGrid = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.OutputTabSplitterPage = new Syncfusion.Windows.Forms.Tools.TabSplitterPage();
            this.OutputGrid = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.ResultTabSplitterPage = new Syncfusion.Windows.Forms.Tools.TabSplitterPage();
            this.ResultsDataGrid = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.SqlImageList = new System.Windows.Forms.ImageList(this.components);
            this.SqlTabSplitterContainer.SuspendLayout();
            this.EditorTabSplitterPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SqlEditControl)).BeginInit();
            this.ErrorsTabSplitterPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorsDataGrid)).BeginInit();
            this.OutputTabSplitterPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OutputGrid)).BeginInit();
            this.ResultTabSplitterPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResultsDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // SqlTabSplitterContainer
            // 
            this.SqlTabSplitterContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SqlTabSplitterContainer.Location = new System.Drawing.Point(0, 0);
            this.SqlTabSplitterContainer.Name = "SqlTabSplitterContainer";
            this.SqlTabSplitterContainer.PrimaryPages.AddRange(new Syncfusion.Windows.Forms.Tools.TabSplitterPage[] {
            this.EditorTabSplitterPage});
            this.SqlTabSplitterContainer.SecondaryPages.AddRange(new Syncfusion.Windows.Forms.Tools.TabSplitterPage[] {
            this.ErrorsTabSplitterPage,
            this.OutputTabSplitterPage,
            this.ResultTabSplitterPage});
            this.SqlTabSplitterContainer.Size = new System.Drawing.Size(741, 371);
            this.SqlTabSplitterContainer.SplitterBackColor = System.Drawing.SystemColors.Control;
            this.SqlTabSplitterContainer.SplitterPosition = 186;
            this.SqlTabSplitterContainer.TabIndex = 0;
            // 
            // EditorTabSplitterPage
            // 
            this.EditorTabSplitterPage.AutoScroll = true;
            this.EditorTabSplitterPage.Controls.Add(this.SqlEditControl);
            this.EditorTabSplitterPage.Hide = false;
            this.EditorTabSplitterPage.Image = ((System.Drawing.Image)(resources.GetObject("EditorTabSplitterPage.Image")));
            this.EditorTabSplitterPage.Location = new System.Drawing.Point(0, 0);
            this.EditorTabSplitterPage.Name = "EditorTabSplitterPage";
            this.EditorTabSplitterPage.Size = new System.Drawing.Size(741, 186);
            this.EditorTabSplitterPage.TabIndex = 6;
            this.EditorTabSplitterPage.Text = "Editor";
            // 
            // SqlEditControl
            // 
            this.SqlEditControl.ChangedLinesMarkingLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(238)))), ((int)(((byte)(98)))));
            this.SqlEditControl.CodeSnipptSize = new System.Drawing.Size(100, 100);
            this.SqlEditControl.Configurator = config1;
            this.SqlEditControl.ContextChoiceBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SqlEditControl.ContextChoiceBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(166)))), ((int)(((byte)(50)))));
            this.SqlEditControl.ContextChoiceForeColor = System.Drawing.SystemColors.InfoText;
            this.SqlEditControl.ContextPromptBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))));
            this.SqlEditControl.ContextTooltipBackgroundBrush = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(232)))), ((int)(((byte)(236))))));
            this.SqlEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SqlEditControl.EnableRTL = false;
            this.SqlEditControl.FileExtensions = new string[] {
        ".sql"};
            this.SqlEditControl.HighlightCurrentLine = true;
            this.SqlEditControl.IndicatorMarginBackColor = System.Drawing.Color.Empty;
            this.SqlEditControl.LineNumbersColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.SqlEditControl.LineNumbersFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SqlEditControl.Location = new System.Drawing.Point(0, 0);
            this.SqlEditControl.MarkChangedLines = true;
            this.SqlEditControl.Name = "SqlEditControl";
            this.SqlEditControl.ParsingMode = Syncfusion.Windows.Forms.Edit.Enums.TextParsingMode.PartialParsingNoFallback;
            this.SqlEditControl.RenderRightToLeft = false;
            this.SqlEditControl.SaveOnClose = false;
            this.SqlEditControl.ScrollPosition = new System.Drawing.Point(0, 91);
            this.SqlEditControl.ScrollVisualStyle = Syncfusion.Windows.Forms.ScrollBarCustomDrawStyles.Office2016;
            this.SqlEditControl.SelectionTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(214)))), ((int)(((byte)(255)))));
            this.SqlEditControl.SharedFileMode = true;
            this.SqlEditControl.Size = new System.Drawing.Size(741, 186);
            this.SqlEditControl.StatusBarSettings.CoordsPanel.Width = 150;
            this.SqlEditControl.StatusBarSettings.EncodingPanel.Width = 100;
            this.SqlEditControl.StatusBarSettings.FileNamePanel.Width = 100;
            this.SqlEditControl.StatusBarSettings.InsertPanel.Width = 33;
            this.SqlEditControl.StatusBarSettings.Offcie2007ColorScheme = Syncfusion.Windows.Forms.Office2007Theme.Blue;
            this.SqlEditControl.StatusBarSettings.Offcie2010ColorScheme = Syncfusion.Windows.Forms.Office2010Theme.Blue;
            this.SqlEditControl.StatusBarSettings.StatusPanel.Width = 70;
            this.SqlEditControl.StatusBarSettings.TextPanel.Width = 214;
            this.SqlEditControl.StatusBarSettings.VisualStyle = Syncfusion.Windows.Forms.Tools.Controls.StatusBar.VisualStyle.Office2016Colorful;
            this.SqlEditControl.Style = Syncfusion.Windows.Forms.Edit.EditControlStyle.Office2016Colorful;
            this.SqlEditControl.TabIndex = 0;
            this.SqlEditControl.Text = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
            this.SqlEditControl.UseXPStyle = false;
            this.SqlEditControl.UseXPStyleBorder = true;
            this.SqlEditControl.VisualColumn = 1;
            this.SqlEditControl.VScrollMode = Syncfusion.Windows.Forms.Edit.ScrollMode.Immediate;
            this.SqlEditControl.TextChanging += new Syncfusion.Windows.Forms.Edit.TextChangingEventHandler(this.SqlEditControl_TextChanging);
            // 
            // ErrorsTabSplitterPage
            // 
            this.ErrorsTabSplitterPage.AutoScroll = true;
            this.ErrorsTabSplitterPage.Controls.Add(this.ErrorsDataGrid);
            this.ErrorsTabSplitterPage.Hide = false;
            this.ErrorsTabSplitterPage.Image = ((System.Drawing.Image)(resources.GetObject("ErrorsTabSplitterPage.Image")));
            this.ErrorsTabSplitterPage.Location = new System.Drawing.Point(0, 206);
            this.ErrorsTabSplitterPage.Name = "ErrorsTabSplitterPage";
            this.ErrorsTabSplitterPage.Size = new System.Drawing.Size(741, 165);
            this.ErrorsTabSplitterPage.TabIndex = 4;
            this.ErrorsTabSplitterPage.Text = "Errors";
            // 
            // ErrorsDataGrid
            // 
            this.ErrorsDataGrid.AccessibleName = "Table";
            this.ErrorsDataGrid.AllowEditing = false;
            this.ErrorsDataGrid.AllowGrouping = false;
            this.ErrorsDataGrid.AllowResizingColumns = true;
            this.ErrorsDataGrid.AutoGenerateColumns = false;
            this.ErrorsDataGrid.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCellsWithLastColumnFill;
            gridNumericColumn1.AllowEditing = false;
            gridNumericColumn1.AllowGrouping = false;
            gridNumericColumn1.AllowResizing = true;
            gridNumericColumn1.Format = "d";
            gridNumericColumn1.HeaderText = "Number";
            gridNumericColumn1.MappingName = "Number";
            gridNumericColumn2.AllowEditing = false;
            gridNumericColumn2.AllowGrouping = false;
            gridNumericColumn2.AllowResizing = true;
            gridNumericColumn2.Format = "d";
            gridNumericColumn2.HeaderText = "Line";
            gridNumericColumn2.MappingName = "Line";
            gridNumericColumn3.AllowEditing = false;
            gridNumericColumn3.AllowGrouping = false;
            gridNumericColumn3.AllowResizing = true;
            gridNumericColumn3.Format = "d";
            gridNumericColumn3.HeaderText = "Column";
            gridNumericColumn3.MappingName = "Column";
            gridTextColumn1.AllowEditing = false;
            gridTextColumn1.AllowGrouping = false;
            gridTextColumn1.AllowResizing = true;
            gridTextColumn1.HeaderText = "Mesage";
            gridTextColumn1.MappingName = "Message";
            this.ErrorsDataGrid.Columns.Add(gridNumericColumn1);
            this.ErrorsDataGrid.Columns.Add(gridNumericColumn2);
            this.ErrorsDataGrid.Columns.Add(gridNumericColumn3);
            this.ErrorsDataGrid.Columns.Add(gridTextColumn1);
            this.ErrorsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ErrorsDataGrid.Location = new System.Drawing.Point(0, 0);
            this.ErrorsDataGrid.Name = "ErrorsDataGrid";
            this.ErrorsDataGrid.ShowRowHeader = true;
            this.ErrorsDataGrid.Size = new System.Drawing.Size(741, 165);
            this.ErrorsDataGrid.TabIndex = 0;
            this.ErrorsDataGrid.ThemeName = "Metro";
            this.ErrorsDataGrid.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.ErrorsDataGrid_CellDoubleClick);
            // 
            // OutputTabSplitterPage
            // 
            this.OutputTabSplitterPage.AutoScroll = true;
            this.OutputTabSplitterPage.Controls.Add(this.OutputGrid);
            this.OutputTabSplitterPage.Hide = false;
            this.OutputTabSplitterPage.Image = ((System.Drawing.Image)(resources.GetObject("OutputTabSplitterPage.Image")));
            this.OutputTabSplitterPage.Location = new System.Drawing.Point(0, 206);
            this.OutputTabSplitterPage.Name = "OutputTabSplitterPage";
            this.OutputTabSplitterPage.Size = new System.Drawing.Size(741, 165);
            this.OutputTabSplitterPage.TabIndex = 5;
            this.OutputTabSplitterPage.Text = "Output";
            // 
            // OutputGrid
            // 
            this.OutputGrid.AccessibleName = "Table";
            this.OutputGrid.AllowEditing = false;
            this.OutputGrid.AllowFiltering = true;
            this.OutputGrid.AllowResizingColumns = true;
            this.OutputGrid.AutoGenerateColumns = false;
            this.OutputGrid.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCellsWithLastColumnFill;
            gridTextColumn2.AllowEditing = false;
            gridTextColumn2.AllowFiltering = true;
            gridTextColumn2.AllowResizing = true;
            gridTextColumn2.HeaderText = "Object Type";
            gridTextColumn2.MappingName = "NodeType";
            gridTextColumn3.AllowEditing = false;
            gridTextColumn3.AllowFiltering = true;
            gridTextColumn3.AllowResizing = true;
            gridTextColumn3.HeaderText = "Schema";
            gridTextColumn3.MappingName = "SchemaName";
            gridTextColumn4.AllowEditing = false;
            gridTextColumn4.AllowFiltering = true;
            gridTextColumn4.AllowGrouping = false;
            gridTextColumn4.AllowResizing = true;
            gridTextColumn4.HeaderText = "Name";
            gridTextColumn4.MappingName = "Name";
            gridTextColumn5.AllowEditing = false;
            gridTextColumn5.AllowFiltering = true;
            gridTextColumn5.AllowGrouping = false;
            gridTextColumn5.AllowResizing = true;
            gridTextColumn5.HeaderText = "File Name";
            gridTextColumn5.MappingName = "FileName";
            gridTextColumn5.TextTrimming = System.Drawing.StringTrimming.EllipsisPath;
            this.OutputGrid.Columns.Add(gridTextColumn2);
            this.OutputGrid.Columns.Add(gridTextColumn3);
            this.OutputGrid.Columns.Add(gridTextColumn4);
            this.OutputGrid.Columns.Add(gridTextColumn5);
            this.OutputGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutputGrid.GroupCaptionTextFormat = "{Key}:  {ItemsCount} ";
            this.OutputGrid.Location = new System.Drawing.Point(0, 0);
            this.OutputGrid.Name = "OutputGrid";
            this.OutputGrid.ShowGroupDropArea = true;
            this.OutputGrid.ShowRowHeader = true;
            this.OutputGrid.Size = new System.Drawing.Size(741, 165);
            this.OutputGrid.TabIndex = 0;
            this.OutputGrid.ThemeName = "Metro";
            this.OutputGrid.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.OutputGrid_CellDoubleClick);
            // 
            // ResultTabSplitterPage
            // 
            this.ResultTabSplitterPage.AutoScroll = true;
            this.ResultTabSplitterPage.Controls.Add(this.ResultsDataGrid);
            this.ResultTabSplitterPage.Hide = false;
            this.ResultTabSplitterPage.Image = ((System.Drawing.Image)(resources.GetObject("ResultTabSplitterPage.Image")));
            this.ResultTabSplitterPage.Location = new System.Drawing.Point(0, 206);
            this.ResultTabSplitterPage.Name = "ResultTabSplitterPage";
            this.ResultTabSplitterPage.Size = new System.Drawing.Size(741, 165);
            this.ResultTabSplitterPage.TabIndex = 7;
            this.ResultTabSplitterPage.Text = "Schema";
            // 
            // ResultsDataGrid
            // 
            this.ResultsDataGrid.AccessibleName = "Table";
            this.ResultsDataGrid.AllowEditing = false;
            this.ResultsDataGrid.AutoGenerateColumns = false;
            this.ResultsDataGrid.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCellsWithLastColumnFill;
            gridCheckBoxColumn1.AllowEditing = false;
            gridCheckBoxColumn1.HeaderText = "Create";
            gridCheckBoxColumn1.MappingName = "Create";
            gridCheckBoxColumn2.AllowEditing = false;
            gridCheckBoxColumn2.HeaderText = "Alter";
            gridCheckBoxColumn2.MappingName = "Alter";
            gridCheckBoxColumn3.AllowEditing = false;
            gridCheckBoxColumn3.HeaderText = "Drop";
            gridCheckBoxColumn3.MappingName = "Drop";
            gridTextColumn6.AllowEditing = false;
            gridTextColumn6.HeaderText = "Name";
            gridTextColumn6.MappingName = "Name";
            this.ResultsDataGrid.Columns.Add(gridCheckBoxColumn1);
            this.ResultsDataGrid.Columns.Add(gridCheckBoxColumn2);
            this.ResultsDataGrid.Columns.Add(gridCheckBoxColumn3);
            this.ResultsDataGrid.Columns.Add(gridTextColumn6);
            this.ResultsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResultsDataGrid.Location = new System.Drawing.Point(0, 0);
            this.ResultsDataGrid.Name = "ResultsDataGrid";
            this.ResultsDataGrid.Size = new System.Drawing.Size(741, 165);
            this.ResultsDataGrid.TabIndex = 0;
            // 
            // SqlImageList
            // 
            this.SqlImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SqlImageList.ImageStream")));
            this.SqlImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.SqlImageList.Images.SetKeyName(0, "important-16.png");
            this.SqlImageList.Images.SetKeyName(1, "output-16.png");
            this.SqlImageList.Images.SetKeyName(2, "edit-file-16.png");
            // 
            // SqlEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SqlTabSplitterContainer);
            this.Name = "SqlEditor";
            this.Size = new System.Drawing.Size(741, 371);
            this.SqlTabSplitterContainer.ResumeLayout(false);
            this.EditorTabSplitterPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SqlEditControl)).EndInit();
            this.ErrorsTabSplitterPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorsDataGrid)).EndInit();
            this.OutputTabSplitterPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OutputGrid)).EndInit();
            this.ResultTabSplitterPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ResultsDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.TabSplitterContainer SqlTabSplitterContainer;
        private System.Windows.Forms.ImageList SqlImageList;
        private Syncfusion.Windows.Forms.Tools.TabSplitterPage ErrorsTabSplitterPage;
        private Syncfusion.Windows.Forms.Tools.TabSplitterPage OutputTabSplitterPage;
        private Syncfusion.Windows.Forms.Tools.TabSplitterPage EditorTabSplitterPage;
        public Syncfusion.Windows.Forms.Edit.EditControl SqlEditControl;
        private Syncfusion.WinForms.DataGrid.SfDataGrid ErrorsDataGrid;
        private Syncfusion.WinForms.DataGrid.SfDataGrid OutputGrid;
        private Syncfusion.Windows.Forms.Tools.TabSplitterPage ResultTabSplitterPage;
        private Syncfusion.WinForms.DataGrid.SfDataGrid ResultsDataGrid;
    }
}
