﻿using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;
using Rogler.Techno.SqlManagement.Forms.Windows;
using Syncfusion.Windows.Forms.Edit;
using Syncfusion.Windows.Forms.Edit.Enums;
using Syncfusion.Windows.Forms.Tools;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Extensions;
using Rogler.Techno.SqlManagement.Forms.Providers;
using Syncfusion.Pdf;
using Syncfusion.Windows.Forms.Edit.Implementation.Config;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;

namespace Rogler.Techno.SqlManagement.Forms.Controls
{
    public partial class SqlEditor : UserControl
    {
        public bool HasChanges
        {
            get => SqlEditControl.FileModified;
            set => SqlEditControl.FileModified = value;
        }


        public EditorViewModel ViewModel { get; set; }

        public string FileName { get; set; }

        //private readonly Main
        public SqlEditor()
        {
            InitializeComponent();
            ViewModel = new EditorViewModel();
            SqlTabSplitterContainer.Dock = DockStyle.Fill;
            SqlEditControl.ApplyConfiguration(KnownLanguages.SQL);
            //SqlEditControl.SetEncoding(Encoding.UTF8);
            SqlEditControl.StatusBarSettings.GripVisibility = Syncfusion.Windows.Forms.Edit.Enums.SizingGripVisibility.Visible;
            SqlEditControl.StatusBarSettings.Visible = true;
            SqlEditControl.StatusBarSettings.GripVisibility = Syncfusion.Windows.Forms.Edit.Enums.SizingGripVisibility.Hidden;
            SqlEditControl.StatusBarSettings.FileNamePanel.Visible = false;

            OutputGrid.Columns["NodeType"].Format = "s";
            OutputGrid.Columns["NodeType"].FormatProvider = new ObjectTypeFormatProvider();

            OutputGrid.CellRenderers.Remove("CaptionSummary");
            OutputGrid.CellRenderers.Add("CaptionSummary", new ObjectTypeSummaryCellRenderer());

            SqlTabSplitterContainer.DataBindings.Add("Collapsed", ViewModel, "SplitterCollapsed");

        }

        public bool LoadFile(Encoding encoding=null, string fileName = null)
        {
            try
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog
                    {
                        CheckFileExists = true,
                        Filter = "SQL Files (*.sql)|*sql",
                        Title = Resources.SelectSqlFile
                    };
                    if (openFileDialog.ShowDialog() == DialogResult.OK) fileName = openFileDialog.FileName;
                }

                if (string.IsNullOrEmpty(fileName)) return false;
                if(encoding==null) encoding = Globals.GetFileEncoding(fileName);
                using (var stream = new StreamReader(fileName, encoding))
                {
                    string s = stream.ReadToEnd();
                    SqlEditControl.Text = s;
                }

                SqlEditControl.ApplyConfiguration(KnownLanguages.SQL);
                SqlEditControl.GoTo(1, 1);
                FileName = fileName;
                SqlEditControl.FileModified = false;
                HasChanges = false;
                SqlTabSplitterContainer.Collapsed = true;
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Resources.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Save()
        {
            string s = SqlEditControl.Text;
            using (StreamWriter outputFile = new StreamWriter(FileName)) {
                outputFile.Write(s);
            }

            HasChanges = false;
            if (this.Parent is TabPageAdv tabPageAdv) tabPageAdv.Text = System.IO.Path.GetFileName(FileName);
        }

        public void SaveAs()
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "SQL File (*.sql)|*.sql|All Files (*.*)|*.*",
                Title = Resources.SaveFile
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(File.Open(saveFile.FileName, FileMode.Create), Encoding.UTF8))
                {
                    string s = SqlEditControl.Text;
                    sw.Write(s);
                }

                FileName = saveFile.FileName;
                HasChanges = false;
                if (this.Parent is TabPageAdv tabPageAdv)
                {
                    tabPageAdv.Text = System.IO.Path.GetFileName(FileName);
                    tabPageAdv.ToolTipText = FileName;
                }
            }

        }

        private void SqlEditControl_TextChanging(object sender, TextChangingEventArgs e)
        {
            if (HasChanges == false)
            {
                HasChanges = true;
                if (this.Parent is TabPageAdv tabPageAdv) tabPageAdv.Text = "* " + System.IO.Path.GetFileName(FileName);
            }
        }

        public void Print()
        {
            SqlEditControl.Print();
        }

        public void Compile()
        {
            if (HasChanges)
            {
                if (MessageBox.Show(Resources.FileHasChanges,
                        Resources.SaveFile, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Save();
                }
                else
                {
                    return;
                }

            }

            CompileForm parseForm = new CompileForm(FileName, SqlEditControl.Text);
            DialogResult result = parseForm.ShowDialog();
            ViewModel.SplitterCollapsed = false;
            switch (result)
            {
                case DialogResult.OK:
                    ErrorsDataGrid.DataSource = null;
                    OutputGrid.DataSource = parseForm.Output;
                    SqlTabSplitterContainer.SecondaryPages.SelectedIndex = 1;
                    (ParentForm as MainForm)?.Sidebar(FileName, parseForm.Output);
                    ViewModel.HasOutput = true;                    
                    ResultsDataGrid.DataSource = parseForm.Result;
                    ViewModel.ParseErrors = null;
                    ViewModel.ObjectsDescription = parseForm.ObjectsDescription;
                    ViewModel.Output = parseForm.Output;
                    ViewModel.Root = parseForm.Root;

                    break;
                case DialogResult.Abort:
                    ErrorsDataGrid.DataSource = parseForm.ParseErrors;
                    ViewModel.ParseErrors = parseForm.ParseErrors;
                    ViewModel.ObjectsDescription = null;
                    ViewModel.Output = null;
                    ViewModel.Root = null;
                    OutputGrid.DataSource = null;
                    SqlTabSplitterContainer.SecondaryPages.SelectedIndex = 0;
                    ViewModel.HasOutput = false;
                    break;
            }

        }

        public void Split()
        {
            if (HasChanges)
            {
                if (MessageBox.Show(Resources.FileHasChanges,
                        Resources.SaveFile, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Save();
                }
                else
                {
                    return;
                }

            }
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog
            {
                Description = Resources.SelectOutputDirectory,
                ShowNewFolderButton = true
            };

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                if (Directory.EnumerateFileSystemEntries(folderBrowser.SelectedPath).Any())
                {
                    if (MessageBox.Show(Resources.NonEmptyDirectory, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
                    DirectoryInfo di = new DirectoryInfo(folderBrowser.SelectedPath);
                    foreach (FileInfo file in di.EnumerateFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.EnumerateDirectories())
                    {
                        dir.Delete(true);
                    }
                }
                ParseForm parseForm = new ParseForm(FileName, folderBrowser.SelectedPath, SqlEditControl.Text);
                DialogResult result = parseForm.ShowDialog();
                ViewModel.SplitterCollapsed = false;

                switch (result)
                {
                    case DialogResult.OK:
                    {
                        ErrorsDataGrid.DataSource = null;
                        OutputGrid.DataSource = parseForm.Output;
                        SqlTabSplitterContainer.SecondaryPages.SelectedIndex = 1;
                        (ParentForm as MainForm)?.Sidebar(FileName, parseForm.Output);
                        ViewModel.HasOutput = true;
                        ResultsDataGrid.DataSource = parseForm.Result;
                        ViewModel.ParseErrors = null;
                        ViewModel.ObjectsDescription = parseForm.ObjectsDescription;
                        ViewModel.Output = parseForm.Output;
                        ViewModel.Root = parseForm.Root;

                        break;
                    }

                    case DialogResult.Abort:
                        ErrorsDataGrid.DataSource = parseForm.ParseErrors;
                        OutputGrid.DataSource = null;
                        SqlTabSplitterContainer.SecondaryPages.SelectedIndex = 0;
                        ViewModel.HasOutput = false;
                        ViewModel.ParseErrors = parseForm.ParseErrors;
                        ViewModel.ObjectsDescription = null;
                        ViewModel.Output = null;
                        ViewModel.Root = null;

                        break;
                }

            }
        }

        public void Check()
        {
            if (HasChanges)
            {
                if (MessageBox.Show(Resources.FileHasChanges,
                        Resources.SaveFile, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Save();
                }
                else
                {
                    return;
                }

            }
            CheckForm checkForm = new CheckForm(SqlEditControl.Text);
            DialogResult result = checkForm.ShowDialog();
            ViewModel.HasOutput = false;
            switch (result)
            {
                case DialogResult.OK:
                    ViewModel.SplitterCollapsed = true;
                    ErrorsDataGrid.DataSource = null;
                    OutputGrid.DataSource = null;
                    MessageBox.Show(Resources.NoErrors, Resources.CheckFile, MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    break;
                case DialogResult.Abort:
                    ViewModel.SplitterCollapsed = false;
                    ErrorsDataGrid.DataSource = checkForm.ParseErrors;
                    OutputGrid.DataSource = null;
                    SqlTabSplitterContainer.SecondaryPages.SelectedIndex = 0;
                    break;
            }
        }

        private void OutputGrid_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            MergeFilesModel record = e.DataRow.RowData as MergeFilesModel;
            if (record == null) return;
            if (string.IsNullOrEmpty(record.FileName) || record.FileName==record.OriginalFileName)
            {
                SqlEditControl.CurrentLine = record.Line;
                SqlEditControl.CurrentColumn = record.Column;
            }
            else
            {
                (this.ParentForm as MainForm)?.LoadFile(Encoding.UTF8, record.FileName);
            }
        }

        private void ErrorsDataGrid_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            if (!(e.DataRow.RowData is ParseError record)) return;
            SqlEditControl.CurrentLine = record.Line;
            SqlEditControl.CurrentColumn = record.Column;

        }

        public void Copy()
        {
            if (SqlEditControl.CanCopy) SqlEditControl.Copy();
        }

        public void Cut()
        {
            if(SqlEditControl.CanCut) SqlEditControl.Cut();
        }

        public void Paste()
        {
            if(SqlEditControl.CanPaste) SqlEditControl.Paste();
        }

        public void Undo()
        {
            if(SqlEditControl.CanUndo) SqlEditControl.Undo();
        }

        public void Redo()
        {
            if(SqlEditControl.CanRedo) SqlEditControl.Redo();
        }

        public void Find()
        {
            SqlEditControl.ShowFindDialog();
        }

        public void Replace()
        {
            SqlEditControl.ShowReplaceDialog();
        }

        public void GoToLine()
        {
            SqlEditControl.ShowGoToDialog();
        }

        public void ExportToExcel()
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "Excel File (*.xlsx)|*.xlsx|All Files (*.*)|*.*",
                Title = Resources.ExportFile
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                var options = new ExcelExportingOptions();
                options.CellExporting += Options_CellExporting;
                options.Exporting += Options_Exporting;
                var excelEngine = OutputGrid.ExportToExcel(OutputGrid.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.Worksheets[0].AutoFilters.FilterRange = workBook.Worksheets[0].UsedRange;
                workBook.Worksheets[0].Range.AutofitColumns();
                workBook.SaveAs(saveFile.FileName);
            }
        }

        private void Options_Exporting(object sender,
            Syncfusion.WinForms.DataGridConverter.Events.DataGridExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.HeaderCell)
            {
                e.CellStyle.FontInfo.Bold = true;
                e.CellStyle.BackGroundColor = Color.LightSkyBlue;
                e.Handled = true;
            }
        }

        private void Options_CellExporting(object sender,
            Syncfusion.WinForms.DataGridConverter.Events.DataGridCellExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "NodeType")
            {
                if (e.CellValue != null && e.CellValue is SqlTreeNodeType orecord)
                {
                    e.Range.Cells[0].Value = orecord.GetDescription();
                    e.Handled = true;
                }
            }
        }

        public void ExportToPdf()
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "PDF File (*.pdf)|*.pdf|All Files (*.*)|*.*",
                Title = Resources.ExportFile
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                PdfExportingOptions options = new PdfExportingOptions { AutoColumnWidth = true };
                options.ExcludeColumns.Add("FileName");

                var document = OutputGrid.ExportToPdf(options);

                document.Save(saveFile.FileName);

            }
        }


    }
}
