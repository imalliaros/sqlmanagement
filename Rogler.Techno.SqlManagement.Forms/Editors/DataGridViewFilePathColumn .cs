﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design.Behavior;

namespace Rogler.Techno.SqlManagement.Forms.Editors
{
    /// <summary>
    /// Hosts a collection of DataGridViewTextBoxCell cells.
    /// </summary>
    public class DataGridViewFilePathColumn : DataGridViewColumn
    {
        private bool showBrowseButton;
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DefaultValue(false)]
        [Category("Appearance")]
        [Description("Show a button in each cell for browsing for files.")]
        public bool ShowBrowseButton
        {
            get { return showBrowseButton; }
            set
            {

                showBrowseButton = value;
            }
        }

        private bool useOpenFileDialogOnButtonClick;
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("OpenFileDialog is dispalyed and on success the contents of the Cell is replaced with the new file path.")]
        public bool UseOpenFileDialogOnButtonClick
        {
            get { return useOpenFileDialogOnButtonClick; }
            set
            {
                useOpenFileDialogOnButtonClick = value;
            }
        }
        public DataGridViewFilePathColumn()
            : base(new DataGridViewFilePathCell())
        {
        }
        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                if (null != value &&
                    !value.GetType().IsAssignableFrom(typeof(DataGridViewFilePathCell)))
                {
                    throw new InvalidCastException("must be a DataGridViewFilePathCell");
                }
                base.CellTemplate = value;
            }
        }
    }
}