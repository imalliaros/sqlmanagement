﻿using System;
using System.ComponentModel;
using System.Reflection;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Models.Db;

namespace Rogler.Techno.SqlManagement.Forms.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    if (Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) is DescriptionAttribute attr)
                    {
                        return attr.Description;
                    }
                }
            }

            return null;
        }

        public static ObjectTypeEnum GetObjectType(this SqlTreeNodeType value)
        {
            switch (value)
            {
                case SqlTreeNodeType.CreateProcedureStatement:
                case SqlTreeNodeType.AlterProcedureStatement:
                    return ObjectTypeEnum.StoreProcedure;
                case SqlTreeNodeType.AlterFunctionStatement:
                case SqlTreeNodeType.CreateFunctionStatement:
                    return ObjectTypeEnum.Function;
                case SqlTreeNodeType.AlterTableStatement:
                case SqlTreeNodeType.CreateTableStatement:
                    return ObjectTypeEnum.Table;
                case SqlTreeNodeType.CreateTriggerStatement:
                    return ObjectTypeEnum.Trigger;
                case SqlTreeNodeType.AlterViewStatement:
                case SqlTreeNodeType.CreateViewStatement:
                    return ObjectTypeEnum.View;
                default:
                    return ObjectTypeEnum.Other;

            }
        }

    }
}