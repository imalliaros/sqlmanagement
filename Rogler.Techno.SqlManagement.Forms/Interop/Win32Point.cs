﻿using System.Runtime.InteropServices;

namespace Rogler.Techno.SqlManagement.Forms.Interop
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Win32Point
    {
        public int x;
        public int y;
    }
}