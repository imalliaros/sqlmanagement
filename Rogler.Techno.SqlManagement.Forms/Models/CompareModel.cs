﻿namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class CompareModel
    {
        public ObjectTypeEnum ObjectType { get; set; }
        public string Name { get; set; }
        public string ServerText { get; set; }
        public string ScriptText { get; set; }
        public bool ServerExists { get; set; }
        public bool Create { get; set; }
        public bool Alter { get; set; }
    }
}