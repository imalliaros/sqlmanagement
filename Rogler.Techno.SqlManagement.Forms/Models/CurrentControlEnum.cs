﻿namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public enum CurrentControlEnum
    {
        None,
        Editor,
        MergeList,
        Log
    }
}