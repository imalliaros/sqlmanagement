﻿using System.Data;
using Microsoft.SqlServer.Management.Smo;

namespace Rogler.Techno.SqlManagement.Forms.Models.Db
{
    public class DbColumn
    {
        public string ColumnName { get; set; }

        public string DataType { get; set; }

        public bool IsNullable { get; set; }

        public int? CharacterMaximumLength { get; set; }
        public int NumericPrecision { get; set; }
        public int OrdinalPosition { get; set; }

        public DbColumn(DataRow row)
        {
            ColumnName = row.Field<string>("ColumnName");
            DataType = row.Field<string>("DataType");
            IsNullable = row.Field<string>("IsNullable")=="YES";
            CharacterMaximumLength = row.Field<int?>("CharacterMaximumLength");
            NumericPrecision = row.Field<byte?>("NumericPrecision")??0;
            OrdinalPosition = row.Field<int>("OrdinalPosition");
        }

        public DbColumn(Column column)
        {
            ColumnName = column.Name;
            DataType = column.DataType.Name;
            CharacterMaximumLength = column.DataType.MaximumLength;
            IsNullable = column.Nullable;
            NumericPrecision = column.DataType.NumericPrecision;            
        }
    }
}