﻿using System.Collections.Generic;

namespace Rogler.Techno.SqlManagement.Forms.Models.Db
{
    public class DbObjectDescription
    {
        public ObjectTypeEnum ObjectType { get; set; }
        public string Name { get; set; }

        public List<FileDescriptionModel> Details { get; set; } = new List<FileDescriptionModel>();
    }
}