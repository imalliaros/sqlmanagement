﻿using System;
using System.Data;
using Microsoft.SqlServer.Management.Smo;
using Syncfusion.Data.Extensions;

namespace Rogler.Techno.SqlManagement.Forms.Models.Db
{
    public class DbRoutine
    {
        public DbRoutine(DataRow row)
        {
            Schema = row.Field<string>("Schema");
            Name = row.Field<string>("Name");
            ProcedureText = row.Field<string>("ProcedureText");
            CreatedDate = row.Field<DateTime>("CreatedDate");
        }

        public DbRoutine(StoredProcedure sp)
        {
            Schema = sp.Schema;
            Name = sp.Name;
            CreatedDate = sp.CreateDate;
            ProcedureText = string.Join("\n", sp.Script().ToList<string>());
        }

        public DbRoutine(UserDefinedFunction function)
        {
            Schema = function.Name;
            Name = function.Name;
            CreatedDate = function.CreateDate;
            ProcedureText = string.Join("\n", function.Script().ToList<string>());
        }

        public string Schema { get; set; }
        public string Name { get; set; }
        public string ProcedureText { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}