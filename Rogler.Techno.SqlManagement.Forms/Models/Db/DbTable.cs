﻿using System.Collections.Generic;
using System.Data;
using Microsoft.SqlServer.Management.Smo;
using Syncfusion.Data.Extensions;


namespace Rogler.Techno.SqlManagement.Forms.Models.Db
{
    public class DbTable
    {
        public DbTable(DataRow row)
        {
            Schema = row.Field<string>("Schema");
            Name = row.Field<string>("Name");
        }

        public DbTable(View view)
        {
            Schema = view.Schema;
            Name = view.Name;
            Script = string.Join("\n", view.Script().ToList<string>());
            Columns = new List<DbColumn>();
            foreach (Column column in view.Columns)
            {
                Columns.Add(new DbColumn(column));
            }
        }

        public DbTable(Table table)
        {
            Schema = table.Schema;
            Name = table.Name;
            Script = string.Join("\n", table.Script().ToList<string>());
            Columns = new List<DbColumn>();
            foreach (Column column in table.Columns)
            {
                Columns.Add(new DbColumn(column));
            }
        }

        public string Schema { get; set; }

        public string Name { get; set; }

        public List<DbColumn> Columns { get; set; }

        public string Script { get; set; }
    }
}