﻿using System;
using System.Data;
using Microsoft.SqlServer.Management.Smo;
using Syncfusion.Data.Extensions;

namespace Rogler.Techno.SqlManagement.Forms.Models.Db
{
    public class DbTrigger
    {
        public DbTrigger(DataRow row)
        {
            TableName = row.Field<string>("TableName");
            TriggerName = row.Field<string>("TriggerName");
            TriggerText = row.Field<string>("TriggerText");
            CreatedDate = row.Field<DateTime>("CreatedDate");
            AfterAction = row.Field<string>("AfterAction");
        }

        public DbTrigger(Trigger trigger)
        {
            TriggerName = trigger.Name;
            TableName = trigger.Parent.Urn.Value;
            CreatedDate = trigger.CreateDate;
            TriggerText = string.Join("\n", trigger.Script().ToList<string>());
            if (trigger.Insert) AfterAction = "INSERT";
            else if (trigger.Delete) AfterAction = "DELETE";
            else if (trigger.Update) AfterAction = "UPDATE";
        }

        public string TableName { get; set; }

        public string TriggerName { get; set; }

        public string TriggerText { get; set; }

        public DateTime CreatedDate { get; set; }

        public string AfterAction { get; set; }
    }
}