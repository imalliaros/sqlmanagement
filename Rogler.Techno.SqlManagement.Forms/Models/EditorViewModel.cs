﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Models.Db;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class EditorViewModel : INotifyPropertyChanged
    {
        private bool _hasOutput = false;
        private bool _splitterCollapsed=true;

        public IList<ParseError> ParseErrors { get; set; }
        public List<MergeFilesModel> Output { get; set; }
        public SqlTreeNode Root { get; set; }

        public Dictionary<string, DbObjectDescription> ObjectsDescription { get; set; }


        public bool HasOutput
        {
            get => _hasOutput;
            set
            {
                if (value == _hasOutput) return;
                _hasOutput = value;
                OnPropertyChanged();
            }
        }

        public bool SplitterCollapsed
        {
            get => _splitterCollapsed;
            set
            {
                if (value == _splitterCollapsed) return;
                _splitterCollapsed = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
