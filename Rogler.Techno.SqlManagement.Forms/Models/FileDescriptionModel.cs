﻿namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class FileDescriptionModel
    {

        public string FileName { get; set; }
        public bool Create { get; set; } = false;
        public bool Alter { get; set; } = false;
        public bool Drop { get; set; } = false;
    }
}