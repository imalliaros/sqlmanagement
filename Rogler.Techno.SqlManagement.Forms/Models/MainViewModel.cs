﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Rogler.Techno.SqlManagement.Forms.Controls;
using Syncfusion.Windows.Forms.Tools;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private EditorViewModel CurrentEditorViewModel { get; set; }
        private MergeListViewModel CurrentMergeViewModel { get; set; }

        public MainViewModel()
        {
        }

        private CurrentControlEnum _currentControl = CurrentControlEnum.None;
        private int _editorTabIndex;

        public CurrentControlEnum CurrentControl
        {
            get => _currentControl;
            set
            {
                if (value == _currentControl) return;
                _currentControl = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ShowFileGroup));
                OnPropertyChanged(nameof(ShowEditorGroup));
                OnPropertyChanged(nameof(ShowCompileGroup));
                OnPropertyChanged(nameof(ShowExportGroup));
            }
        }

        public bool ShowFileGroup => CurrentControl == CurrentControlEnum.Editor;

        public bool ShowEditorGroup => CurrentControl == CurrentControlEnum.Editor;

        public bool ShowCompileGroup => CurrentControl == CurrentControlEnum.Editor;

        public bool ShowMergeGroup => CurrentControl == CurrentControlEnum.MergeList;

        public bool ShowEditMergeGroup => CurrentControl == CurrentControlEnum.MergeList;

        public bool ShowExportGroup => CurrentControl == CurrentControlEnum.Log || (CurrentControl == CurrentControlEnum.Editor && CurrentEditorViewModel?.HasOutput==true);

        public bool IsEditMergeListEnabled => CurrentMergeViewModel != null && CurrentMergeViewModel.IsEditMode == true;

        public int EditorTabIndex
        {
            get => _editorTabIndex;
            set
            {
                if (value == _editorTabIndex) return;
                _editorTabIndex = value;
                if (value != -1)
                {
                    var control = Program.MainForm.EditorTabControl.SelectedTab?.Controls[0];
                    if (control != null)
                    {
                        if (CurrentEditorViewModel != null)
                        {
                            CurrentEditorViewModel.PropertyChanged -= CurrentEditorViewModelOnPropertyChanged;
                            
                        }
                        switch (control)
                        {
                            case SqlEditor editor:
                                CurrentControl = CurrentControlEnum.Editor;
                                CurrentEditorViewModel = editor.ViewModel;
                                CurrentEditorViewModel.PropertyChanged += CurrentEditorViewModelOnPropertyChanged;                                
                                break;
                            case MergeList list:
                                CurrentControl = CurrentControlEnum.MergeList;
                                CurrentMergeViewModel = list.ViewModel;
                                CurrentMergeViewModel.PropertyChanged += CurrentMergeViewModelOnPropertyChanged;
                                break;
                            case MergeLog _:
                                CurrentControl = CurrentControlEnum.Log;
                                break;
                            default:
                                CurrentControl = CurrentControlEnum.None;
                                break;
                        }
                    }
                }
                else CurrentControl = CurrentControlEnum.None;
                OnPropertyChanged();
            }
        }

        private void CurrentMergeViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName==nameof(MergeListViewModel.IsEditMode)) OnPropertyChanged(nameof(IsEditMergeListEnabled));
        }

        private void CurrentEditorViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if(args.PropertyName==nameof(EditorViewModel.HasOutput)) OnPropertyChanged(nameof(ShowExportGroup));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}