﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using JetBrains.Annotations;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class MergeFilesModel : INotifyPropertyChanged
    {
        private string _name;
        private SqlTreeNodeType _nodeType;
        private string _fileName;
        private IList<ParseFileError> _parseErrors;
        private string _schemaName;
        private bool _containsGo;
        private string _originalFileName;
        private int _column;
        private int _line;
        private int _offset;
        private bool _dropCreated;
        private int _id;

        [XmlIgnore]
        public int Id {
            get => _id;
            set
            {
                if (value == _id) return;
                _id = value;
                OnPropertyChanged();
            }
        }

        [XmlIgnore]
        public IList<ParseFileError> ParseErrors
        {
            get => _parseErrors;
            set
            {
                if (Equals(value, _parseErrors)) return;
                _parseErrors = value;
                OnPropertyChanged();
            }
        }

        public string SchemaName
        {
            get => _schemaName;
            set
            {
                if (value == _schemaName) return;
                _schemaName = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }


        public SqlTreeNodeType NodeType
        {
            get => _nodeType;
            set
            {
                if (value == _nodeType) return;
                _nodeType = value;
                OnPropertyChanged();
            }
        }


        public string FileName
        {
            get => _fileName;
            set
            {
                if (value == _fileName) return;
                _fileName = value;
                OnPropertyChanged();
            }
        }

        public bool ContainsGo
        {
            get => _containsGo;
            set
            {
                if (value == _containsGo) return;
                _containsGo = value;
                OnPropertyChanged();
            }
        }

        [ReadOnly(true)]
        public string OriginalFileName
        {
            get => _originalFileName;
            set
            {
                if (value == _originalFileName) return;
                _originalFileName = value;
                OnPropertyChanged();
            }
        }

        [ReadOnly(true)]
        public int Column
        {
            get => _column;
            set
            {
                if (value == _column) return;
                _column = value;
                OnPropertyChanged();
            }
        }

        [ReadOnly(true)]
        public int Line
        {
            get => _line;
            set
            {
                if (value == _line) return;
                _line = value;
                OnPropertyChanged();
            }
        }

        [ReadOnly(true)]
        public int Offset
        {
            get => _offset;
            set
            {
                if (value == _offset) return;
                _offset = value;
                OnPropertyChanged();
            }
        }

        public bool DropCreated
        {
            get => _dropCreated;
            set
            {
                if (value == _dropCreated) return;
                _dropCreated = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}