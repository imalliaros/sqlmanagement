﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class MergeListViewModel : INotifyPropertyChanged
    {
        private bool _isEditMode = false;
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsEditMode
        {
            get => _isEditMode;
            set
            {
                if (value == _isEditMode) return;
                _isEditMode = value;
                OnPropertyChanged();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}