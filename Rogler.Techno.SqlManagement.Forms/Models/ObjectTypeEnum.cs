﻿namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public enum ObjectTypeEnum
    {
        StoreProcedure,
        Function,
        Table,
        View,
        Trigger,
        Other
    }
}