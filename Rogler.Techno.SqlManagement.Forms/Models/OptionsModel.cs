﻿using System.ComponentModel;
using Rogler.Techno.SqlManagement.Forms.Properties;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class OptionsModel
    {

        [DisplayName("Prepend Schema Name")]
        [Category("Split")]
        [Description("If true prepends schema name to the function name.")]
        public bool PrependSchemaName {
            get => Settings.Default.PrependSchemaName;
            set => Settings.Default.PrependSchemaName = value;
        }

        [DisplayName("Schema Separator")]
        [Category("Split")]
        [Description("The schema and function name separator.")]
        public string SchemaSeparator
        {
            get => Settings.Default.SchemaSeparator;
            set => Settings.Default.SchemaSeparator = value;
        }

        [DisplayName("Alter Literal")]
        [Category("Split")]
        [Description("The literal that is added to the alter statement files.")]
        public string AlterLiteral
        {
            get => Settings.Default.AlterLiteral;
            set => Settings.Default.AlterLiteral = value;
        }

        [DisplayName("Insert Literal")]
        [Category("Split")]
        [Description("The literal that is added to the insert statement files.")]
        public string InsertLiteral
        {
            get => Settings.Default.InsertLiteral;
            set => Settings.Default.InsertLiteral = value;
        }

        [DisplayName("Other Literal")]
        [Category("Split")]
        [Description("The literal that is added to the rest of statement files.")]
        public string OtherLiteral
        {
            get => Settings.Default.OtherLiteral;
            set => Settings.Default.OtherLiteral = value;
        }

        [DisplayName("Create File Name")]
        [Category("Merge")]
        [Description("The name of the file with the create statements.")]
        public string CreateFile
        {
            get => Settings.Default.CreateFile;
            set => Settings.Default.CreateFile = value;
        }

        [DisplayName("Delete file Name")]
        [Category("Merge")]
        [Description("The name of the file with the delete statements.")]
        public string DeleteFile
        {
            get => Settings.Default.DeleteFile;
            set => Settings.Default.DeleteFile = value;
        }

        [DisplayName("Generate If Exists statements")]
        [Category("Merge")]
        [Description("Generate If Exists statements when generating the Delete File")]
        public bool GenerateIfExistsStatements
        {
            get => Settings.Default.GenerateIfExistsStatements;
            set => Settings.Default.GenerateIfExistsStatements = value;
        }

        [DisplayName("Default Schema Name")]
        [Category("Merge")]
        [Description("Default schema name for create and delete statements")]
        public string DefaultSchemaName
        {
            get => Settings.Default.DefaultSchemaName;
            set => Settings.Default.DefaultSchemaName = value;
        }

        [DisplayName("Generate Drop for Create Table")]
        [Category("Merge")]
        [Description("Generate Drop Statements for the Create Table")]
        public bool GenerateDropForCreateTable
        {
            get => Settings.Default.GenerateDropForCreateTable;
            set => Settings.Default.GenerateDropForCreateTable = value;
        }

        [DisplayName("Generate Drop for Alter Table")]
        [Category("Merge")]
        [Description("Generate Drop Statements for the Alter Table")]
        public bool GenerateDropForAlterTable
        {
            get => Settings.Default.GenerateDropForAlterTable;
            set => Settings.Default.GenerateDropForAlterTable = value;
        }

        [DisplayName("Generate Drop for Alter Statements")]
        [Category("Merge")]
        [Description("Generate Drop Statements for the Alter Statements")]
        public bool GenerateDropForAlterStatements
        {
            get => Settings.Default.GenerateDropForAlterStatements;
            set => Settings.Default.GenerateDropForAlterStatements = value;
        }

        [DisplayName("Mark Multiple Statements as Error")]
        [Category("Common")]
        [Description("Mark multiple create statements inside the same batch as error")]
        public bool MarkMultipleStatementsAsError
        {
            get => Settings.Default.MarkMultipleStatementsAsError;
            set => Settings.Default.MarkMultipleStatementsAsError = value;
        }

        [DisplayName("Remember Connections")]
        [Category("Common")]
        [Description("Preserve past connection details")]
        public bool RememberConnections
        {
            get => Settings.Default.RememberConnections;
            set
            {
                Settings.Default.RememberConnections = value;
                if (value == false)
                {
                    Settings.Default.ConnectionHistory.Clear();
                }
            }
        }

        [DisplayName("Compile after merge")]
        [Category("Merge")]
        [Description("Compile merged file for potential errors")]
        public bool CompileAfterMerge
        {
            get => Settings.Default.CompileAfterMerge;
            set => Settings.Default.CompileAfterMerge = value;
        }

        [DisplayName("Preserve Order")]
        [Category("Common")]
        [Description("Preserve the procedure sequence order on merge. This will generate an index file on split that will hold the order of the procedure creation.")]
        public bool PreserveOrder
        {
            get => Settings.Default.PreserveOrder;
            set => Settings.Default.PreserveOrder = value;
        }

        [DisplayName("Index File")]
        [Category("Common")]
        [Description("The index file with the procedure creation order. It is a text file and it is posible to alter it manually.")]
        public string IndexFile
        {
            get => Settings.Default.IndexFile;
            set => Settings.Default.IndexFile = value;
        }

        [DisplayName("Execute Literal")]
        [Category("Split")]
        [Description("The literal that is added to the execute statement files.")]
        public string ExecuteLiteral
        {
            get => Settings.Default.ExecuteLiteral;
            set => Settings.Default.ExecuteLiteral = value;
        }

        [DisplayName("Update Literal")]
        [Category("Split")]
        [Description("The literal that is added to the update statement files.")]
        public string UpdateLiteral
        {
            get => Settings.Default.UpdateLiteral;
            set => Settings.Default.UpdateLiteral = value;
        }

        [DisplayName("Set Literal")]
        [Category("Split")]
        [Description("The literal that is added to the set statement files.")]
        public string SetLiteral
        {
            get => Settings.Default.SetLiteral;
            set => Settings.Default.SetLiteral = value;
        }

        [DisplayName("Create Procedure Literal")]
        [Category("Split")]
        [Description("The literal that is added to the create  statement files.")]
        public string CreateLiteral
        {
            get => Settings.Default.CreateLiteral;
            set => Settings.Default.CreateLiteral = value;
        }
    }
}