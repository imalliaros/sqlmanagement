﻿using System.ComponentModel;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public enum OutputRecordEnum2
    {
        [Description("Create Function")]
        CreateFunction,

        [Description("Create Procedure")]
        CreateProcedure,

        [Description("Create Trigger")]
        CreateTrigger,

        [Description("Create View")]
        CreateView,

        [Description("Create Table")]
        CreateTable,

        [Description("Alter View")]
        AlterView,

        [Description("Alter Procedure")]
        AlterProcedure,

        [Description("Alter Function")]
        AlterFunction,

        [Description("Alter Table")]
        AlterTable,

        [Description("Insert Statement")]
        Insert,

        [Description("Execute Statement")]
        Execute,

        [Description("Update Statement")]
        Update,

        [Description("Other Statements")]
        Other
    }
}