﻿using System;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class ParseFileError
    {
        private readonly ParseError _error;

        public ParseFileError(ParseError error, string filename)
        {
            _error = error ?? throw new ArgumentNullException(nameof(error));
            FileName = filename;
        }

        public string FileName { get; }

        public int Number => _error.Number;

        public int Line => _error.Line;

        public int Column => _error.Column;

        public string Message => _error.Message;

        public int Offset => _error.Offset;

    }
}