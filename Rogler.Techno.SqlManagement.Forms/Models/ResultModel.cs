﻿using Rogler.Techno.SqlManagement.Forms.Models.Db;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class ResultModel
    {
        public string Name { get; set; }
        public ObjectTypeEnum ObjectType { get; set; }
        public bool Create { get; set; } = false;
        public bool Alter { get; set; } = false;
        public bool Drop { get; set; } = false;
    }
}