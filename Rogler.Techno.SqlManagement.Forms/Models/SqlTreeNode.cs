﻿using System.Collections.Generic;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public class SqlTreeNode
    {
        public SqlTreeNode()
        {
            Children = new List<SqlTreeNode>();
        }

        public SqlTreeNodeType TreeType { get; set; }

        public string Name { get; set; }

        public string Script { get; set; }

        public List<SqlTreeNode> Children { get; set; }

        public int Column { get; set; }

        public int Line { get; set; }
    }
}