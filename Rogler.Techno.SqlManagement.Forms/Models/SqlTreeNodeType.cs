﻿using System.ComponentModel;

namespace Rogler.Techno.SqlManagement.Forms.Models
{
    public enum SqlTreeNodeType
    {
        [Description("File")]
        File,

        [Description("Batch")]
        Batch,

        [Description("Set Statement")]
        SetStatement,

        [Description("Insert Statement")]
        InsertStatement,
        
        [Description("Create Procedure")]
        CreateProcedureStatement,

        [Description("Alter Procedure")]
        AlterProcedureStatement,

        [Description("Create Function")]
        CreateFunctionStatement,

        [Description("Alter Function")]
        AlterFunctionStatement,

        [Description("Create Trigger")]
        CreateTriggerStatement,

        [Description("Create View")]
        CreateViewStatement,

        [Description("Alter View")]
        AlterViewStatement,

        [Description("Create Table")]
        CreateTableStatement,

        [Description("Alter Table")]
        AlterTableStatement,

        [Description("Execute Statement")]
        ExecuteStatement,

        [Description("Update Statement")]
        UpdateStatement,

        [Description("Other Statement")]
        OtherStatement

        
    }
}