#region Copyright Syncfusion Inc. 2001-2018.
// Copyright Syncfusion Inc. 2001-2018. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Forms;
using Rogler.Techno.SqlManagement.Forms.Properties;
using Rogler.Techno.SqlManagement.Forms.Windows;
using Syncfusion.Licensing;

namespace Rogler.Techno.SqlManagement.Forms
{
    static class Program
    {
        public static MainForm MainForm { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if(Settings.Default.ConnectionHistory==null) Settings.Default.ConnectionHistory = new StringCollection();
            SyncfusionLicenseProvider.RegisterLicense("MTc1MDVAMzEzNjJlMzIyZTMwZWcxMWtFNkh3Zjc0SmhRWWJEellCb2E0dDZkUmVYSzAralFDNXhqVFF4TT0=");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm = new MainForm();
            Application.Run(MainForm);
        }
    }
}
