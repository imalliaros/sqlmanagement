﻿using Rogler.Techno.SqlManagement.Forms.Models;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rogler.Techno.SqlManagement.Forms.Providers
{
    class FileNameFormatProvider : IDataGridFormatProvider
    {
        public object GetFormat(Type formatType)
        {
            return this;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            throw new NotImplementedException();
        }

        public object Format(string format, GridColumnBase gridColumn, object record, object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            if (gridColumn is GridColumn column && column.MappingName == "FileName")
            {
                if (record is MergeFilesModel outputRecord)
                {
                    return Path.GetFileName(outputRecord.FileName);
                }
                else if (record is DataRowView rowView)
                {
                    string filename = rowView.Row["FileName"].ToString();
                    return Path.GetFileName(filename);
                }
            }

            return value.ToString();
        }
    }
}
