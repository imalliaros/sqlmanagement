﻿using System;
using System.Data;
using Rogler.Techno.SqlManagement.Forms.Extensions;
using Rogler.Techno.SqlManagement.Forms.Models;
using Syncfusion.WinForms.DataGrid;

namespace Rogler.Techno.SqlManagement.Forms.Providers
{
    public class ObjectTypeFormatProvider : IDataGridFormatProvider
    {
        public object GetFormat(Type formatType)
        {
            return this;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            throw new NotImplementedException();
        }

        public object Format(string format, GridColumnBase gridColumn, object record, object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            if (gridColumn is GridColumn column && column.MappingName == "NodeType")
            {
                if (record is MergeFilesModel outputRecord)
                {
                    return outputRecord.NodeType.GetDescription();
                }
                else if(record is DataRowView rowView)
                {
                    SqlTreeNodeType recordEnum = (SqlTreeNodeType)Enum.Parse(typeof(SqlTreeNodeType), rowView.Row["NodeType"].ToString());
                    return recordEnum.GetDescription();
                }
            }

            return value.ToString();
        }
    }
}