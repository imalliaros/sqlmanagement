﻿using System;
using System.Drawing;
using System.Globalization;
using Rogler.Techno.SqlManagement.Forms.Extensions;
using Rogler.Techno.SqlManagement.Forms.Models;
using Syncfusion.Data;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Renderers;
using Syncfusion.WinForms.DataGrid.Styles;
using Syncfusion.WinForms.GridCommon.ScrollAxis;

namespace Rogler.Techno.SqlManagement.Forms.Providers
{
    public class ObjectTypeSummaryCellRenderer : GridCaptionSummaryCellRenderer
    {        
        protected override void OnRender(Graphics paint, Rectangle cellRect, string cellValue,
            CellStyleInfo style, DataColumnBase column, RowColumnIndex rowColumnIndex)
        {
            if (string.IsNullOrEmpty(cellValue))
                return;
            StringFormat stringFormat = new StringFormat();
            stringFormat.LineAlignment = StringAlignment.Center;
            stringFormat.Alignment = StringAlignment.Near;
            if (column.GridColumn.MappingName == "NodeType")
            {
                string[] values = cellValue.Split(':');
                if(Enum.TryParse(values[0], true, out SqlTreeNodeType recordEnum))
                {
                    cellValue = recordEnum.GetDescription() + ": " + values[1];
                }
                //OutputRecordEnum value= (OutputRecordEnum)Enum.Parse(typeof(OutputRecordEnum), values[0]);
                
            }
            paint.DrawString(cellValue, style.Font.GetFont(), Brushes.Black, cellRect, stringFormat);
        }
    }
}