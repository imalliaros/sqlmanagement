﻿using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class CheckForm : Rogler.Techno.SqlManagement.Forms.Windows.WaitForm
    {
        public IList<ParseError> ParseErrors;
        private readonly string _text;

        public CheckForm(string text)
        {
            InitializeComponent();
            _text = text;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorkerOnDoWork;
        }

        private void WaitBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                TSql140Parser parser = new TSql140Parser(true);

                string sql = _text;
                TSqlFragment result = parser.Parse(new StringReader(sql), out ParseErrors);

                if (Settings.Default.MarkMultipleStatementsAsError && result is TSqlScript sqlCheck)
                {
                    foreach (TSqlBatch sqlBatch in sqlCheck.Batches)
                    {
                        List<TSqlStatement> statements = new List<TSqlStatement>();
                        foreach (TSqlStatement statement in sqlBatch.Statements)
                        {
                            if (statement is CreateProcedureStatement || statement is CreateFunctionStatement)
                                statements.Add(statement);
                        }

                        for (int i = 1; i < statements.Count; ++i)
                        {

                            TSqlStatement statement = sqlBatch.Statements[i];
                            ParseErrors.Add(new ParseError(Constants.MultipleStatementsError, statement.StartOffset,
                                statement.StartLine, statement.StartColumn, Resources.MultipleStatementsError));
                        }
                    }
                }

                if (ParseErrors.Count > 0)
                {
                    e.Result = DialogResult.Abort;
                    return;
                }

                if (WaitBackgroundWorker.CancellationPending)
                {
                    e.Cancel = true;
                    e.Result = DialogResult.Cancel;
                    return;
                }

            }

            catch (Exception exc)
            {
                ParseErrors.Add(new ParseError(0, 0, 0, 0, exc.Message));
                e.Result = DialogResult.Abort;
            }

            e.Result = DialogResult.OK;
        }

    }
}
