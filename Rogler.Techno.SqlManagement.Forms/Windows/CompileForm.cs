﻿using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Rogler.Techno.SqlManagement.Forms.Extensions;
using Rogler.Techno.SqlManagement.Forms.Models.Db;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class CompileForm : Rogler.Techno.SqlManagement.Forms.Windows.WaitForm
    {
        public IList<ParseError> ParseErrors;
        private readonly string _inputFile;
        private readonly string _text;
        public List<MergeFilesModel> Output { get; set; } = new List<MergeFilesModel>();
        public SqlTreeNode Root { get; set; } = null;

        public Dictionary<string, DbObjectDescription> ObjectsDescription { get; set; } =
            new Dictionary<string, DbObjectDescription>();

        public List<ResultModel> Result
        {
            get
            {
                List<ResultModel> list = new List<ResultModel>();
                foreach (var descriptionValue in ObjectsDescription.Values)
                {
                    ResultModel model = new ResultModel
                    {
                        Name = descriptionValue.Name
                    };
                    if (!descriptionValue.Details.Any()) continue;
                    foreach (FileDescriptionModel detail in descriptionValue.Details)
                    {
                        if (detail.Create) model.Create = true;
                        if (detail.Alter) model.Alter = true;
                        if (detail.Drop) model.Drop = true;
                    }

                    list.Add(model);
                }

                return list;
            }
        }


        public CompileForm(string inputFile, string text)
        {
            InitializeComponent();
            _inputFile = inputFile;
            _text = text;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorkerOnDoWork;
        }

        private void WaitBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                TSql140Parser parser = new TSql140Parser(true);
                string sql = _text;
                TSqlFragment result = parser.Parse(new StringReader(sql), out ParseErrors);


                if (Settings.Default.MarkMultipleStatementsAsError && result is TSqlScript sqlCheck)
                {
                    foreach (TSqlBatch sqlBatch in sqlCheck.Batches)
                    {
                        List<TSqlStatement> statements = new List<TSqlStatement>();
                        foreach (TSqlStatement statement in sqlBatch.Statements)
                        {
                            if (statement is CreateProcedureStatement || statement is CreateFunctionStatement)
                                statements.Add(statement);
                        }

                        for (int i = 1; i < statements.Count; ++i)
                        {

                            TSqlStatement statement = sqlBatch.Statements[i];
                            ParseErrors.Add(new ParseError(Constants.MultipleStatementsError, statement.StartOffset,
                                statement.StartLine, statement.StartColumn, Resources.MultipleStatementsError));
                        }
                    }
                }

                if (ParseErrors.Count > 0)
                {
                    e.Result = DialogResult.Abort;
                    return;
                }

                if (WaitBackgroundWorker.CancellationPending)
                {
                    e.Cancel = true;
                    e.Result = DialogResult.Cancel;
                    return;
                }

                if (result is TSqlScript sqlScript)
                {
                    WaitBackgroundWorker.ReportProgress(0);
                    int currentBath = 0;

                    Root = new SqlTreeNode
                    {
                        Name = _inputFile,
                        TreeType = SqlTreeNodeType.File,
                        Script = sql
                    };

                    foreach (TSqlBatch sqlBatch in sqlScript.Batches)
                    {
                        if (WaitBackgroundWorker.CancellationPending)
                        {
                            e.Cancel = true;
                            e.Result = DialogResult.Cancel;
                            return;
                        }

                        SqlTreeNode batchNode = new SqlTreeNode
                        {
                            Name = "Batch",
                            TreeType = SqlTreeNodeType.Batch,
                            Script = Globals.GetScript(sql, result)
                        };
                        Root.Children.Add(batchNode);

                        for (int index = 0; index < sqlBatch.Statements.Count; index++)
                        {
                            TSqlStatement sqlStatement = sqlBatch.Statements[index];
                            StringBuilder statementName = new StringBuilder();
                            switch (sqlStatement)
                            {
                                case PredicateSetStatement setStatement:

                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = Settings.Default.SetLiteral + " " +
                                               Globals.ToTitleCaseFromPascal(setStatement.Options.ToString()) + "=" +
                                               (setStatement.IsOn ? "ON" : "OFF"),
                                        TreeType = SqlTreeNodeType.SetStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case InsertStatement insertStatement:
                                {
                                    string schema = Settings.Default.DefaultSchemaName;
                                    if (insertStatement.InsertSpecification.Target is NamedTableReference tableReference
                                    )
                                    {
                                        if (tableReference.SchemaObject.SchemaIdentifier != null)
                                        {
                                            schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                        }

                                        if (Settings.Default.PrependSchemaName)
                                        {
                                            statementName.Append(Settings.Default.SchemaSeparator);
                                            statementName.Append(schema);
                                        }

                                        statementName.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                    }
                                    else
                                    {
                                        statementName.Append(Settings.Default.InsertLiteral);
                                    }


                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = statementName.ToString(),
                                        SchemaName = schema,
                                        NodeType = SqlTreeNodeType.InsertStatement,
                                        Column = insertStatement.StartColumn,
                                        Line = insertStatement.StartLine,
                                        Offset = insertStatement.StartOffset,
                                        FileName = _inputFile,
                                        OriginalFileName = _inputFile
                                    });

                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = Settings.Default.InsertLiteral + " (" + statementName.ToString() + ")",
                                        TreeType = SqlTreeNodeType.InsertStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });

                                    break;
                                }

                                case CreateProcedureStatement procedureStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value,
                                        SchemaName =
                                            procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        NodeType = SqlTreeNodeType.CreateProcedureStatement,
                                        Column = procedureStatement.StartColumn,
                                        Line = procedureStatement.StartLine,
                                        Offset = procedureStatement.StartOffset,
                                        FileName = _inputFile,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Create Procedure (" +
                                               procedureStatement.ProcedureReference.Name.BaseIdentifier.Value + ")",
                                        TreeType = SqlTreeNodeType.CreateProcedureStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case AlterProcedureStatement procedureStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value,
                                        SchemaName =
                                            procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        NodeType = SqlTreeNodeType.AlterProcedureStatement,
                                        Column = procedureStatement.StartColumn,
                                        Line = procedureStatement.StartLine,
                                        Offset = procedureStatement.StartOffset,
                                        FileName = _inputFile,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Alter Procedure (" +
                                               procedureStatement.ProcedureReference.Name.BaseIdentifier.Value + ")",
                                        TreeType = SqlTreeNodeType.AlterProcedureStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case CreateFunctionStatement functionStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = functionStatement.Name.BaseIdentifier.Value,
                                        SchemaName =
                                            functionStatement.Name.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.CreateFunctionStatement,
                                        Column = functionStatement.StartColumn,
                                        Line = functionStatement.StartLine,
                                        Offset = functionStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Create Function (" + functionStatement.Name.BaseIdentifier.Value + ")",
                                        TreeType = SqlTreeNodeType.CreateFunctionStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case AlterFunctionStatement functionStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = functionStatement.Name.BaseIdentifier.Value,
                                        SchemaName =
                                            functionStatement.Name.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.AlterFunctionStatement,
                                        Column = functionStatement.StartColumn,
                                        Line = functionStatement.StartLine,
                                        Offset = functionStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Alter Function (" + functionStatement.Name.BaseIdentifier.Value + ")",
                                        TreeType = SqlTreeNodeType.AlterFunctionStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case CreateTriggerStatement triggerStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = triggerStatement.Name.BaseIdentifier.Value,
                                        SchemaName =
                                            triggerStatement.Name.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.CreateTriggerStatement,
                                        Column = triggerStatement.StartColumn,
                                        Line = triggerStatement.StartLine,
                                        Offset = triggerStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Create Trigger (" + triggerStatement.Name.BaseIdentifier.Value + ")",
                                        TreeType = SqlTreeNodeType.CreateTriggerStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case CreateViewStatement viewStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = viewStatement.SchemaObjectName.BaseIdentifier.Value,
                                        SchemaName =
                                            viewStatement.SchemaObjectName.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.CreateViewStatement,
                                        Column = viewStatement.StartColumn,
                                        Line = viewStatement.StartLine,
                                        Offset = viewStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Create View (" + viewStatement.SchemaObjectName.BaseIdentifier.Value +
                                               ")",
                                        TreeType = SqlTreeNodeType.CreateViewStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case AlterViewStatement viewStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = viewStatement.SchemaObjectName.BaseIdentifier.Value,
                                        SchemaName =
                                            viewStatement.SchemaObjectName.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.AlterViewStatement,
                                        Column = viewStatement.StartColumn,
                                        Line = viewStatement.StartLine,
                                        Offset = viewStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Alter View (" + viewStatement.SchemaObjectName.BaseIdentifier.Value +
                                               ")",
                                        TreeType = SqlTreeNodeType.AlterViewStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case CreateTableStatement tableStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = tableStatement.SchemaObjectName.BaseIdentifier.Value,
                                        SchemaName =
                                            tableStatement.SchemaObjectName.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.CreateTableStatement,
                                        Column = tableStatement.StartColumn,
                                        Line = tableStatement.StartLine,
                                        Offset = tableStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Create Table (" + tableStatement.SchemaObjectName.BaseIdentifier.Value +
                                               ")",
                                        TreeType = SqlTreeNodeType.CreateTableStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case AlterTableStatement tableStatement:
                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = tableStatement.SchemaObjectName.BaseIdentifier.Value,
                                        SchemaName =
                                            tableStatement.SchemaObjectName.SchemaIdentifier?.Value ??
                                            Settings.Default.DefaultSchemaName,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.AlterTableStatement,
                                        Column = tableStatement.StartColumn,
                                        Line = tableStatement.StartLine,
                                        Offset = tableStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Alter Table (" + tableStatement.SchemaObjectName.BaseIdentifier.Value +
                                               ")",
                                        TreeType = SqlTreeNodeType.AlterTableStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;
                                case ExecuteStatement executeStatement:
                                {
                                    string schema = Settings.Default.DefaultSchemaName;
                                    if (executeStatement.ExecuteSpecification.ExecutableEntity is
                                        ExecutableProcedureReference procedureReference)
                                    {
                                        if (procedureReference.ProcedureReference.ProcedureReference.Name
                                                .SchemaIdentifier != null)
                                        {
                                            schema = procedureReference.ProcedureReference.ProcedureReference.Name
                                                .SchemaIdentifier.Value;
                                        }

                                        if (Settings.Default.PrependSchemaName)
                                        {
                                            statementName.Append(Settings.Default.SchemaSeparator);
                                            statementName.Append(schema);
                                        }

                                        statementName.Append(procedureReference.ProcedureReference.ProcedureReference
                                            .Name.BaseIdentifier.Value);
                                    }
                                    else
                                    {
                                        statementName.Append(Settings.Default.ExecuteLiteral);
                                    }

                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = statementName.ToString(),
                                        SchemaName = schema,
                                        NodeType = SqlTreeNodeType.ExecuteStatement,
                                        Column = executeStatement.StartColumn,
                                        Line = executeStatement.StartLine,
                                        Offset = executeStatement.StartOffset,
                                        FileName = _inputFile,
                                        OriginalFileName = _inputFile
                                    });

                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Execute (" + statementName.ToString() + ")",
                                        TreeType = SqlTreeNodeType.ExecuteStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });

                                    break;
                                }
                                case UpdateStatement updateStatement:
                                {
                                    string schema = Settings.Default.DefaultSchemaName;
                                    if (updateStatement.UpdateSpecification.Target is NamedTableReference tableReference
                                    )
                                    {
                                        if (tableReference.SchemaObject.SchemaIdentifier != null)
                                        {
                                            schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                        }

                                        if (Settings.Default.PrependSchemaName)
                                        {
                                            statementName.Append(Settings.Default.SchemaSeparator);
                                            statementName.Append(schema);
                                        }

                                        statementName.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                    }
                                    else
                                    {
                                        statementName.Append(Settings.Default.UpdateLiteral);
                                    }


                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = statementName.ToString(),
                                        SchemaName = schema,
                                        NodeType = SqlTreeNodeType.UpdateStatement,
                                        Column = updateStatement.StartColumn,
                                        Line = updateStatement.StartLine,
                                        Offset = updateStatement.StartOffset,
                                        FileName = _inputFile,
                                        OriginalFileName = _inputFile
                                    });

                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Update (" + statementName.ToString() + ")",
                                        TreeType = SqlTreeNodeType.UpdateStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });

                                    break;

                                }
                                default:
                                {
                                    if (index < sqlBatch.Statements.Count - 1) break;
                                    string fn = (Settings.Default.OtherLiteral ?? "Other");

                                    Output.Add(new MergeFilesModel
                                    {
                                        Name = fn,
                                        FileName = _inputFile,
                                        NodeType = SqlTreeNodeType.OtherStatement,
                                        Column = sqlStatement.StartColumn,
                                        Line = sqlStatement.StartLine,
                                        Offset = sqlStatement.StartOffset,
                                        OriginalFileName = _inputFile
                                    });
                                    batchNode.Children.Add(new SqlTreeNode
                                    {
                                        Name = "Other (" + fn + ")",
                                        TreeType = SqlTreeNodeType.OtherStatement,
                                        Script = Globals.GetScript(sql, sqlStatement)
                                    });
                                    break;

                                }
                            }
                        }

                        double percent = ++currentBath / (double) sqlScript.Batches.Count;
                        WaitBackgroundWorker.ReportProgress((int) Math.Floor(percent * 100));

                    }
                    if (Output.Count > 0)
                    {
                        CreateResult();
                    }
                }

            }
            catch (Exception exc)
            {
                ParseErrors.Add(new ParseError(0, 0, 0, 0, exc.Message));
                e.Result = DialogResult.Abort;
            }

            e.Result = DialogResult.OK;
        }

        private void CreateResult()
        {
            foreach (MergeFilesModel file in Output)
            {
                if (!ObjectsDescription.ContainsKey(file.Name))
                {
                    ObjectsDescription.Add(file.Name, new DbObjectDescription
                    {
                        Name = file.Name
                    });
                }

                DbObjectDescription dbObject = ObjectsDescription[file.Name];
                if (dbObject != null)
                {
                    FileDescriptionModel fdmodel = new FileDescriptionModel
                    {
                        FileName = file.FileName
                    };
                    dbObject.ObjectType = file.NodeType.GetObjectType();
                    switch (file.NodeType)
                    {
                        case SqlTreeNodeType.CreateProcedureStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateFunctionStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateTableStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateTriggerStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateViewStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.AlterProcedureStatement:
                            fdmodel.Alter = true;
                            break;
                        case SqlTreeNodeType.AlterFunctionStatement:
                            fdmodel.Alter = true;
                            break;
                        case SqlTreeNodeType.AlterTableStatement:
                            fdmodel.Alter = true;
                            break;
                        case SqlTreeNodeType.AlterViewStatement:
                            fdmodel.Alter = true;
                            break;
                        default:
                            continue;
                    }

                    if (fdmodel.Alter || fdmodel.Create || fdmodel.Drop) dbObject.Details.Add(fdmodel);
                }
            }

        }
    }
}
