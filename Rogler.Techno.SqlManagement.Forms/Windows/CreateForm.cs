﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class CreateForm : Rogler.Techno.SqlManagement.Forms.Windows.WaitForm
    {
        private readonly List<MergeFilesModel> _files;
        private readonly string _outputDirectory;
        private readonly string _indexFile;
        public List<MergeFilesModel> SortedFiles { get; set; } = new List<MergeFilesModel>();

        public CreateForm(List<MergeFilesModel> files, string indexFile, string outputDirectory)
        {
            InitializeComponent();
            _files = files;
            _indexFile = indexFile;
            _outputDirectory = outputDirectory;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorkerOnDoWork;
        }

        private void WaitBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                StringBuilder createSql = new StringBuilder();
                StringBuilder deleteSql = new StringBuilder();
                int count = 0;
                WaitBackgroundWorker.ReportProgress(0);
                
                if (Settings.Default.PreserveOrder)
                {
                    List<MergeFilesModel> indexedFiles = Globals.ParseIndexFile(_indexFile);

                    foreach (MergeFilesModel file in indexedFiles)
                    {
                        MergeFilesModel mf = _files.FirstOrDefault(f => Path.GetFileName(f.FileName) == Path.GetFileName(file.FileName));
                        if (mf != null)
                        {
                            SortedFiles.Add(mf);
                            _files.Remove(mf);
                        }
                        else
                        {
                            Debugger.Break();
                        }
                    }
                    SortedFiles.AddRange(_files);
                }
                else
                {
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.CreateTableStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.CreateTriggerStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.AlterTableStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.CreateViewStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.AlterViewStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.CreateProcedureStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.AlterProcedureStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.CreateFunctionStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.AlterFunctionStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.InsertStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.UpdateStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.ExecuteStatement).ToList());
                    SortedFiles.AddRange(_files.Where(f => f.NodeType == SqlTreeNodeType.OtherStatement).ToList());
                }

                foreach (var file in SortedFiles)
                {
                    double percent = ++count / (double)SortedFiles.Count;
                    WaitBackgroundWorker.ReportProgress((int)Math.Floor(percent * 100));

                    if (!string.IsNullOrEmpty(Settings.Default.DeleteFile))
                    {
                        switch (file.NodeType)
                        {
                            case SqlTreeNodeType.CreateFunctionStatement:
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine(
                                        $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                    deleteSql.AppendLine("BEGIN");
                                }
                                deleteSql.AppendLine($"DROP FUNCTION [{file.SchemaName}].[{file.Name}]");
                                file.DropCreated = true;
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine("END");
                                    deleteSql.AppendLine("GO");
                                }
                                break;
                            case SqlTreeNodeType.CreateProcedureStatement:
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine(
                                        $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                    deleteSql.AppendLine("BEGIN");
                                }
                                deleteSql.AppendLine($"DROP PROCEDURE [{file.SchemaName}].[{file.Name}]");
                                file.DropCreated = true;
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine("END");
                                    deleteSql.AppendLine("GO");
                                }
                                break;
                            case SqlTreeNodeType.CreateTriggerStatement:
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine(
                                        $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                    deleteSql.AppendLine("BEGIN");
                                }
                                deleteSql.AppendLine($"DROP TRIGGER [{file.SchemaName}].[{file.Name}]");
                                file.DropCreated = true;
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine("END");
                                    deleteSql.AppendLine("GO");
                                }
                                break;
                            case SqlTreeNodeType.CreateViewStatement:
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine(
                                        $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                    deleteSql.AppendLine("BEGIN");
                                }
                                deleteSql.AppendLine($"DROP View [{file.SchemaName}].[{file.Name}]");
                                file.DropCreated = true;
                                if (Settings.Default.GenerateIfExistsStatements)
                                {
                                    deleteSql.AppendLine("END");
                                    deleteSql.AppendLine("GO");
                                }
                                break;
                            case SqlTreeNodeType.CreateTableStatement:
                                if (Settings.Default.GenerateDropForCreateTable)
                                {
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine(
                                            $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                        deleteSql.AppendLine("BEGIN");
                                    }
                                    deleteSql.AppendLine($"DROP Table [{file.SchemaName}].[{file.Name}]");
                                    file.DropCreated = true;
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine("END");
                                        deleteSql.AppendLine("GO");
                                    }
                                }
                                break;
                            case SqlTreeNodeType.AlterTableStatement:
                                if (Settings.Default.GenerateDropForAlterTable)
                                {
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine(
                                            $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                        deleteSql.AppendLine("BEGIN");
                                    }
                                    deleteSql.AppendLine($"DROP Table [{file.SchemaName}].[{file.Name}]");
                                    file.DropCreated = true;
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine("END");
                                        deleteSql.AppendLine("GO");
                                    }
                                }
                                break;
                            case SqlTreeNodeType.AlterFunctionStatement:
                                if (Settings.Default.GenerateDropForAlterStatements)
                                {
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine(
                                            $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                        deleteSql.AppendLine("BEGIN");
                                    }
                                    deleteSql.AppendLine($"DROP Function [{file.SchemaName}].[{file.Name}]");
                                    file.DropCreated = true;
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine("END");
                                        deleteSql.AppendLine("GO");
                                    }
                                }
                                break;
                            case SqlTreeNodeType.AlterProcedureStatement:
                                if (Settings.Default.GenerateDropForAlterStatements)
                                {
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine(
                                            $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                        deleteSql.AppendLine("BEGIN");
                                    }
                                    deleteSql.AppendLine($"DROP PROCEDURE [{file.SchemaName}].[{file.Name}]");
                                    file.DropCreated = true;
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine("END");
                                        deleteSql.AppendLine("GO");
                                    }
                                }
                                break;
                            case SqlTreeNodeType.AlterViewStatement:
                                if (Settings.Default.GenerateDropForAlterStatements)
                                {
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine(
                                            $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{file.SchemaName}].[{file.Name}]'))");
                                        deleteSql.AppendLine("BEGIN");
                                    }
                                    deleteSql.AppendLine($"DROP View [{file.SchemaName}].[{file.Name}]");
                                    file.DropCreated = true;
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine("END");
                                        deleteSql.AppendLine("GO");
                                    }
                                }
                                break;
                        }
                    }
                    using (var sr = new StreamReader(file.FileName))
                    {
                        createSql.AppendLine(sr.ReadToEnd());
                    }
                    if(file.ContainsGo==false) createSql.AppendLine("GO");
                }

                if (deleteSql.Length > 0)
                {
                    using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(_outputDirectory, Settings.Default.DeleteFile)))
                    {
                        outputFile.Write(deleteSql.ToString());
                    }
                }
                using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(_outputDirectory, Settings.Default.CreateFile)))
                {
                    outputFile.Write(Globals.ConvertToNewLine(createSql.ToString()));
                }

                DialogResult = DialogResult.OK;
            }
            catch(Exception exc)
            {
                DialogResult = DialogResult.Abort;
            }
        }


    }
}
