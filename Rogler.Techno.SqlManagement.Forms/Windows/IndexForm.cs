﻿using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class IndexForm : Rogler.Techno.SqlManagement.Forms.Windows.WaitForm
    {
        private readonly string _inputFile;
        public MergeFilesModel Model { get; set; } = new MergeFilesModel();

        public IndexForm(string inputfile)
        {
            InitializeComponent();
            _inputFile = inputfile;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorkerOnDoWork;
        }

        private void WaitBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            try
            {
                if (!File.Exists(_inputFile)) throw new Exception(Resources.FileDoesNotExist);
                Model.FileName = _inputFile;
                Model.OriginalFileName = _inputFile;
                TSql140Parser parser = new TSql140Parser(true);
                using (StreamReader sreader = new StreamReader(_inputFile))
                {
                    string sql = sreader.ReadToEnd();
                    TSqlFragment result = parser.Parse(new StringReader(sql), out IList<ParseError> parseErrors);
                    int otherNo = 0;
                    if (result is TSqlScript sqlScript)
                    {
                        for (int i = 0; i < sqlScript.Batches.Count; i++)
                        {
                            TSqlBatch sqlBatch = sqlScript.Batches[i];
                            for (int index = 0; index < sqlBatch.Statements.Count; index++)
                            {
                                TSqlStatement sqlStatement = sqlBatch.Statements[index];
                                switch (sqlStatement)
                                {
                                    case CreateProcedureStatement procedureStatement:
                                        Model.NodeType = SqlTreeNodeType.CreateProcedureStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value)
                                            ? procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value;
                                        break;
                                    case AlterProcedureStatement procedureStatement:
                                        Model.NodeType = SqlTreeNodeType.AlterProcedureStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value)
                                            ? procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value;
                                        break;
                                    case CreateFunctionStatement functionStatement:
                                        Model.NodeType = SqlTreeNodeType.CreateFunctionStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier?.Value)
                                            ? functionStatement.Name.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = functionStatement.Name.BaseIdentifier.Value;
                                        break;
                                    case AlterFunctionStatement functionStatement:
                                        Model.NodeType = SqlTreeNodeType.AlterFunctionStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier?.Value)
                                            ? functionStatement.Name.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = functionStatement.Name.BaseIdentifier.Value;
                                        break;
                                    case CreateTriggerStatement triggerStatement:
                                        Model.NodeType = SqlTreeNodeType.CreateTriggerStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(triggerStatement.Name.SchemaIdentifier?.Value)
                                            ? triggerStatement.Name.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = triggerStatement.Name.BaseIdentifier.Value;
                                        break;
                                    case CreateViewStatement viewStatement:
                                        Model.NodeType = SqlTreeNodeType.CreateViewStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                            ? viewStatement.SchemaObjectName.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = viewStatement.SchemaObjectName.BaseIdentifier.Value;
                                        break;
                                    case CreateTableStatement tableStatement:
                                        Model.NodeType = SqlTreeNodeType.CreateTableStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                            ? tableStatement.SchemaObjectName.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = tableStatement.SchemaObjectName.BaseIdentifier.Value;
                                        break;
                                    case AlterTableStatement tableStatement:
                                        Model.NodeType = SqlTreeNodeType.AlterTableStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                            ? tableStatement.SchemaObjectName.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = tableStatement.SchemaObjectName.BaseIdentifier.Value;
                                        break;
                                    case AlterViewStatement viewStatement:
                                        Model.NodeType = SqlTreeNodeType.AlterViewStatement;
                                        Model.SchemaName = !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                            ? viewStatement.SchemaObjectName.SchemaIdentifier.Value
                                            : Settings.Default.DefaultSchemaName;
                                        Model.Name = viewStatement.SchemaObjectName.BaseIdentifier.Value;
                                        break;
                                    case InsertStatement insertStatement:
                                        {
                                            StringBuilder fn = new StringBuilder(Settings.Default.InsertLiteral ?? "insert");
                                            fn.Append(insertStatement.InsertSpecification.InsertOption.ToString());
                                            string schema = Settings.Default.DefaultSchemaName;
                                            if (insertStatement.InsertSpecification.Target is NamedTableReference tableReference)
                                            {
                                                if (tableReference.SchemaObject.SchemaIdentifier != null)
                                                {
                                                    schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                                }

                                                if (Settings.Default.PrependSchemaName)
                                                {
                                                    fn.Append(Settings.Default.SchemaSeparator);
                                                    fn.Append(schema);
                                                }

                                                fn.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                            }
                                            Model.NodeType = SqlTreeNodeType.InsertStatement;
                                            Model.SchemaName = schema;
                                            Model.Name = fn.ToString();
                                            break;
                                        }
                                    case ExecuteStatement executeStatement:
                                        {
                                            StringBuilder fn = new StringBuilder(Settings.Default.ExecuteLiteral ?? "Execute");
                                            string schema = Settings.Default.DefaultSchemaName;
                                            if (executeStatement.ExecuteSpecification.ExecutableEntity is ExecutableProcedureReference procedureReference)
                                            {
                                                if (procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier != null)
                                                {
                                                    schema = procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier.Value;
                                                }

                                                if (Settings.Default.PrependSchemaName)
                                                {
                                                    fn.Append(Settings.Default.SchemaSeparator);
                                                    fn.Append(schema);
                                                }

                                                fn.Append(procedureReference.ProcedureReference.ProcedureReference.Name.BaseIdentifier.Value);
                                            }
                                            Model.NodeType = SqlTreeNodeType.ExecuteStatement;
                                            Model.SchemaName = schema;
                                            Model.Name = fn.ToString();
                                            break;
                                        }


                                    case UpdateStatement updateStatement:
                                        {
                                            StringBuilder fn = new StringBuilder(Settings.Default.UpdateLiteral ?? "Update");
                                            string schema = Settings.Default.DefaultSchemaName;
                                            if (updateStatement.UpdateSpecification.Target is NamedTableReference tableReference)
                                            {
                                                if (tableReference.SchemaObject.SchemaIdentifier != null)
                                                {
                                                    schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                                }

                                                if (Settings.Default.PrependSchemaName)
                                                {
                                                    fn.Append(Settings.Default.SchemaSeparator);
                                                    fn.Append(schema);
                                                }

                                                fn.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                            }

                                            Model.NodeType = SqlTreeNodeType.UpdateStatement;
                                            Model.SchemaName = schema;
                                            Model.Name = fn.ToString();
                                            break;
                                        }
                                    case PredicateSetStatement _:
                                        break;
                                    default:
                                        if (index < sqlBatch.Statements.Count - 1 || i < sqlScript.Batches.Count - 1) continue;
                                        Model.NodeType = SqlTreeNodeType.OtherStatement;
                                        Model.SchemaName = Settings.Default.DefaultSchemaName;
                                        Model.Name = Settings.Default.OtherLiteral ?? "other" + otherNo++;
                                        break;
                                }
                                return;
                            }
                        }

                    }

                }

            }
            catch (Exception exc)
            {
                Model.ParseErrors.Add(new ParseFileError(new ParseError(0, 0, 0, 0, exc.Message), null));
            }
        }
    }


}
