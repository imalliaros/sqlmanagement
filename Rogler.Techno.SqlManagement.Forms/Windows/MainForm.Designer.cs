#region Copyright Syncfusion Inc. 2001-2018.
// Copyright Syncfusion Inc. 2001-2018. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>fmin
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Syncfusion.Windows.Forms.Tools.CaptionButtonsCollection ccbNodesTreeView = new Syncfusion.Windows.Forms.Tools.CaptionButtonsCollection();
            Syncfusion.Windows.Forms.Tools.TreeNodeAdvStyleInfo treeNodeAdvStyleInfo1 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdvStyleInfo();
            Syncfusion.Windows.Forms.Tools.Office2016ColorTable office2016ColorTable1 = new Syncfusion.Windows.Forms.Tools.Office2016ColorTable();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo1 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo4 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo2 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo3 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo5 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo6 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo7 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo8 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo9 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo10 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo11 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo12 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo13 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo14 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo15 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo16 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo17 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo18 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo19 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo20 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo21 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo22 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo23 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo24 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo25 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo26 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo27 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo28 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo29 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo30 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo31 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo32 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            Syncfusion.Windows.Forms.Tools.ToolTipInfo toolTipInfo33 = new Syncfusion.Windows.Forms.Tools.ToolTipInfo();
            this.MainDockingManager = new Syncfusion.Windows.Forms.Tools.DockingManager(this.components);
            this.MainImageList = new System.Windows.Forms.ImageList(this.components);
            this.NodesTreeView = new Syncfusion.Windows.Forms.Tools.TreeViewAdv();
            this.MainRibbon = new Syncfusion.Windows.Forms.Tools.RibbonControlAdv();
            this.MainBackStageView = new Syncfusion.Windows.Forms.BackStageView(this.components);
            this.MainBackStage = new Syncfusion.Windows.Forms.BackStage();
            this.InfoBackStageTab = new Syncfusion.Windows.Forms.BackStageTab();
            this.InfoBackStageTabPanel = new System.Windows.Forms.Panel();
            this.OptionsBackStageButton = new Syncfusion.Windows.Forms.BackStageButton();
            this.CloseBackStageButton = new Syncfusion.Windows.Forms.BackStageButton();
            this.HomeTab = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.CommonToolStrip = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.LoadToolStripButton = new System.Windows.Forms.ToolStripSplitButton();
            this.Utf8EncodingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AnsiEncodingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UnicodeEncodingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MergeToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ConnectToolStripButton = new System.Windows.Forms.ToolStripSplitButton();
            this.DisconnectToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.SyncToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.FileToolStrip = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.SaveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.SaveAsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.SaveAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.PrintToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.CompileToolStrip = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.CompileToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.CheckToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.SplitToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.CompareToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MergeToolStrip = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.CreateToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.SelectionToolStripPanelItem = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.SelectAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ClearAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.EditMergeLstToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ReloadToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.EditToolStrip = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.CopyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.CutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.PasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.UndoRedoToolStripPanelItem = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.UndoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.RedoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.SearchToolStripPanelItem = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.FindToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ReplaceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.GoToLineToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ExportToolStrip = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.ExportExcelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.PdfToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.EditMergeListToolStrip = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.SaveMergeListToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.NewMergeListItemToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.CancelEditMergeListToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainStatusStrip = new Syncfusion.Windows.Forms.Tools.StatusStripEx();
            this.LogoStatusStripLabel = new Syncfusion.Windows.Forms.Tools.StatusStripLabel();
            this.ConnectionStatusStripLabel = new Syncfusion.Windows.Forms.Tools.StatusStripLabel();
            this.VersionToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.RibonSuperToolTip = new Syncfusion.Windows.Forms.Tools.SuperToolTip(this);
            this.MainSplashControl = new Syncfusion.Windows.Forms.Tools.SplashControl();
            this.MainDockingClientPanel = new Syncfusion.Windows.Forms.Tools.DockingClientPanel();
            this.EditorTabControl = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            ((System.ComponentModel.ISupportInitialize)(this.MainDockingManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NodesTreeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainRibbon)).BeginInit();
            this.MainRibbon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainBackStage)).BeginInit();
            this.MainBackStage.SuspendLayout();
            this.InfoBackStageTab.SuspendLayout();
            this.HomeTab.Panel.SuspendLayout();
            this.CommonToolStrip.SuspendLayout();
            this.FileToolStrip.SuspendLayout();
            this.CompileToolStrip.SuspendLayout();
            this.MergeToolStrip.SuspendLayout();
            this.EditToolStrip.SuspendLayout();
            this.ExportToolStrip.SuspendLayout();
            this.EditMergeListToolStrip.SuspendLayout();
            this.MainStatusStrip.SuspendLayout();
            this.MainDockingClientPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditorTabControl)).BeginInit();
            this.SuspendLayout();
            // 
            // MainDockingManager
            // 
            this.MainDockingManager.AnimateAutoHiddenWindow = true;
            this.MainDockingManager.AutoHideTabForeColor = System.Drawing.Color.Empty;
            this.MainDockingManager.DockLayoutStream = ((System.IO.MemoryStream)(resources.GetObject("MainDockingManager.DockLayoutStream")));
            this.MainDockingManager.DragProviderStyle = Syncfusion.Windows.Forms.Tools.DragProviderStyle.VS2012;
            this.MainDockingManager.HostControl = this;
            this.MainDockingManager.ImageList = this.MainImageList;
            this.MainDockingManager.MetroButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.MainDockingManager.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(158)))), ((int)(((byte)(218)))));
            this.MainDockingManager.MetroSplitterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(159)))), ((int)(((byte)(183)))));
            this.MainDockingManager.ReduceFlickeringInRtl = false;
            this.MainDockingManager.VisualStyle = Syncfusion.Windows.Forms.VisualStyle.VS2010;
            this.MainDockingManager.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Close, "CloseButton"));
            this.MainDockingManager.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Pin, "PinButton"));
            this.MainDockingManager.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Maximize, "MaximizeButton"));
            this.MainDockingManager.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Restore, "RestoreButton"));
            this.MainDockingManager.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Menu, "MenuButton"));
            this.MainDockingManager.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Custom, "ClearAllButton", 16, System.Drawing.Color.Transparent, "Clear all items"));
            this.MainDockingManager.SetDockLabel(this.NodesTreeView, "Compiled Tree");
            this.MainDockingManager.SetEnableDocking(this.NodesTreeView, true);
            ccbNodesTreeView.MergeWith(this.MainDockingManager.CaptionButtons, false);
            this.MainDockingManager.SetCustomCaptionButtons(this.NodesTreeView, ccbNodesTreeView);
            // 
            // MainImageList
            // 
            this.MainImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MainImageList.ImageStream")));
            this.MainImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MainImageList.Images.SetKeyName(0, "sql-16.png");
            this.MainImageList.Images.SetKeyName(1, "trash.png");
            this.MainImageList.Images.SetKeyName(2, "list.png");
            this.MainImageList.Images.SetKeyName(3, "letter-p.png");
            this.MainImageList.Images.SetKeyName(4, "letter-f.png");
            this.MainImageList.Images.SetKeyName(5, "database-export-16.png");
            this.MainImageList.Images.SetKeyName(6, "letter-t.png");
            this.MainImageList.Images.SetKeyName(7, "letter-v.png");
            this.MainImageList.Images.SetKeyName(8, "letter-e.png");
            this.MainImageList.Images.SetKeyName(9, "letter-i.png");
            this.MainImageList.Images.SetKeyName(10, "letter-o.png");
            this.MainImageList.Images.SetKeyName(11, "folder.png");
            this.MainImageList.Images.SetKeyName(12, "file.png");
            this.MainImageList.Images.SetKeyName(13, "list 2.png");
            this.MainImageList.Images.SetKeyName(14, "log16.png");
            this.MainImageList.Images.SetKeyName(15, "database.png");
            this.MainImageList.Images.SetKeyName(16, "trash-16.png");
            this.MainImageList.Images.SetKeyName(17, "icons8database-16.png");
            this.MainImageList.Images.SetKeyName(18, "square-root-16.png");
            this.MainImageList.Images.SetKeyName(19, "square-root-edit-16.png");
            this.MainImageList.Images.SetKeyName(20, "workflow-16.png");
            this.MainImageList.Images.SetKeyName(21, "workflow-edit-16.png");
            this.MainImageList.Images.SetKeyName(22, "equal-sign-16.png");
            this.MainImageList.Images.SetKeyName(23, "add-row-16.png");
            this.MainImageList.Images.SetKeyName(24, "trigger-16.png");
            this.MainImageList.Images.SetKeyName(25, "view-16.png");
            this.MainImageList.Images.SetKeyName(26, "view-alter-16.png");
            this.MainImageList.Images.SetKeyName(27, "view2-16.png");
            this.MainImageList.Images.SetKeyName(28, "view2-alter-16.png");
            this.MainImageList.Images.SetKeyName(29, "exe-16.png");
            this.MainImageList.Images.SetKeyName(30, "update-16.png");
            this.MainImageList.Images.SetKeyName(31, "inner-join-16.png");
            this.MainImageList.Images.SetKeyName(32, "sql-20.png");
            this.MainImageList.Images.SetKeyName(33, "xml-20.png");
            this.MainImageList.Images.SetKeyName(34, "active-directory-16.png");
            this.MainImageList.Images.SetKeyName(35, "available-updates-16.png");
            // 
            // NodesTreeView
            // 
            this.NodesTreeView.BackColor = System.Drawing.Color.White;
            this.NodesTreeView.BackgroundColor = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))));
            treeNodeAdvStyleInfo1.EnsureDefaultOptionedChild = true;
            treeNodeAdvStyleInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            treeNodeAdvStyleInfo1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.NodesTreeView.BaseStylePairs.AddRange(new Syncfusion.Windows.Forms.Tools.StyleNamePair[] {
            new Syncfusion.Windows.Forms.Tools.StyleNamePair("Standard", treeNodeAdvStyleInfo1)});
            this.NodesTreeView.BeforeTouchSize = new System.Drawing.Size(312, 398);
            this.NodesTreeView.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.NodesTreeView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NodesTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.NodesTreeView.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.NodesTreeView.FullRowSelect = true;
            this.NodesTreeView.GridOfficeScrollBars = Syncfusion.Windows.Forms.OfficeScrollBars.Office2016;
            // 
            // 
            // 
            this.NodesTreeView.HelpTextControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NodesTreeView.HelpTextControl.Location = new System.Drawing.Point(0, 0);
            this.NodesTreeView.HelpTextControl.Margin = new System.Windows.Forms.Padding(3);
            this.NodesTreeView.HelpTextControl.Name = "helpText";
            this.NodesTreeView.HelpTextControl.Size = new System.Drawing.Size(49, 15);
            this.NodesTreeView.HelpTextControl.TabIndex = 0;
            this.NodesTreeView.HelpTextControl.Text = "help text";
            this.NodesTreeView.HotTracking = true;
            this.NodesTreeView.InactiveSelectedNodeForeColor = System.Drawing.SystemColors.ControlText;
            this.NodesTreeView.LeftImageList = this.MainImageList;
            this.NodesTreeView.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.NodesTreeView.Location = new System.Drawing.Point(1, 26);
            this.NodesTreeView.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(165)))), ((int)(((byte)(220)))));
            this.NodesTreeView.MetroScrollBars = true;
            this.NodesTreeView.Name = "NodesTreeView";
            this.NodesTreeView.Office2016ScrollBars = true;
            this.NodesTreeView.SelectedNodeBackground = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197))))));
            this.NodesTreeView.SelectedNodeForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.NodesTreeView.ShowFocusRect = false;
            this.NodesTreeView.ShowLines = false;
            this.NodesTreeView.Size = new System.Drawing.Size(312, 398);
            this.NodesTreeView.Style = Syncfusion.Windows.Forms.Tools.TreeStyle.Office2016Colorful;
            this.NodesTreeView.TabIndex = 7;
            // 
            // 
            // 
            this.NodesTreeView.ToolTipControl.BackColor = System.Drawing.SystemColors.Info;
            this.NodesTreeView.ToolTipControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NodesTreeView.ToolTipControl.Location = new System.Drawing.Point(0, 0);
            this.NodesTreeView.ToolTipControl.Name = "toolTip";
            this.NodesTreeView.ToolTipControl.Size = new System.Drawing.Size(41, 15);
            this.NodesTreeView.ToolTipControl.TabIndex = 1;
            this.NodesTreeView.ToolTipControl.Text = "toolTip";
            this.NodesTreeView.TransparentControls = true;
            this.NodesTreeView.AfterSelect += new System.EventHandler(this.NodesTreeView_AfterSelect);
            // 
            // MainRibbon
            // 
            this.MainRibbon.BackStageView = this.MainBackStageView;
            this.MainRibbon.CaptionFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainRibbon.CustomRibbonHeaderImage = ((System.Drawing.Image)(resources.GetObject("MainRibbon.CustomRibbonHeaderImage")));
            this.MainRibbon.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.MainRibbon.Header.AddMainItem(HomeTab);
            this.MainRibbon.Location = new System.Drawing.Point(1, 0);
            this.MainRibbon.Margin = new System.Windows.Forms.Padding(2);
            this.MainRibbon.MenuButtonFont = new System.Drawing.Font("Segoe UI", 8.25F);
            this.MainRibbon.MenuButtonText = "Menu";
            this.MainRibbon.MenuButtonWidth = 56;
            this.MainRibbon.MenuColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(198)))));
            this.MainRibbon.Name = "MainRibbon";
            this.MainRibbon.Office2016ColorScheme = Syncfusion.Windows.Forms.Tools.Office2016ColorScheme.White;
            office2016ColorTable1.CheckedTabForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            office2016ColorTable1.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            office2016ColorTable1.SelectedTabColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(181)))));
            office2016ColorTable1.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.MainRibbon.Office2016ColorTable.Add(office2016ColorTable1);
            this.MainRibbon.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Silver;
            // 
            // MainRibbon.OfficeMenu
            // 
            this.MainRibbon.OfficeMenu.AutoClose = false;
            this.MainRibbon.OfficeMenu.Name = "OfficeMenu";
            this.MainRibbon.OfficeMenu.ShowItemToolTips = true;
            this.MainRibbon.OfficeMenu.Size = new System.Drawing.Size(12, 65);
            this.MainRibbon.QuickPanelImageLayout = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MainRibbon.QuickPanelVisible = false;
            this.MainRibbon.RibbonHeaderImage = Syncfusion.Windows.Forms.Tools.RibbonHeaderImage.CircleBands;
            this.MainRibbon.RibbonStyle = Syncfusion.Windows.Forms.Tools.RibbonStyle.Office2016;
            this.MainRibbon.SelectedTab = this.HomeTab;
            this.MainRibbon.ShowRibbonDisplayOptionButton = true;
            this.MainRibbon.Size = new System.Drawing.Size(1320, 156);
            this.MainRibbon.SystemText.QuickAccessDialogDropDownName = "Start menu";
            this.MainRibbon.SystemText.RenameDisplayLabelText = "&Display Name:";
            this.MainRibbon.TabIndex = 0;
            this.MainRibbon.Text = "SQL Management";
            this.MainRibbon.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.MainRibbon.TouchMode = true;
            // 
            // MainBackStageView
            // 
            this.MainBackStageView.BackStage = this.MainBackStage;
            this.MainBackStageView.HostControl = null;
            this.MainBackStageView.HostForm = this;
            // 
            // MainBackStage
            // 
            this.MainBackStage.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.MainBackStage.AllowDrop = true;
            this.MainBackStage.BackStagePanelWidth = 128;
            this.MainBackStage.BeforeTouchSize = new System.Drawing.Size(1316, 552);
            this.MainBackStage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MainBackStage.ChildItemSize = new System.Drawing.Size(80, 140);
            this.MainBackStage.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.MainBackStage.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.MainBackStage.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.MainBackStage.Controls.Add(this.InfoBackStageTab);
            this.MainBackStage.Controls.Add(this.OptionsBackStageButton);
            this.MainBackStage.Controls.Add(this.CloseBackStageButton);
            this.MainBackStage.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.MainBackStage.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.MainBackStage.ItemSize = new System.Drawing.Size(128, 40);
            this.MainBackStage.Location = new System.Drawing.Point(0, 0);
            this.MainBackStage.Margin = new System.Windows.Forms.Padding(2);
            this.MainBackStage.Name = "backStage1";
            this.MainBackStage.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Silver;
            this.MainBackStage.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.MainBackStage.ShowSeparator = false;
            this.MainBackStage.Size = new System.Drawing.Size(1316, 552);
            this.MainBackStage.TabIndex = 2;
            this.MainBackStage.Visible = false;
            // 
            // InfoBackStageTab
            // 
            this.InfoBackStageTab.Accelerator = "";
            this.InfoBackStageTab.BackColor = System.Drawing.Color.White;
            this.InfoBackStageTab.Controls.Add(this.InfoBackStageTabPanel);
            this.InfoBackStageTab.Image = null;
            this.InfoBackStageTab.ImageSize = new System.Drawing.Size(16, 16);
            this.InfoBackStageTab.Location = new System.Drawing.Point(127, 0);
            this.InfoBackStageTab.Margin = new System.Windows.Forms.Padding(2);
            this.InfoBackStageTab.Name = "InfoBackStageTab";
            this.InfoBackStageTab.Position = new System.Drawing.Point(11, 51);
            this.InfoBackStageTab.ShowCloseButton = true;
            this.InfoBackStageTab.Size = new System.Drawing.Size(1189, 552);
            this.InfoBackStageTab.TabIndex = 3;
            this.InfoBackStageTab.Text = "Info";
            this.InfoBackStageTab.ThemesEnabled = false;
            // 
            // InfoBackStageTabPanel
            // 
            this.InfoBackStageTabPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.InfoBackStageTabPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InfoBackStageTabPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoBackStageTabPanel.Location = new System.Drawing.Point(0, 0);
            this.InfoBackStageTabPanel.Margin = new System.Windows.Forms.Padding(2);
            this.InfoBackStageTabPanel.Name = "InfoBackStageTabPanel";
            this.InfoBackStageTabPanel.Size = new System.Drawing.Size(1189, 552);
            this.InfoBackStageTabPanel.TabIndex = 0;
            // 
            // OptionsBackStageButton
            // 
            this.OptionsBackStageButton.Accelerator = "";
            this.OptionsBackStageButton.BackColor = System.Drawing.Color.Transparent;
            this.OptionsBackStageButton.BeforeTouchSize = new System.Drawing.Size(75, 23);
            this.OptionsBackStageButton.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.Options;
            this.OptionsBackStageButton.IsBackStageButton = false;
            this.OptionsBackStageButton.Location = new System.Drawing.Point(10, 51);
            this.OptionsBackStageButton.Name = "OptionsBackStageButton";
            this.OptionsBackStageButton.Size = new System.Drawing.Size(110, 25);
            this.OptionsBackStageButton.TabIndex = 10;
            this.OptionsBackStageButton.Text = "Options";
            this.OptionsBackStageButton.Click += new System.EventHandler(this.OptionsBackStageButton_Click);
            // 
            // CloseBackStageButton
            // 
            this.CloseBackStageButton.Accelerator = "";
            this.CloseBackStageButton.BackColor = System.Drawing.Color.Transparent;
            this.CloseBackStageButton.BeforeTouchSize = new System.Drawing.Size(75, 23);
            this.CloseBackStageButton.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.Exit;
            this.CloseBackStageButton.IsBackStageButton = false;
            this.CloseBackStageButton.Location = new System.Drawing.Point(10, 76);
            this.CloseBackStageButton.Margin = new System.Windows.Forms.Padding(2);
            this.CloseBackStageButton.Name = "CloseBackStageButton";
            this.CloseBackStageButton.Size = new System.Drawing.Size(110, 25);
            this.CloseBackStageButton.TabIndex = 9;
            this.CloseBackStageButton.Text = "Close";
            this.CloseBackStageButton.Click += new System.EventHandler(this.CloseBackStageButton_Click);
            // 
            // HomeTab
            // 
            this.HomeTab.Name = "HomeTab";
            // 
            // MainRibbon.ribbonPanel1
            // 
            this.HomeTab.Panel.Controls.Add(this.CommonToolStrip);
            this.HomeTab.Panel.Controls.Add(this.FileToolStrip);
            this.HomeTab.Panel.Controls.Add(this.CompileToolStrip);
            this.HomeTab.Panel.Controls.Add(this.MergeToolStrip);
            this.HomeTab.Panel.Controls.Add(this.EditToolStrip);
            this.HomeTab.Panel.Controls.Add(this.ExportToolStrip);
            this.HomeTab.Panel.Controls.Add(this.EditMergeListToolStrip);
            this.HomeTab.Panel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.HomeTab.Panel.Name = "ribbonPanel1";
            this.HomeTab.Panel.Padding = new System.Windows.Forms.Padding(0, 1, 20, 0);
            this.HomeTab.Panel.ScrollPosition = 0;
            this.HomeTab.Panel.TabIndex = 2;
            this.HomeTab.Panel.Text = "Home";
            this.HomeTab.Position = 0;
            this.HomeTab.Size = new System.Drawing.Size(53, 30);
            this.HomeTab.Tag = "1";
            this.HomeTab.Text = "Home";
            // 
            // CommonToolStrip
            // 
            this.CommonToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.CommonToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.CommonToolStrip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.CommonToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.CommonToolStrip.Image = null;
            this.CommonToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LoadToolStripButton,
            this.MergeToolStripDropDownButton,
            this.ConnectToolStripButton,
            this.DisconnectToolStripButton,
            this.SyncToolStripButton});
            this.CommonToolStrip.Location = new System.Drawing.Point(0, 1);
            this.CommonToolStrip.Name = "CommonToolStrip";
            this.CommonToolStrip.Office12Mode = false;
            this.CommonToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.CommonToolStrip.Size = new System.Drawing.Size(307, 90);
            this.CommonToolStrip.TabIndex = 0;
            this.CommonToolStrip.Text = "Common";
            // 
            // LoadToolStripButton
            // 
            this.LoadToolStripButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Utf8EncodingToolStripMenuItem,
            this.AnsiEncodingToolStripMenuItem,
            this.UnicodeEncodingToolStripMenuItem});
            this.LoadToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("LoadToolStripButton.Image")));
            this.LoadToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.LoadToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LoadToolStripButton.Name = "LoadToolStripButton";
            this.LoadToolStripButton.Size = new System.Drawing.Size(48, 73);
            this.LoadToolStripButton.Text = "Load";
            this.LoadToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo1.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.code_16;
            toolTipInfo1.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo1.Body.Text = "Load an SQL File in editor";
            toolTipInfo1.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo1.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo1.Header.Text = "Load Sql File";
            this.RibonSuperToolTip.SetToolTip(this.LoadToolStripButton, toolTipInfo1);
            this.LoadToolStripButton.ButtonClick += new System.EventHandler(this.LoadToolStripButton_ButtonClick);
            this.LoadToolStripButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.LoadToolStripButton_DropDownItemClicked);
            // 
            // Utf8EncodingToolStripMenuItem
            // 
            this.Utf8EncodingToolStripMenuItem.Name = "Utf8EncodingToolStripMenuItem";
            this.Utf8EncodingToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.Utf8EncodingToolStripMenuItem.Text = "UTF-8 Encoding";
            this.Utf8EncodingToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AnsiEncodingToolStripMenuItem
            // 
            this.AnsiEncodingToolStripMenuItem.Name = "AnsiEncodingToolStripMenuItem";
            this.AnsiEncodingToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.AnsiEncodingToolStripMenuItem.Text = "ANSI Encoding";
            // 
            // UnicodeEncodingToolStripMenuItem
            // 
            this.UnicodeEncodingToolStripMenuItem.Name = "UnicodeEncodingToolStripMenuItem";
            this.UnicodeEncodingToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.UnicodeEncodingToolStripMenuItem.Text = "Unicode Encoding";
            // 
            // MergeToolStripDropDownButton
            // 
            this.MergeToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.DirectoryToolStripMenuItem});
            this.MergeToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("MergeToolStripDropDownButton.Image")));
            this.MergeToolStripDropDownButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MergeToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MergeToolStripDropDownButton.Name = "MergeToolStripDropDownButton";
            this.MergeToolStripDropDownButton.Size = new System.Drawing.Size(82, 73);
            this.MergeToolStripDropDownButton.Text = "Merge From";
            this.MergeToolStripDropDownButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo4.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.merge16;
            toolTipInfo4.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo4.Body.Text = "Merges multiple SQL files consisting from DDL statements each into a single file " +
    "suitable for running at customer machine";
            toolTipInfo4.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo4.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo4.Header.Text = "Merge From";
            this.RibonSuperToolTip.SetToolTip(this.MergeToolStripDropDownButton, toolTipInfo4);
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.file;
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.FileToolStripMenuItem.Text = "File";
            toolTipInfo2.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo2.Body.Text = "Merge using an index file";
            toolTipInfo2.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo2.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo2.Header.Text = "Merge From File";
            this.RibonSuperToolTip.SetToolTip(this.FileToolStripMenuItem, toolTipInfo2);
            this.FileToolStripMenuItem.Click += new System.EventHandler(this.FileToolStripMenuItem_Click);
            // 
            // DirectoryToolStripMenuItem
            // 
            this.DirectoryToolStripMenuItem.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.folder;
            this.DirectoryToolStripMenuItem.Name = "DirectoryToolStripMenuItem";
            this.DirectoryToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.DirectoryToolStripMenuItem.Text = "Directory";
            toolTipInfo3.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo3.Body.Text = "Merge files without using index file ";
            toolTipInfo3.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo3.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo3.Header.Text = "Merge From Directory";
            this.RibonSuperToolTip.SetToolTip(this.DirectoryToolStripMenuItem, toolTipInfo3);
            this.DirectoryToolStripMenuItem.Click += new System.EventHandler(this.DirectoryToolStripMenuItem_Click);
            // 
            // ConnectToolStripButton
            // 
            this.ConnectToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ConnectToolStripButton.Image")));
            this.ConnectToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ConnectToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ConnectToolStripButton.Name = "ConnectToolStripButton";
            this.ConnectToolStripButton.Size = new System.Drawing.Size(66, 73);
            this.ConnectToolStripButton.Text = "Connect";
            this.ConnectToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo5.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo5.Body.Text = "Connect to an SQL Server and load its schema";
            toolTipInfo5.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo5.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo5.Header.Text = "Connect";
            this.RibonSuperToolTip.SetToolTip(this.ConnectToolStripButton, toolTipInfo5);
            this.ConnectToolStripButton.ButtonClick += new System.EventHandler(this.ConnectToolStripButton_ButtonClick);
            this.ConnectToolStripButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ConnectToolStripButton_DropDownItemClicked);
            // 
            // DisconnectToolStripButton
            // 
            this.DisconnectToolStripButton.Enabled = false;
            this.DisconnectToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("DisconnectToolStripButton.Image")));
            this.DisconnectToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.DisconnectToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DisconnectToolStripButton.Name = "DisconnectToolStripButton";
            this.DisconnectToolStripButton.Size = new System.Drawing.Size(68, 73);
            this.DisconnectToolStripButton.Text = "Disconnect";
            this.DisconnectToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo6.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipInfo6.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo6.Body.Text = "Disconnect from current data source";
            toolTipInfo6.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo6.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo6.Header.Text = "Disconnect";
            this.RibonSuperToolTip.SetToolTip(this.DisconnectToolStripButton, toolTipInfo6);
            this.DisconnectToolStripButton.Click += new System.EventHandler(this.DisconnectToolStripButton_Click);
            // 
            // SyncToolStripButton
            // 
            this.SyncToolStripButton.Enabled = false;
            this.SyncToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SyncToolStripButton.Image")));
            this.SyncToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SyncToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SyncToolStripButton.Name = "SyncToolStripButton";
            this.SyncToolStripButton.Size = new System.Drawing.Size(34, 73);
            this.SyncToolStripButton.Text = "Sync";
            this.SyncToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo7.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo7.Body.Text = "Sync with current data source";
            toolTipInfo7.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo7.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo7.Header.Text = "Sync";
            this.RibonSuperToolTip.SetToolTip(this.SyncToolStripButton, toolTipInfo7);
            this.SyncToolStripButton.Click += new System.EventHandler(this.SyncToolStripButton_Click);
            // 
            // FileToolStrip
            // 
            this.FileToolStrip.AutoSize = false;
            this.FileToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.FileToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FileToolStrip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.FileToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.FileToolStrip.Image = null;
            this.FileToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveToolStripButton,
            this.SaveAsToolStripButton,
            this.SaveAllToolStripButton,
            this.PrintToolStripButton});
            this.FileToolStrip.Location = new System.Drawing.Point(309, 1);
            this.FileToolStrip.Name = "FileToolStrip";
            this.FileToolStrip.Office12Mode = false;
            this.FileToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.FileToolStrip.Size = new System.Drawing.Size(223, 90);
            this.FileToolStrip.TabIndex = 5;
            this.FileToolStrip.Text = "File";
            // 
            // SaveToolStripButton
            // 
            this.SaveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveToolStripButton.Image")));
            this.SaveToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveToolStripButton.Name = "SaveToolStripButton";
            this.SaveToolStripButton.Size = new System.Drawing.Size(36, 73);
            this.SaveToolStripButton.Text = "Save";
            this.SaveToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo8.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.save16;
            toolTipInfo8.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo8.Body.Text = "Save current file of the editor";
            toolTipInfo8.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo8.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo8.Header.Text = "Save";
            this.RibonSuperToolTip.SetToolTip(this.SaveToolStripButton, toolTipInfo8);
            this.SaveToolStripButton.Click += new System.EventHandler(this.SaveToolStripButton_Click);
            // 
            // SaveAsToolStripButton
            // 
            this.SaveAsToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveAsToolStripButton.Image")));
            this.SaveAsToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SaveAsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveAsToolStripButton.Name = "SaveAsToolStripButton";
            this.SaveAsToolStripButton.Size = new System.Drawing.Size(49, 73);
            this.SaveAsToolStripButton.Text = "Save As";
            this.SaveAsToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo9.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipInfo9.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo9.Body.Text = "Save current file of the editor under a different name";
            toolTipInfo9.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo9.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo9.Header.Text = "Save As";
            this.RibonSuperToolTip.SetToolTip(this.SaveAsToolStripButton, toolTipInfo9);
            this.SaveAsToolStripButton.Click += new System.EventHandler(this.SaveAsToolStripButton_Click);
            // 
            // SaveAllToolStripButton
            // 
            this.SaveAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveAllToolStripButton.Image")));
            this.SaveAllToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SaveAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveAllToolStripButton.Name = "SaveAllToolStripButton";
            this.SaveAllToolStripButton.Size = new System.Drawing.Size(50, 73);
            this.SaveAllToolStripButton.Text = "Save All";
            this.SaveAllToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo10.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipInfo10.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo10.Body.Text = "Save all files of the editor";
            toolTipInfo10.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo10.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo10.Header.Text = "Save All";
            this.RibonSuperToolTip.SetToolTip(this.SaveAllToolStripButton, toolTipInfo10);
            this.SaveAllToolStripButton.Click += new System.EventHandler(this.SaveAllToolStripButton_Click);
            // 
            // PrintToolStripButton
            // 
            this.PrintToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("PrintToolStripButton.Image")));
            this.PrintToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PrintToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PrintToolStripButton.Name = "PrintToolStripButton";
            this.PrintToolStripButton.Size = new System.Drawing.Size(36, 73);
            this.PrintToolStripButton.Text = "Print";
            this.PrintToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo11.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipInfo11.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo11.Body.Text = "Print current file of the editor";
            toolTipInfo11.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo11.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo11.Header.Text = "Print file";
            this.RibonSuperToolTip.SetToolTip(this.PrintToolStripButton, toolTipInfo11);
            this.PrintToolStripButton.Click += new System.EventHandler(this.PrintToolStripButton_Click);
            // 
            // CompileToolStrip
            // 
            this.CompileToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.CompileToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.CompileToolStrip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.CompileToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.CompileToolStrip.Image = null;
            this.CompileToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CompileToolStripButton,
            this.CheckToolStripButton,
            this.SplitToolStripButton,
            this.toolStripSeparator1,
            this.CompareToolStripButton});
            this.CompileToolStrip.Location = new System.Drawing.Point(534, 1);
            this.CompileToolStrip.Name = "CompileToolStrip";
            this.CompileToolStrip.Office12Mode = false;
            this.CompileToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.CompileToolStrip.Size = new System.Drawing.Size(211, 90);
            this.CompileToolStrip.TabIndex = 1;
            this.CompileToolStrip.Text = "Compile";
            // 
            // CompileToolStripButton
            // 
            this.CompileToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CompileToolStripButton.Image")));
            this.CompileToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CompileToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CompileToolStripButton.Name = "CompileToolStripButton";
            this.CompileToolStripButton.Size = new System.Drawing.Size(53, 73);
            this.CompileToolStripButton.Text = "Compile";
            this.CompileToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo12.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.code_16;
            toolTipInfo12.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo12.Body.Text = "Compile current file of the edtior, but without generating any file";
            toolTipInfo12.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo12.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo12.Header.Text = "Compile";
            this.RibonSuperToolTip.SetToolTip(this.CompileToolStripButton, toolTipInfo12);
            this.CompileToolStripButton.Click += new System.EventHandler(this.CompileToolStripButton_Click);
            // 
            // CheckToolStripButton
            // 
            this.CheckToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CheckToolStripButton.Image")));
            this.CheckToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CheckToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CheckToolStripButton.Name = "CheckToolStripButton";
            this.CheckToolStripButton.Size = new System.Drawing.Size(42, 73);
            this.CheckToolStripButton.Text = "Check";
            this.CheckToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo13.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipInfo13.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo13.Body.Text = "Check current file of the editor for syntax errors";
            toolTipInfo13.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo13.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo13.Header.Text = "Check File";
            this.RibonSuperToolTip.SetToolTip(this.CheckToolStripButton, toolTipInfo13);
            this.CheckToolStripButton.Click += new System.EventHandler(this.CheckToolStripButton_Click);
            // 
            // SplitToolStripButton
            // 
            this.SplitToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SplitToolStripButton.Image")));
            this.SplitToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SplitToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SplitToolStripButton.Name = "SplitToolStripButton";
            this.SplitToolStripButton.Size = new System.Drawing.Size(44, 73);
            this.SplitToolStripButton.Text = "Split";
            this.SplitToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo14.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.split16;
            toolTipInfo14.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo14.Body.Text = "Splits a large SQL file consisting from DDL statements into multiple files each h" +
    "anving a batch (up to first \'GO\' statement)";
            toolTipInfo14.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo14.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo14.Header.Text = "Split";
            this.RibonSuperToolTip.SetToolTip(this.SplitToolStripButton, toolTipInfo14);
            this.SplitToolStripButton.Click += new System.EventHandler(this.SplitToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 76);
            // 
            // CompareToolStripButton
            // 
            this.CompareToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CompareToolStripButton.Image")));
            this.CompareToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CompareToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CompareToolStripButton.Name = "CompareToolStripButton";
            this.CompareToolStripButton.Size = new System.Drawing.Size(57, 73);
            this.CompareToolStripButton.Text = "Compare";
            this.CompareToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo15.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo15.Body.Text = "Compare the script with an existing server";
            toolTipInfo15.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo15.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo15.Header.Text = "Compare with Server";
            this.RibonSuperToolTip.SetToolTip(this.CompareToolStripButton, toolTipInfo15);
            this.CompareToolStripButton.Click += new System.EventHandler(this.CompareToolStripButton_Click);
            // 
            // MergeToolStrip
            // 
            this.MergeToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.MergeToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.MergeToolStrip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.MergeToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MergeToolStrip.Image = null;
            this.MergeToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateToolStripButton,
            this.SelectionToolStripPanelItem,
            this.EditMergeLstToolStripButton,
            this.ReloadToolStripButton});
            this.MergeToolStrip.Location = new System.Drawing.Point(747, 1);
            this.MergeToolStrip.Name = "MergeToolStrip";
            this.MergeToolStrip.Office12Mode = false;
            this.MergeToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.MergeToolStrip.Size = new System.Drawing.Size(207, 90);
            this.MergeToolStrip.TabIndex = 2;
            this.MergeToolStrip.Text = "Merge";
            // 
            // CreateToolStripButton
            // 
            this.CreateToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CreateToolStripButton.Image")));
            this.CreateToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CreateToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CreateToolStripButton.Name = "CreateToolStripButton";
            this.CreateToolStripButton.Size = new System.Drawing.Size(44, 73);
            this.CreateToolStripButton.Text = "Create";
            this.CreateToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo16.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.merge_cells;
            toolTipInfo16.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo16.Body.Text = "Merge the selected files into a single SQL file. \r\nAdditionally, if that option h" +
    "as been selected create a second file with delete statements for the selected pr" +
    "ocedures/functions";
            toolTipInfo16.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo16.Header.Size = new System.Drawing.Size(20, 20);
            this.RibonSuperToolTip.SetToolTip(this.CreateToolStripButton, toolTipInfo16);
            this.CreateToolStripButton.Click += new System.EventHandler(this.CreateToolStripButton_Click);
            // 
            // SelectionToolStripPanelItem
            // 
            this.SelectionToolStripPanelItem.CausesValidation = false;
            this.SelectionToolStripPanelItem.ForeColor = System.Drawing.Color.MidnightBlue;
            this.SelectionToolStripPanelItem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SelectAllToolStripButton,
            this.ClearAllToolStripButton});
            this.SelectionToolStripPanelItem.Name = "SelectionToolStripPanelItem";
            this.SelectionToolStripPanelItem.RowCount = 2;
            this.SelectionToolStripPanelItem.Size = new System.Drawing.Size(75, 76);
            this.SelectionToolStripPanelItem.Transparent = true;
            // 
            // SelectAllToolStripButton
            // 
            this.SelectAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SelectAllToolStripButton.Image")));
            this.SelectAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SelectAllToolStripButton.Name = "SelectAllToolStripButton";
            this.SelectAllToolStripButton.Size = new System.Drawing.Size(71, 23);
            this.SelectAllToolStripButton.Text = "Select All";
            toolTipInfo17.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.passed_16;
            toolTipInfo17.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo17.Body.Text = "Select all items";
            toolTipInfo17.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo17.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo17.Header.Text = "Select All";
            this.RibonSuperToolTip.SetToolTip(this.SelectAllToolStripButton, toolTipInfo17);
            this.SelectAllToolStripButton.Click += new System.EventHandler(this.SelectAllToolStripButton_Click);
            // 
            // ClearAllToolStripButton
            // 
            this.ClearAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ClearAllToolStripButton.Image")));
            this.ClearAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearAllToolStripButton.Name = "ClearAllToolStripButton";
            this.ClearAllToolStripButton.Size = new System.Drawing.Size(67, 23);
            this.ClearAllToolStripButton.Text = "Clear All";
            toolTipInfo18.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipInfo18.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo18.Body.Text = "Clear all items";
            toolTipInfo18.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo18.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo18.Header.Text = "Clear All";
            this.RibonSuperToolTip.SetToolTip(this.ClearAllToolStripButton, toolTipInfo18);
            this.ClearAllToolStripButton.Click += new System.EventHandler(this.ClearAllToolStripButton_Click);
            // 
            // EditMergeLstToolStripButton
            // 
            this.EditMergeLstToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("EditMergeLstToolStripButton.Image")));
            this.EditMergeLstToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.EditMergeLstToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.EditMergeLstToolStripButton.Name = "EditMergeLstToolStripButton";
            this.EditMergeLstToolStripButton.Size = new System.Drawing.Size(36, 73);
            this.EditMergeLstToolStripButton.Text = "Edit";
            this.EditMergeLstToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo19.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipInfo19.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo19.Body.Text = "Edit list by adding, updating or removing elements";
            toolTipInfo19.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo19.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo19.Header.Text = "Edit List";
            this.RibonSuperToolTip.SetToolTip(this.EditMergeLstToolStripButton, toolTipInfo19);
            this.EditMergeLstToolStripButton.Click += new System.EventHandler(this.EditMergeLstToolStripButton_Click);
            // 
            // ReloadToolStripButton
            // 
            this.ReloadToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ReloadToolStripButton.Image")));
            this.ReloadToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ReloadToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ReloadToolStripButton.Name = "ReloadToolStripButton";
            this.ReloadToolStripButton.Size = new System.Drawing.Size(41, 73);
            this.ReloadToolStripButton.Text = "Relod";
            this.ReloadToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo20.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo20.Body.Text = "Reload and recompile files";
            toolTipInfo20.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo20.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo20.Header.Text = "Reload Files";
            this.RibonSuperToolTip.SetToolTip(this.ReloadToolStripButton, toolTipInfo20);
            this.ReloadToolStripButton.Click += new System.EventHandler(this.ReloadToolStripButton_Click);
            // 
            // EditToolStrip
            // 
            this.EditToolStrip.AutoSize = false;
            this.EditToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.EditToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.EditToolStrip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.EditToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.EditToolStrip.Image = null;
            this.EditToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyToolStripButton,
            this.CutToolStripButton,
            this.PasteToolStripButton,
            this.UndoRedoToolStripPanelItem,
            this.SearchToolStripPanelItem});
            this.EditToolStrip.Location = new System.Drawing.Point(956, 1);
            this.EditToolStrip.Name = "EditToolStrip";
            this.EditToolStrip.Office12Mode = false;
            this.EditToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.EditToolStrip.Size = new System.Drawing.Size(305, 90);
            this.EditToolStrip.TabIndex = 3;
            this.EditToolStrip.Text = "Edit";
            // 
            // CopyToolStripButton
            // 
            this.CopyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CopyToolStripButton.Image")));
            this.CopyToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CopyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CopyToolStripButton.Name = "CopyToolStripButton";
            this.CopyToolStripButton.Size = new System.Drawing.Size(37, 73);
            this.CopyToolStripButton.Text = "Copy";
            this.CopyToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo21.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipInfo21.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo21.Body.Text = "Copy the selected text to the Clipboard";
            toolTipInfo21.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo21.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo21.Header.Text = "Copy";
            this.RibonSuperToolTip.SetToolTip(this.CopyToolStripButton, toolTipInfo21);
            this.CopyToolStripButton.Click += new System.EventHandler(this.CopyToolStripButton_Click);
            // 
            // CutToolStripButton
            // 
            this.CutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CutToolStripButton.Image")));
            this.CutToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CutToolStripButton.Name = "CutToolStripButton";
            this.CutToolStripButton.Size = new System.Drawing.Size(44, 73);
            this.CutToolStripButton.Text = "Cut";
            this.CutToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo22.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipInfo22.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo22.Body.Text = "Cut the selected text from the document and copy it to the Clipboard";
            toolTipInfo22.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo22.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo22.Header.Text = "Cut";
            this.RibonSuperToolTip.SetToolTip(this.CutToolStripButton, toolTipInfo22);
            this.CutToolStripButton.Click += new System.EventHandler(this.CutToolStripButton_Click);
            // 
            // PasteToolStripButton
            // 
            this.PasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("PasteToolStripButton.Image")));
            this.PasteToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PasteToolStripButton.Name = "PasteToolStripButton";
            this.PasteToolStripButton.Size = new System.Drawing.Size(38, 73);
            this.PasteToolStripButton.Text = "Paste";
            this.PasteToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo23.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipInfo23.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo23.Body.Text = "Paste the text from the clipboard at the insertion point";
            toolTipInfo23.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo23.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo23.Header.Text = "Paste";
            this.RibonSuperToolTip.SetToolTip(this.PasteToolStripButton, toolTipInfo23);
            this.PasteToolStripButton.Click += new System.EventHandler(this.PasteToolStripButton_Click);
            // 
            // UndoRedoToolStripPanelItem
            // 
            this.UndoRedoToolStripPanelItem.CausesValidation = false;
            this.UndoRedoToolStripPanelItem.ForeColor = System.Drawing.Color.MidnightBlue;
            this.UndoRedoToolStripPanelItem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UndoToolStripButton,
            this.RedoToolStripButton});
            this.UndoRedoToolStripPanelItem.Name = "UndoRedoToolStripPanelItem";
            this.UndoRedoToolStripPanelItem.Size = new System.Drawing.Size(56, 76);
            this.UndoRedoToolStripPanelItem.Transparent = true;
            // 
            // UndoToolStripButton
            // 
            this.UndoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UndoToolStripButton.Name = "UndoToolStripButton";
            this.UndoToolStripButton.Size = new System.Drawing.Size(52, 23);
            this.UndoToolStripButton.Text = "Undo";
            toolTipInfo24.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipInfo24.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo24.Body.Text = "Undo last action";
            toolTipInfo24.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo24.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo24.Header.Text = "Undo";
            this.RibonSuperToolTip.SetToolTip(this.UndoToolStripButton, toolTipInfo24);
            this.UndoToolStripButton.Click += new System.EventHandler(this.UndoToolStripButton_Click);
            // 
            // RedoToolStripButton
            // 
            this.RedoToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("RedoToolStripButton.Image")));
            this.RedoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RedoToolStripButton.Name = "RedoToolStripButton";
            this.RedoToolStripButton.Size = new System.Drawing.Size(50, 23);
            this.RedoToolStripButton.Text = "Redo";
            toolTipInfo25.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipInfo25.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo25.Body.Text = "Reo last action";
            toolTipInfo25.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo25.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo25.Header.Text = "Redo";
            this.RibonSuperToolTip.SetToolTip(this.RedoToolStripButton, toolTipInfo25);
            this.RedoToolStripButton.Click += new System.EventHandler(this.RedoToolStripButton_Click);
            // 
            // SearchToolStripPanelItem
            // 
            this.SearchToolStripPanelItem.CausesValidation = false;
            this.SearchToolStripPanelItem.ForeColor = System.Drawing.Color.MidnightBlue;
            this.SearchToolStripPanelItem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FindToolStripButton,
            this.ReplaceToolStripButton,
            this.GoToLineToolStripButton});
            this.SearchToolStripPanelItem.Name = "SearchToolStripPanelItem";
            this.SearchToolStripPanelItem.Size = new System.Drawing.Size(83, 76);
            this.SearchToolStripPanelItem.Transparent = true;
            // 
            // FindToolStripButton
            // 
            this.FindToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("FindToolStripButton.Image")));
            this.FindToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FindToolStripButton.Name = "FindToolStripButton";
            this.FindToolStripButton.Size = new System.Drawing.Size(46, 23);
            this.FindToolStripButton.Text = "Find";
            toolTipInfo26.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image12")));
            toolTipInfo26.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo26.Body.Text = "Open Find dialog";
            toolTipInfo26.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo26.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo26.Header.Text = "Find";
            this.RibonSuperToolTip.SetToolTip(this.FindToolStripButton, toolTipInfo26);
            this.FindToolStripButton.Click += new System.EventHandler(this.FindToolStripButton_Click);
            // 
            // ReplaceToolStripButton
            // 
            this.ReplaceToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ReplaceToolStripButton.Image")));
            this.ReplaceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ReplaceToolStripButton.Name = "ReplaceToolStripButton";
            this.ReplaceToolStripButton.Size = new System.Drawing.Size(64, 23);
            this.ReplaceToolStripButton.Text = "Replace";
            toolTipInfo27.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image13")));
            toolTipInfo27.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo27.Body.Text = "Open Replace dialog";
            toolTipInfo27.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo27.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo27.Header.Text = "Replace";
            this.RibonSuperToolTip.SetToolTip(this.ReplaceToolStripButton, toolTipInfo27);
            this.ReplaceToolStripButton.Click += new System.EventHandler(this.ReplaceToolStripButton_Click);
            // 
            // GoToLineToolStripButton
            // 
            this.GoToLineToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("GoToLineToolStripButton.Image")));
            this.GoToLineToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GoToLineToolStripButton.Name = "GoToLineToolStripButton";
            this.GoToLineToolStripButton.Size = new System.Drawing.Size(79, 23);
            this.GoToLineToolStripButton.Text = "Go To Line";
            toolTipInfo28.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image14")));
            toolTipInfo28.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo28.Body.Text = "Open Go To dialog";
            toolTipInfo28.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo28.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo28.Header.Text = "Go To ...";
            this.RibonSuperToolTip.SetToolTip(this.GoToLineToolStripButton, toolTipInfo28);
            this.GoToLineToolStripButton.Click += new System.EventHandler(this.GoToLineToolStripButton_Click);
            // 
            // ExportToolStrip
            // 
            this.ExportToolStrip.AutoSize = false;
            this.ExportToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.ExportToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.ExportToolStrip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.ExportToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ExportToolStrip.Image = null;
            this.ExportToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportExcelToolStripButton,
            this.PdfToolStripButton});
            this.ExportToolStrip.Location = new System.Drawing.Point(1263, 1);
            this.ExportToolStrip.Name = "ExportToolStrip";
            this.ExportToolStrip.Office12Mode = false;
            this.ExportToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.ExportToolStrip.Size = new System.Drawing.Size(100, 90);
            this.ExportToolStrip.TabIndex = 4;
            this.ExportToolStrip.Text = "Export";
            // 
            // ExportExcelToolStripButton
            // 
            this.ExportExcelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ExportExcelToolStripButton.Image")));
            this.ExportExcelToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ExportExcelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExportExcelToolStripButton.Name = "ExportExcelToolStripButton";
            this.ExportExcelToolStripButton.Size = new System.Drawing.Size(36, 73);
            this.ExportExcelToolStripButton.Text = "Excel";
            this.ExportExcelToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo29.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image15")));
            toolTipInfo29.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo29.Body.Text = "Export tha contents of tha table to excel file";
            toolTipInfo29.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo29.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo29.Header.Text = "Export to Excel";
            this.RibonSuperToolTip.SetToolTip(this.ExportExcelToolStripButton, toolTipInfo29);
            this.ExportExcelToolStripButton.Click += new System.EventHandler(this.ExportExcelToolStripButton_Click);
            // 
            // PdfToolStripButton
            // 
            this.PdfToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("PdfToolStripButton.Image")));
            this.PdfToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PdfToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PdfToolStripButton.Name = "PdfToolStripButton";
            this.PdfToolStripButton.Size = new System.Drawing.Size(36, 73);
            this.PdfToolStripButton.Text = "Pdf";
            this.PdfToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo30.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image16")));
            toolTipInfo30.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo30.Body.Text = "Export the contents of the table to PDF file";
            toolTipInfo30.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo30.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo30.Header.Text = "Export to Pdf";
            this.RibonSuperToolTip.SetToolTip(this.PdfToolStripButton, toolTipInfo30);
            this.PdfToolStripButton.Click += new System.EventHandler(this.PdfToolStripButton_Click);
            // 
            // EditMergeListToolStrip
            // 
            this.EditMergeListToolStrip.AutoSize = false;
            this.EditMergeListToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.EditMergeListToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.EditMergeListToolStrip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.EditMergeListToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.EditMergeListToolStrip.Image = null;
            this.EditMergeListToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveMergeListToolStripButton,
            this.NewMergeListItemToolStripButton,
            this.CancelEditMergeListToolStripButton});
            this.EditMergeListToolStrip.Location = new System.Drawing.Point(1365, 1);
            this.EditMergeListToolStrip.Name = "EditMergeListToolStrip";
            this.EditMergeListToolStrip.Office12Mode = false;
            this.EditMergeListToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.EditMergeListToolStrip.Size = new System.Drawing.Size(164, 90);
            this.EditMergeListToolStrip.TabIndex = 6;
            this.EditMergeListToolStrip.Text = "Edit List";
            // 
            // SaveMergeListToolStripButton
            // 
            this.SaveMergeListToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveMergeListToolStripButton.Image")));
            this.SaveMergeListToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SaveMergeListToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveMergeListToolStripButton.Name = "SaveMergeListToolStripButton";
            this.SaveMergeListToolStripButton.Size = new System.Drawing.Size(36, 73);
            this.SaveMergeListToolStripButton.Text = "Save";
            this.SaveMergeListToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo31.Body.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image17")));
            toolTipInfo31.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo31.Body.Text = "Save curent list and stop editing";
            toolTipInfo31.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo31.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo31.Header.Text = "Save List";
            this.RibonSuperToolTip.SetToolTip(this.SaveMergeListToolStripButton, toolTipInfo31);
            this.SaveMergeListToolStripButton.Click += new System.EventHandler(this.SaveMergeListToolStripButton_Click);
            // 
            // NewMergeListItemToolStripButton
            // 
            this.NewMergeListItemToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("NewMergeListItemToolStripButton.Image")));
            this.NewMergeListItemToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.NewMergeListItemToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewMergeListItemToolStripButton.Name = "NewMergeListItemToolStripButton";
            this.NewMergeListItemToolStripButton.Size = new System.Drawing.Size(36, 73);
            this.NewMergeListItemToolStripButton.Text = "New";
            this.NewMergeListItemToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo32.Body.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.new16;
            toolTipInfo32.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo32.Body.Text = "Append new SQL file at List";
            toolTipInfo32.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo32.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo32.Header.Text = "New Item";
            this.RibonSuperToolTip.SetToolTip(this.NewMergeListItemToolStripButton, toolTipInfo32);
            this.NewMergeListItemToolStripButton.Click += new System.EventHandler(this.NewMergeListItemToolStripButton_Click);
            // 
            // CancelEditMergeListToolStripButton
            // 
            this.CancelEditMergeListToolStripButton.Image = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.merge_cells;
            this.CancelEditMergeListToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CancelEditMergeListToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelEditMergeListToolStripButton.Name = "CancelEditMergeListToolStripButton";
            this.CancelEditMergeListToolStripButton.Size = new System.Drawing.Size(45, 73);
            this.CancelEditMergeListToolStripButton.Text = "Cancel";
            this.CancelEditMergeListToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            toolTipInfo33.Body.Size = new System.Drawing.Size(20, 20);
            toolTipInfo33.Body.Text = "Cancel editing and reload list";
            toolTipInfo33.Footer.Size = new System.Drawing.Size(20, 20);
            toolTipInfo33.Header.Size = new System.Drawing.Size(20, 20);
            toolTipInfo33.Header.Text = "Cancel";
            this.RibonSuperToolTip.SetToolTip(this.CancelEditMergeListToolStripButton, toolTipInfo33);
            this.CancelEditMergeListToolStripButton.Click += new System.EventHandler(this.CancelEditMergeListToolStripButton_Click);
            // 
            // MainStatusStrip
            // 
            this.MainStatusStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.MainStatusStrip.BeforeTouchSize = new System.Drawing.Size(1316, 22);
            this.MainStatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LogoStatusStripLabel,
            this.ConnectionStatusStripLabel,
            this.VersionToolStripStatusLabel});
            this.MainStatusStrip.Location = new System.Drawing.Point(1, 581);
            this.MainStatusStrip.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.MainStatusStrip.Name = "MainStatusStrip";
            this.MainStatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.MainStatusStrip.ShowSeparator = false;
            this.MainStatusStrip.Size = new System.Drawing.Size(1316, 22);
            this.MainStatusStrip.TabIndex = 6;
            this.MainStatusStrip.VisualStyle = Syncfusion.Windows.Forms.Tools.StatusStripExStyle.Office2016Colorful;
            // 
            // LogoStatusStripLabel
            // 
            this.LogoStatusStripLabel.Margin = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.LogoStatusStripLabel.Name = "LogoStatusStripLabel";
            this.LogoStatusStripLabel.Size = new System.Drawing.Size(102, 15);
            this.LogoStatusStripLabel.Text = "SQL Management";
            // 
            // ConnectionStatusStripLabel
            // 
            this.ConnectionStatusStripLabel.Margin = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.ConnectionStatusStripLabel.Name = "ConnectionStatusStripLabel";
            this.ConnectionStatusStripLabel.Size = new System.Drawing.Size(0, 0);
            // 
            // VersionToolStripStatusLabel
            // 
            this.VersionToolStripStatusLabel.Name = "VersionToolStripStatusLabel";
            this.VersionToolStripStatusLabel.Size = new System.Drawing.Size(43, 15);
            this.VersionToolStripStatusLabel.Text = "v. 2.3.1";
            // 
            // RibonSuperToolTip
            // 
            this.RibonSuperToolTip.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(158)))), ((int)(((byte)(218)))));
            // 
            // MainSplashControl
            // 
            this.MainSplashControl.HostForm = this;
            this.MainSplashControl.SplashImage = global::Rogler.Techno.SqlManagement.Forms.Properties.Resources.splash;
            // 
            // MainDockingClientPanel
            // 
            this.MainDockingClientPanel.Controls.Add(this.EditorTabControl);
            this.MainDockingClientPanel.Location = new System.Drawing.Point(522, 197);
            this.MainDockingClientPanel.Name = "MainDockingClientPanel";
            this.MainDockingClientPanel.Size = new System.Drawing.Size(301, 171);
            this.MainDockingClientPanel.TabIndex = 11;
            // 
            // EditorTabControl
            // 
            this.EditorTabControl.ActiveTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.EditorTabControl.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.EditorTabControl.BeforeTouchSize = new System.Drawing.Size(301, 171);
            this.EditorTabControl.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.EditorTabControl.CloseButtonHoverForeColor = System.Drawing.Color.Black;
            this.EditorTabControl.CloseButtonPressedForeColor = System.Drawing.Color.Black;
            this.EditorTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorTabControl.HotTrack = true;
            this.EditorTabControl.ImageList = this.MainImageList;
            this.EditorTabControl.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.EditorTabControl.Location = new System.Drawing.Point(0, 0);
            this.EditorTabControl.Name = "EditorTabControl";
            this.EditorTabControl.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.EditorTabControl.ShowCloseButtonHighLightBackColor = true;
            this.EditorTabControl.ShowSeparator = false;
            this.EditorTabControl.ShowTabCloseButton = true;
            this.EditorTabControl.ShowToolTips = true;
            this.EditorTabControl.Size = new System.Drawing.Size(301, 171);
            this.EditorTabControl.TabIndex = 0;
            this.EditorTabControl.TabPanelBackColor = System.Drawing.SystemColors.ControlLight;
            this.EditorTabControl.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.TabRendererDockingWhidbey);
            this.EditorTabControl.ThemesEnabled = true;
            this.EditorTabControl.UserMoveTabs = true;
            this.EditorTabControl.SelectedIndexChanged += new System.EventHandler(this.EditorTabControl_SelectedIndexChanged);
            this.EditorTabControl.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.EditorTabControl_ControlAdded);
            this.EditorTabControl.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.EditorTabControl_ControlRemoved);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1318, 604);
            this.ColorScheme = Syncfusion.Windows.Forms.Tools.RibbonForm.ColorSchemeType.Silver;
            this.Controls.Add(this.MainDockingClientPanel);
            this.Controls.Add(this.MainBackStage);
            this.Controls.Add(this.MainStatusStrip);
            this.Controls.Add(this.MainRibbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(1, 0, 1, 1);
            this.ShowApplicationIcon = false;
            this.Text = "SQL Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.MainForm_DragOver);
            this.DragLeave += new System.EventHandler(this.MainForm_DragLeave);
            ((System.ComponentModel.ISupportInitialize)(this.MainDockingManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NodesTreeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainRibbon)).EndInit();
            this.MainRibbon.ResumeLayout(false);
            this.MainRibbon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainBackStage)).EndInit();
            this.MainBackStage.ResumeLayout(false);
            this.InfoBackStageTab.ResumeLayout(false);
            this.HomeTab.Panel.ResumeLayout(false);
            this.HomeTab.Panel.PerformLayout();
            this.CommonToolStrip.ResumeLayout(false);
            this.CommonToolStrip.PerformLayout();
            this.FileToolStrip.ResumeLayout(false);
            this.FileToolStrip.PerformLayout();
            this.CompileToolStrip.ResumeLayout(false);
            this.CompileToolStrip.PerformLayout();
            this.MergeToolStrip.ResumeLayout(false);
            this.MergeToolStrip.PerformLayout();
            this.EditToolStrip.ResumeLayout(false);
            this.EditToolStrip.PerformLayout();
            this.ExportToolStrip.ResumeLayout(false);
            this.ExportToolStrip.PerformLayout();
            this.EditMergeListToolStrip.ResumeLayout(false);
            this.EditMergeListToolStrip.PerformLayout();
            this.MainStatusStrip.ResumeLayout(false);
            this.MainDockingClientPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EditorTabControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

     

        #endregion

        private Syncfusion.Windows.Forms.Tools.RibbonControlAdv MainRibbon;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem HomeTab;
        private Syncfusion.Windows.Forms.BackStageView MainBackStageView;
        private Syncfusion.Windows.Forms.BackStage MainBackStage;
        private Syncfusion.Windows.Forms.BackStageButton CloseBackStageButton;
        private Syncfusion.Windows.Forms.Tools.StatusStripEx MainStatusStrip;
        private Syncfusion.Windows.Forms.Tools.StatusStripLabel LogoStatusStripLabel;
        private Syncfusion.Windows.Forms.Tools.StatusStripLabel ConnectionStatusStripLabel;
        private Syncfusion.Windows.Forms.BackStageTab InfoBackStageTab;
        private System.Windows.Forms.Panel InfoBackStageTabPanel;
        private Syncfusion.Windows.Forms.BackStageButton OptionsBackStageButton;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx CompileToolStrip;
        private Syncfusion.Windows.Forms.Tools.SuperToolTip RibonSuperToolTip;
        private Syncfusion.Windows.Forms.Tools.SplashControl MainSplashControl;
        private Syncfusion.Windows.Forms.Tools.DockingManager MainDockingManager;
        private Syncfusion.Windows.Forms.Tools.TreeViewAdv NodesTreeView;
        private Syncfusion.Windows.Forms.Tools.DockingClientPanel MainDockingClientPanel;
        private System.Windows.Forms.ImageList MainImageList;
        private System.Windows.Forms.ToolStripButton CompileToolStripButton;
        private System.Windows.Forms.ToolStripButton CheckToolStripButton;
        private System.Windows.Forms.ToolStripButton SplitToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel VersionToolStripStatusLabel;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem SearchToolStripPanelItem;
        private System.Windows.Forms.ToolStripButton SaveAllToolStripButton;
        private System.Windows.Forms.ToolStripButton PrintToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DirectoryToolStripMenuItem;
        public Syncfusion.Windows.Forms.Tools.TabControlAdv EditorTabControl;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx CommonToolStrip;
        public System.Windows.Forms.ToolStripDropDownButton MergeToolStripDropDownButton;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx FileToolStrip;
        public System.Windows.Forms.ToolStripButton SaveToolStripButton;
        public System.Windows.Forms.ToolStripButton SaveAsToolStripButton;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx MergeToolStrip;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx EditToolStrip;
        public System.Windows.Forms.ToolStripButton CreateToolStripButton;
        public Syncfusion.Windows.Forms.Tools.ToolStripPanelItem SelectionToolStripPanelItem;
        public System.Windows.Forms.ToolStripButton SelectAllToolStripButton;
        public System.Windows.Forms.ToolStripButton ClearAllToolStripButton;
        public System.Windows.Forms.ToolStripButton CopyToolStripButton;
        public System.Windows.Forms.ToolStripButton CutToolStripButton;
        public System.Windows.Forms.ToolStripButton PasteToolStripButton;
        public Syncfusion.Windows.Forms.Tools.ToolStripPanelItem UndoRedoToolStripPanelItem;
        public System.Windows.Forms.ToolStripButton UndoToolStripButton;
        public System.Windows.Forms.ToolStripButton RedoToolStripButton;
        public System.Windows.Forms.ToolStripButton FindToolStripButton;
        public System.Windows.Forms.ToolStripButton ReplaceToolStripButton;
        public System.Windows.Forms.ToolStripButton GoToLineToolStripButton;
        public Syncfusion.Windows.Forms.Tools.ToolStripEx ExportToolStrip;
        public System.Windows.Forms.ToolStripButton ExportExcelToolStripButton;
        public System.Windows.Forms.ToolStripButton PdfToolStripButton;
        public System.Windows.Forms.ToolStripButton EditMergeLstToolStripButton;
        public System.Windows.Forms.ToolStripButton SaveMergeListToolStripButton;
        public System.Windows.Forms.ToolStripButton NewMergeListItemToolStripButton;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx EditMergeListToolStrip;
        private System.Windows.Forms.ToolStripButton CancelEditMergeListToolStripButton;
        private System.Windows.Forms.ToolStripButton ReloadToolStripButton;
        private System.Windows.Forms.ToolStripSplitButton ConnectToolStripButton;
        private System.Windows.Forms.ToolStripButton DisconnectToolStripButton;
        private System.Windows.Forms.ToolStripButton SyncToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton CompareToolStripButton;
        public System.Windows.Forms.ToolStripSplitButton LoadToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem Utf8EncodingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AnsiEncodingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UnicodeEncodingToolStripMenuItem;
    }
}

