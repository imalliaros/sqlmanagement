using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Controls;
using Rogler.Techno.SqlManagement.Forms.Interop;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;
using Syncfusion.Drawing;
using Syncfusion.Windows.Forms;
using Syncfusion.Windows.Forms.Diagram;
using Syncfusion.Windows.Forms.Edit;
using Syncfusion.Windows.Forms.Tools;
using Icon = System.Drawing.Icon;
using IDataObject_Com = System.Runtime.InteropServices.ComTypes.IDataObject;
using Resources = Rogler.Techno.SqlManagement.Forms.Properties.Resources;


namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class MainForm : RibbonForm
    {
        private readonly IDropTargetHelper _ddHelper = (IDropTargetHelper)new DragDropHelper();
        private string _currentConnectionString;
        private Operations _operations;

        public MainViewModel ViewModel { get; }

        private int _firstIcon;

        public MainForm()
        {
            InitializeComponent();
            ViewModel = new MainViewModel();
            this.MainBackStage.VisibleFullText = true;
            MainRibbon.MenuColor = ColorTranslator.FromHtml("#2a579a");
            MainDockingManager.CaptionButtons[5].Click += (sender, args) =>
            {
                NodesTreeView.Nodes.Clear();
            };
            EditorTabControl.DataBindings.Add("SelectedIndex", ViewModel, "EditorTabIndex", true,
                DataSourceUpdateMode.OnPropertyChanged);
            EditToolStrip.DataBindings.Add("Visible", ViewModel, "ShowEditorGroup");
            FileToolStrip.DataBindings.Add("Visible", ViewModel, "ShowFileGroup");
            MergeToolStrip.DataBindings.Add("Visible", ViewModel, "ShowMergeGroup");
            EditMergeListToolStrip.DataBindings.Add("Visible", ViewModel, "ShowEditMergeGroup");
            EditMergeListToolStrip.DataBindings.Add("Enabled", ViewModel, "IsEditMergeListEnabled");
            CompileToolStrip.DataBindings.Add("Visible", ViewModel, "ShowCompileGroup");
            ExportToolStrip.DataBindings.Add("Visible", ViewModel, "ShowExportGroup");
            ViewModel.EditorTabIndex = -1;
            _firstIcon = MainImageList.Images.Count;
            AddIcons();
            if (Settings.Default.RememberConnections)
            {
                foreach (var ch in Settings.Default.ConnectionHistory)
                {
                    System.Windows.Forms.ToolStripMenuItem newconnection = new System.Windows.Forms.ToolStripMenuItem
                    {
                        Text = Globals.GetConnectionStringFriendlyName(ch), 
                        Tag = ch
                    };
                    ConnectToolStripButton.DropDownItems.Add(newconnection);
                }
            }
        }

        
        private void CloseBackStageButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OptionsBackStageButton_Click(object sender, EventArgs e)
        {
            OptionsForm options = new OptionsForm();
            options.ShowDialog();
        }


        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (GetFilename(out var filename, e))
            {
                e.Effect = DragDropEffects.Copy;
                Point p = Cursor.Position;
                Win32Point wp;
                wp.x = p.X;
                wp.y = p.Y;

                _ddHelper.DragEnter(this.Handle, e.Data as IDataObject_Com, ref wp, (int)e.Effect);

            }
            else
            {
                e.Effect = DragDropEffects.None;
            }

        }

        protected bool GetFilename(out string filename, DragEventArgs e)
        {
            bool ret = false;
            filename = String.Empty;

            if ((e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
            {
                if (((IDataObject)e.Data).GetData("FileDrop") is Array data)
                {
                    if ((data.Length == 1) && (data.GetValue(0) is String))
                    {
                        filename = ((string[])data)[0];
                        string ext = Path.GetExtension(filename)?.ToLower();
                        if (ext == ".sql" || ext==".xml")
                        {
                            ret = true;
                        }
                    }
                }
            }
            return ret;
        }

        private void MainForm_DragLeave(object sender, EventArgs e)
        {
            _ddHelper.DragLeave();
        }

        private void MainForm_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            Point p = Cursor.Position;
            Win32Point wp;
            wp.x = p.X;
            wp.y = p.Y;

            _ddHelper.DragOver(ref wp, (int)e.Effect);
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            Point p = Cursor.Position;
            Win32Point wp;
            wp.x = p.X;
            wp.y = p.Y;

            _ddHelper.Drop(e.Data as IDataObject_Com, ref wp, (int)e.Effect);
            if (GetFilename(out var filename, e))
            {
                if(string.Equals(Path.GetExtension(filename), ".sql", StringComparison.InvariantCultureIgnoreCase)) LoadFile(Encoding.UTF8, filename);
                else if(string.Equals(Path.GetExtension(filename), ".xml", StringComparison.InvariantCultureIgnoreCase)) LoadIndexFile(filename);
            }
        }

        

        public SqlEditor LoadFile(Encoding encoding=null, string filename=null)
        {
            SqlEditor sqlEditor = new SqlEditor();
            TabPageAdv tabPage = new TabPageAdv {Name = Guid.NewGuid().ToString("N")};
            sqlEditor.Parent = tabPage;
            sqlEditor.Dock = DockStyle.Fill;
            tabPage.Closing += TabPage_Closing;
            EditorTabControl.TabPages.Add(tabPage);

            if (sqlEditor.LoadFile(encoding, filename) != true)
            {
                EditorTabControl.TabPages.Remove(tabPage);
                return null;
            }
            else
            {
                tabPage.ImageIndex = 32;
                tabPage.ToolTipText = sqlEditor.FileName;
                tabPage.Text = System.IO.Path.GetFileName(sqlEditor.FileName);
                EditorTabControl.SelectedTab = tabPage;
                return sqlEditor;
            }
        }

        private void TabPage_Closing(object sender, TabPageAdvClosingEventArgs args)
        {
            if (args.TabPageAdv.Controls[0] is SqlEditor editor)
            {
                if (editor.HasChanges)
                {
                    DialogResult result = MessageBox.Show(Resources.FileHasChanges, Resources.ApplicationName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                    if (result == DialogResult.Cancel)
                    {
                        args.Cancel = true;
                        return;
                    }
                    if(result==DialogResult.Yes) editor.Save();
                    args.Cancel = false;
                }
            }

        }

        private void EditorTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewModel.EditorTabIndex = EditorTabControl.SelectedIndex;
        }


        private void EditorTabControl_ControlAdded(object sender, ControlEventArgs e)
        {
            ViewModel.EditorTabIndex = EditorTabControl.SelectedIndex;
        }

        private void EditorTabControl_ControlRemoved(object sender, ControlEventArgs e)
        {            
            ViewModel.EditorTabIndex = EditorTabControl.SelectedIndex;
        }

        private void SplitToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Split();
            }
        }

        private void SaveToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Save();
            }
        }

        private void SaveAsToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.SaveAs();
            }
        }

        private void PrintToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Print();
            }
        }

        private void SaveAllToolStripButton_Click(object sender, EventArgs e)
        {
            foreach (TabPageAdv tabPage in EditorTabControl.TabPages)
            {
                if (tabPage.Controls.Count > 0 && tabPage.Controls[0] is SqlEditor editor)
                {
                    editor.Save();
                }
            }
        }

        private void CheckToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Check();
            }
        }

        public void CreateSidebar(SqlTreeNode rootNode, string name)
        {
            foreach (TreeNodeAdv node in NodesTreeView.Nodes)
            {
                if (node.Text == rootNode.Name)
                {
                    NodesTreeView.Nodes.Remove(node);
                    break;
                }
            }

            TreeNodeAdv root = new TreeNodeAdv(rootNode.Name)
            {
                Background = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(246)))), ((int)(((byte)(246)))))),
                EnsureDefaultOptionedChild = true,
                LeftImageIndices = new[] { 17 },
                Tag = name
            };


            if (rootNode.Children.Any())
            {
                AddSideBarNodeChildren(rootNode, root);
            }
            NodesTreeView.Nodes.Add(root);
        }

        public void AddSideBarNodeChildren( SqlTreeNode node, TreeNodeAdv parent)
        {

            foreach (SqlTreeNode child in node.Children)
            {
                if (child.TreeType == SqlTreeNodeType.Batch && child.Children.Count == 1 &&
                    child.Children[0].Children.Count == 0)
                {
                    AddSideBarNodeChildren(child, parent);
                    continue;
                }

                int nodeIcon = 0;
                switch (child.TreeType)
                {
                    case SqlTreeNodeType.Batch:
                        nodeIcon = 17;
                        break;
                    case SqlTreeNodeType.CreateFunctionStatement:
                        nodeIcon = 18;
                        break;
                    case SqlTreeNodeType.AlterFunctionStatement:
                        nodeIcon = 19;
                        break;
                    case SqlTreeNodeType.CreateProcedureStatement:
                        nodeIcon = 20;
                        break;
                    case SqlTreeNodeType.AlterProcedureStatement:
                        nodeIcon = 21;
                        break;
                    case SqlTreeNodeType.SetStatement:
                        nodeIcon = 22;
                        break;
                    case SqlTreeNodeType.InsertStatement:
                        nodeIcon = 23;
                        break;
                    case SqlTreeNodeType.CreateTriggerStatement:
                        nodeIcon = 24;
                        break;
                    case SqlTreeNodeType.CreateTableStatement:
                        nodeIcon = 25;
                        break;
                    case SqlTreeNodeType.AlterTableStatement:
                        nodeIcon = 26;
                        break;
                    case SqlTreeNodeType.CreateViewStatement:
                        nodeIcon = 27;
                        break;
                    case SqlTreeNodeType.AlterViewStatement:
                        nodeIcon = 28;
                        break;
                    case SqlTreeNodeType.ExecuteStatement:
                        nodeIcon = 29;
                        break;
                    case SqlTreeNodeType.UpdateStatement:
                        nodeIcon = 30;
                        break;
                    case SqlTreeNodeType.OtherStatement:
                        nodeIcon = 31;
                        break;

                }
                TreeNodeAdv outRec = new TreeNodeAdv(child.Name)
                {
                    LeftImageIndices = new[] { nodeIcon },
                    Tag = child
                };
                outRec.ChildStyle.EnsureDefaultOptionedChild = true;
                parent.Nodes.Add(outRec);
                if (child.Children.Any())
                {
                    AddSideBarNodeChildren(child, outRec);
                }
            }
        }


        private void NodesTreeView_AfterSelect(object sender, EventArgs e)
        {
            if (NodesTreeView.SelectedNode==null || NodesTreeView.SelectedNode.HasChildren) return;
            if (NodesTreeView.SelectedNode.Tag is MergeFilesModel record)
            {
                SqlEditor initialEdit=null;
                foreach (TabPageAdv tabPage in EditorTabControl.TabPages)
                {
                    if (tabPage.Controls.Count > 0 && tabPage.Controls[0] is SqlEditor editor && editor.FileName==record.OriginalFileName)
                    {
                        initialEdit = editor;
                        EditorTabControl.SelectedTab = tabPage;
                        break;
                    }
                }

                if (initialEdit == null) initialEdit = LoadFile(Encoding.UTF8, record.OriginalFileName);
                if (initialEdit != null)
                {
                    initialEdit.SqlEditControl.CurrentLine = record.Line;
                    initialEdit.SqlEditControl.CurrentColumn = record.Column;
                }
            } 
            else if (NodesTreeView.SelectedNode.Tag is SqlTreeNode node)
            {
            }
        }

        private void CompileToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Compile();
            }
        }

        private void SelectAllToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.SelectAll();
            }
        }

        private void ClearAllToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.ClearAll();
            }
        }

        private void CreateToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.Create();
            }
        }

        private void LoadIndexFile(string filename)
        {
            MergeForm mergeForm = new MergeForm(filename, true);
            mergeForm.ShowDialog();
            MergeList mergeList = new MergeList(mergeForm.Model, filename, null, this);
            TabPageAdv tabPage = new TabPageAdv {Name = Guid.NewGuid().ToString("N")};
            mergeList.Parent = tabPage;
            mergeList.Dock = DockStyle.Fill;
            EditorTabControl.TabPages.Add(tabPage);
            tabPage.ImageIndex = 33;
            tabPage.ToolTipText = filename;
            tabPage.Text = Path.GetFileName(filename);
            EditorTabControl.SelectedTab = tabPage;
        }

        private void FileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = "XML Files (*.xml)|*.xml|All Files (*.*)|*.*",
                Title = Resources.SelectIndexFile
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadIndexFile(openFileDialog.FileName);
            }
        }

        private void DirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog
            {
                Description = Resources.EnterOutpuDirectory
            };
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                MergeForm mergeForm = new MergeForm(folderBrowser.SelectedPath);
                mergeForm.ShowDialog();
                MergeList mergeList = new MergeList(mergeForm.Model, null, folderBrowser.SelectedPath, this);
                TabPageAdv tabPage = new TabPageAdv {Name = Guid.NewGuid().ToString("N")};
                mergeList.Parent = tabPage;
                mergeList.Dock = DockStyle.Fill;
                EditorTabControl.TabPages.Add(tabPage);
                tabPage.ImageIndex = 33;
                tabPage.ToolTipText = folderBrowser.SelectedPath;
                tabPage.Text = new DirectoryInfo(folderBrowser.SelectedPath).Name;
                EditorTabControl.SelectedTab = tabPage;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = MessageBox.Show(Resources.ExitConfirmation, Resources.ApplicationName, MessageBoxButtons.OKCancel,
                           MessageBoxIcon.Warning) != DialogResult.OK;
        }

        private void CopyToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Copy();
            }
        }

        private void CutToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Cut();
            }
        }

        private void PasteToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Paste();
            }
        }

        private void UndoToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Undo();
            }
        }

        private void RedoToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Redo();
            }
        }

        private void FindToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Find();
            }
        }

        private void ReplaceToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.Replace();
            }
        }

        private void GoToLineToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.GoToLine();
            }
        }

        private void ExportExcelToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.ExportToExcel();
            }
            else if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                       EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                if (list.CanExport) list.ExportToExcel();
            }
            else if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                       EditorTabControl.SelectedTab.Controls[0] is MergeLog log)
            {
                log.ExportToExcel();
            }
        }

        private void PdfToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                editor.ExportToPdf();
            }
            else if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                     EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                if (list.CanExport) list.ExportToPdf();
            }
            else if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                     EditorTabControl.SelectedTab.Controls[0] is MergeLog log)
            {
                log.ExportToPdf();
            }
        }

        public void ShowLog(List<MergeFilesModel> model, string createFile)
        {
            MergeLog mergeLog = new MergeLog(model, this, createFile);
            TabPageAdv tabPage = new TabPageAdv {Name = Guid.NewGuid().ToString("N")};
            mergeLog.Parent = tabPage;
            mergeLog.Dock = DockStyle.Fill;
            EditorTabControl.TabPages.Add(tabPage);
            tabPage.ImageIndex = 14;
            tabPage.ToolTipText = createFile;
            tabPage.Text = Path.GetFileName(createFile);
            EditorTabControl.SelectedTab = tabPage;

        }

        private void EditMergeLstToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.BeginEdit();
            }
        }

        private void SaveMergeListToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.EndEdit();
            }
        }

        private void NewMergeListItemToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.New();
            }
        }

        private void CancelEditMergeListToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.CancelEdit();
            }
        }

#region Icons

        private void AddIcons()
        {
            Color iconColor = Color.FromArgb(255, 0, 114, 198);
            int size = 40, smallsize = 20;
            
            MainImageList.AddIcon(IconChar.Database, smallsize, iconColor); //0
            MainImageList.AddIcon(IconChar.Hdd, smallsize, iconColor); //1

            

            Icon = Icon.FromHandle(IconChar.Server.ToBitmap(48, Color.Fuchsia).GetHicon());
            LoadToolStripButton.Image = Icon.FromHandle(IconChar.FolderOpen.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(LoadToolStripButton).Body.Image = Icon.FromHandle(IconChar.FolderOpen.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            
            CompileToolStripButton.Image = Icon.FromHandle(IconChar.Code.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(CompileToolStripButton).Body.Image = Icon.FromHandle(IconChar.Code.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            
            SplitToolStripButton.Image = Icon.FromHandle(IconChar.CodeBranch.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(SplitToolStripButton).Body.Image = Icon.FromHandle(IconChar.CodeBranch.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            MergeToolStripDropDownButton.Image = Icon.FromHandle(IconChar.Hubspot.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(MergeToolStripDropDownButton).Body.Image = Icon.FromHandle(IconChar.Hubspot.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            SaveToolStripButton.Image = Icon.FromHandle(IconChar.Save.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(SaveToolStripButton).Body.Image = Icon.FromHandle(IconChar.Save.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            ExportExcelToolStripButton.Image = Icon.FromHandle(IconChar.FileExcel.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(ExportExcelToolStripButton).Body.Image = Icon.FromHandle(IconChar.FileExcel.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            PdfToolStripButton.Image = Icon.FromHandle(IconChar.FilePdf.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(PdfToolStripButton).Body.Image = Icon.FromHandle(IconChar.FilePdf.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            CopyToolStripButton.Image = Icon.FromHandle(IconChar.Copy.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(CopyToolStripButton).Body.Image = Icon.FromHandle(IconChar.Copy.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            PasteToolStripButton.Image = Icon.FromHandle(IconChar.Paste.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(PasteToolStripButton).Body.Image = Icon.FromHandle(IconChar.Paste.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            CutToolStripButton.Image = Icon.FromHandle(IconChar.Cut.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(CutToolStripButton).Body.Image = Icon.FromHandle(IconChar.Cut.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            UndoToolStripButton.Image = Icon.FromHandle(IconChar.Undo.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(UndoToolStripButton).Body.Image = Icon.FromHandle(IconChar.Undo.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            RedoToolStripButton.Image = Icon.FromHandle(IconChar.Redo.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(RedoToolStripButton).Body.Image = Icon.FromHandle(IconChar.Redo.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            FindToolStripButton.Image = Icon.FromHandle(IconChar.Binoculars.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(FindToolStripButton).Body.Image = Icon.FromHandle(IconChar.Binoculars.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            ReplaceToolStripButton.Image = Icon.FromHandle(IconChar.Sync.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(ReplaceToolStripButton).Body.Image = Icon.FromHandle(IconChar.Sync.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            GoToLineToolStripButton.Image = Icon.FromHandle(IconChar.Indent.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(GoToLineToolStripButton).Body.Image = Icon.FromHandle(IconChar.Indent.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            PrintToolStripButton.Image = Icon.FromHandle(IconChar.Print.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(PrintToolStripButton).Body.Image = Icon.FromHandle(IconChar.Print.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            FileToolStripMenuItem.Image = Icon.FromHandle(IconChar.File.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(FileToolStripMenuItem).Body.Image = Icon.FromHandle(IconChar.File.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            
            DirectoryToolStripMenuItem.Image = Icon.FromHandle(IconChar.Folder.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(DirectoryToolStripMenuItem).Body.Image = Icon.FromHandle(IconChar.Folder.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            SaveMergeListToolStripButton.Image = Icon.FromHandle(IconChar.Save.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(SaveMergeListToolStripButton).Body.Image = Icon.FromHandle(IconChar.Save.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            CheckToolStripButton.Image = Icon.FromHandle(IconChar.ClipboardCheck.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(CheckToolStripButton).Body.Image = Icon.FromHandle(IconChar.ClipboardCheck.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            SelectAllToolStripButton.Image = Icon.FromHandle(IconChar.ListAlt.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(SelectAllToolStripButton).Body.Image = Icon.FromHandle(IconChar.ListAlt.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            ClearAllToolStripButton.Image = Icon.FromHandle(IconChar.Broom.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(ClearAllToolStripButton).Body.Image = Icon.FromHandle(IconChar.Broom.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();


            EditMergeLstToolStripButton.Image = Icon.FromHandle(IconChar.PencilAlt.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(EditMergeLstToolStripButton).Body.Image = Icon.FromHandle(IconChar.PencilAlt.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            NewMergeListItemToolStripButton.Image = Icon.FromHandle(IconChar.Plus.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(NewMergeListItemToolStripButton).Body.Image = Icon.FromHandle(IconChar.Plus.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            ReloadToolStripButton.Image = Icon.FromHandle(IconChar.Sync.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(ReloadToolStripButton).Body.Image = Icon.FromHandle(IconChar.Sync.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            CompareToolStripButton.Image = Icon.FromHandle(IconChar.Columns.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(CompareToolStripButton).Body.Image = Icon.FromHandle(IconChar.Columns.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            CancelEditMergeListToolStripButton.Image = Icon.FromHandle(IconChar.Times.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(CancelEditMergeListToolStripButton).Body.Image = Icon.FromHandle(IconChar.Times.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            SyncToolStripButton.Image = Icon.FromHandle(IconChar.Sync.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(SyncToolStripButton).Body.Image = Icon.FromHandle(IconChar.Sync.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            CreateToolStripButton.Image = Icon.FromHandle(IconChar.FileImport.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(CreateToolStripButton).Body.Image = Icon.FromHandle(IconChar.FileImport.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            ConnectToolStripButton.Image = Icon.FromHandle(IconChar.Plug.ToBitmap(size, iconColor).GetHicon()).ToBitmap();
            RibonSuperToolTip.GetToolTip(ConnectToolStripButton).Body.Image = Icon.FromHandle(IconChar.Plug.ToBitmap(smallsize, iconColor).GetHicon()).ToBitmap();

            OptionsBackStageButton.Image = Icon.FromHandle(IconChar.Cog.ToBitmap(smallsize, Color.White).GetHicon()).ToBitmap();
            CloseBackStageButton.Image = Icon.FromHandle(IconChar.SignOutAlt.ToBitmap(smallsize, Color.White).GetHicon()).ToBitmap();



        }
        #endregion

        private void ReloadToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is MergeList list)
            {
                list.Reload();
            }
        }

        private void LoadSchemaTab()
        {
            Schema schema = new Schema(_operations);
            CloseAllSchemas();
            TabPageAdv tabPage = new TabPageAdv {Name = Guid.NewGuid().ToString("N")};
            schema.Parent = tabPage;
            schema.Dock = DockStyle.Fill;
            tabPage.Closing += TabPage_Closing;
            EditorTabControl.TabPages.Add(tabPage);
            tabPage.ImageIndex = 34;
            tabPage.ToolTipText = _operations.ToString();
            tabPage.Text = _operations.ToString();
            EditorTabControl.SelectedTab = tabPage;
            ConnectionStatusStripLabel.Text = _operations.ToString();
            if (Settings.Default.RememberConnections)
            {
                if (Settings.Default.ConnectionHistory.IndexOf(_operations.ConnectionString) == -1)
                {
                    if (Settings.Default.ConnectionHistory.Count >= 10)
                    {
                        Settings.Default.ConnectionHistory.RemoveAt(0);
                        ConnectToolStripButton.DropDownItems.RemoveAt(0);
                    }

                    System.Windows.Forms.ToolStripMenuItem newconnection = new System.Windows.Forms.ToolStripMenuItem
                    {
                        Text = Globals.GetConnectionStringFriendlyName(_operations.ConnectionString), Tag = _operations.ConnectionString
                    };
                    Settings.Default.ConnectionHistory.Add(_operations.ConnectionString);
                    ConnectToolStripButton.DropDownItems.Add(newconnection);
                    Settings.Default.Save();
                }
            }

            ConnectToolStripButton.Enabled = false;
            DisconnectToolStripButton.Enabled = true;
            SyncToolStripButton.Enabled = true;
            _currentConnectionString = _operations.ConnectionString;
        }

        private void ConnectToolStripButton_ButtonClick(object sender, EventArgs e)
        {
            _operations = new Operations();
            if (_operations.GetConnection())
            {
                ServerForm server = new ServerForm(_operations);
                if(server.ShowDialog()==DialogResult.OK) LoadSchemaTab();
            }
        }

        private void ConnectToolStripButton_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            _operations = new Operations((string) e.ClickedItem.Tag);
            ServerForm server = new ServerForm(_operations);
            if(server.ShowDialog()==DialogResult.OK) LoadSchemaTab();
        }

        private void DisconnectToolStripButton_Click(object sender, EventArgs e)
        {
            DisconnectToolStripButton.Enabled = false;
            ConnectToolStripButton.Enabled = true;
            ConnectionStatusStripLabel.Text = string.Empty;
            SyncToolStripButton.Enabled = false;
            _currentConnectionString = string.Empty;
            _operations = null;
        }

        private void SyncToolStripButton_Click(object sender, EventArgs e)
        {
            Operations ops = new Operations(_currentConnectionString);
            ServerForm server = new ServerForm(ops);
            if(server.ShowDialog()==DialogResult.OK) LoadSchemaTab();
        }

        private void CloseAllSchemas()
        {
            foreach (TabPageAdv tabPage in EditorTabControl.TabPages)
            {
                if (tabPage.Controls.Count > 0 && tabPage.Controls[0] is Schema)
                {
                    EditorTabControl.TabPages.Remove(tabPage);
                }
            }
        }

        private void CompareToolStripButton_Click(object sender, EventArgs e)
        {
            if (EditorTabControl.SelectedTab?.Controls.Count > 0 &&
                EditorTabControl.SelectedTab.Controls[0] is SqlEditor editor)
            {
                if (string.IsNullOrEmpty(_currentConnectionString)) ConnectToolStripButton_ButtonClick(sender, e);
                if (string.IsNullOrEmpty(_currentConnectionString)) return;
                if (!editor.ViewModel.HasOutput)
                {
                    if (editor.ViewModel.ParseErrors != null && editor.ViewModel.ParseErrors.Any())
                    {
                        MessageBox.Show(Resources.FileHasErrors, Resources.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    editor.Compile();
                    if (!editor.ViewModel.HasOutput)
                    {                        
                        if (editor.ViewModel.ParseErrors != null && editor.ViewModel.ParseErrors.Any())
                        {
                            MessageBox.Show(Resources.FileHasErrors, Resources.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        return;
                    }
                }
                Compare cmp = new Compare(editor.ViewModel.Output, _operations);
                TabPageAdv tabPage = new TabPageAdv {Name = Guid.NewGuid().ToString("N")};
                cmp.Parent = tabPage;
                cmp.Dock = DockStyle.Fill;
                tabPage.Closing += TabPage_Closing;
                EditorTabControl.TabPages.Add(tabPage);
                tabPage.ImageIndex = 35;
                tabPage.ToolTipText = _operations.ToString() + "-" +  editor.FileName;
                tabPage.Text = _operations.ToString() + "-" +  Path.GetFileName(editor.FileName);
                EditorTabControl.SelectedTab = tabPage;
            }
            
        }

        private void LoadToolStripButton_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == Utf8EncodingToolStripMenuItem)
            {
                LoadFile(Encoding.UTF8);
            }
            else if (e.ClickedItem == AnsiEncodingToolStripMenuItem)
            {
                LoadFile(Encoding.Default);
            }
            else if (e.ClickedItem == UnicodeEncodingToolStripMenuItem)
            {
                LoadFile(Encoding.Unicode);
            }
        }

        public void Sidebar(string inputFile, IEnumerable<MergeFilesModel> output)
        {
            foreach (TreeNodeAdv node in NodesTreeView.Nodes)
            {
                if (node.Text == inputFile)
                {
                    NodesTreeView.Nodes.Remove(node);
                    break;
                }
            }

            TreeNodeAdv root = new TreeNodeAdv(inputFile)
            {
                Background = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(246)))), ((int)(((byte)(246)))))),
                EnsureDefaultOptionedChild = true
            };

            foreach (MergeFilesModel record in output)
            {
                int imageIndex = 0;
                switch (record.NodeType)
                {
                    case SqlTreeNodeType.Batch:
                        imageIndex = 17;
                        break;
                    case SqlTreeNodeType.CreateFunctionStatement:
                        imageIndex = 18;
                        break;
                    case SqlTreeNodeType.AlterFunctionStatement:
                        imageIndex = 19;
                        break;
                    case SqlTreeNodeType.CreateProcedureStatement:
                        imageIndex = 20;
                        break;
                    case SqlTreeNodeType.AlterProcedureStatement:
                        imageIndex = 21;
                        break;
                    case SqlTreeNodeType.SetStatement:
                        imageIndex = 22;
                        break;
                    case SqlTreeNodeType.InsertStatement:
                        imageIndex = 23;
                        break;
                    case SqlTreeNodeType.CreateTriggerStatement:
                        imageIndex = 24;
                        break;
                    case SqlTreeNodeType.CreateTableStatement:
                        imageIndex = 25;
                        break;
                    case SqlTreeNodeType.AlterTableStatement:
                        imageIndex = 26;
                        break;
                    case SqlTreeNodeType.CreateViewStatement:
                        imageIndex = 27;
                        break;
                    case SqlTreeNodeType.AlterViewStatement:
                        imageIndex = 28;
                        break;
                    case SqlTreeNodeType.ExecuteStatement:
                        imageIndex = 29;
                        break;
                    case SqlTreeNodeType.UpdateStatement:
                        imageIndex = 30;
                        break;
                    case SqlTreeNodeType.OtherStatement:
                        imageIndex = 31;
                        break;

                }
                TreeNodeAdv outRec = new TreeNodeAdv(record.Name)
                {
                    LeftImageIndices = new[] { imageIndex },
                    Tag = record
                };
                outRec.ChildStyle.EnsureDefaultOptionedChild = true;
                root.Nodes.Add(outRec);
            }

            NodesTreeView.Nodes.Add(root);
        }

        private void LoadToolStripButton_ButtonClick(object sender, EventArgs e)
        {
            LoadFile();
        }
    }

}
