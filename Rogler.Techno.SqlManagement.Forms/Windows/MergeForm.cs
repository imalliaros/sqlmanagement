﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;


namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class MergeForm : Rogler.Techno.SqlManagement.Forms.Windows.WaitForm
    {
        private string _inputDirectory;
        public List<MergeFilesModel> Model = new List<MergeFilesModel>();
        public string IndexFile=null;

        public MergeForm(string inputDirectory)
        {
            InitializeComponent();
            _inputDirectory = inputDirectory;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorkerOnDoWork;
        }

        public MergeForm(string indexFile, bool isIndex)
        {
            InitializeComponent();
            IndexFile = indexFile;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorkerOnDoWorkIndexFile;
        }

        private void WaitBackgroundWorkerOnDoWorkIndexFile(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<MergeFilesModel> records = Globals.ParseIndexFile(IndexFile);
                WaitBackgroundWorker.ReportProgress(0);
                _inputDirectory = Path.GetDirectoryName(IndexFile) ?? "./";
                int count = 0;
                foreach (MergeFilesModel record in records)
                {
                    double percent = ++count / (double)records.Count;
                    WaitBackgroundWorker.ReportProgress((int)Math.Floor(percent * 100));
                    string sqlFile = record.FileName;
                    if (!File.Exists(sqlFile))
                        sqlFile = Path.Combine(_inputDirectory, Path.GetFileName(record.FileName));
                    if (!File.Exists(sqlFile)) continue;

                    MergeFilesModel model = new MergeFilesModel
                    {
                        FileName = sqlFile
                    };
                    TSql140Parser parser = new TSql140Parser(true);
                    using (StreamReader sreader = new StreamReader(sqlFile))
                    {
                        string sql = sreader.ReadToEnd();
                        TSqlFragment result = parser.Parse(new StringReader(sql), out var parseErrors);
                        model.ParseErrors = parseErrors.Select(p => new ParseFileError(p, sqlFile)).ToList();
                        int otherNo = 0;
                        if (result is TSqlScript sqlScript)
                        {
                            for (int i = 0; i < sqlScript.Batches.Count; i++)
                            {
                                TSqlBatch sqlBatch = sqlScript.Batches[i];
                                for (var index = 0; index < sqlBatch.Statements.Count; index++)
                                {
                                    TSqlStatement sqlStatement = sqlBatch.Statements[index];
                                    switch (sqlStatement)
                                    {
                                        case CreateProcedureStatement procedureStatement:
                                            model.NodeType = SqlTreeNodeType.CreateProcedureStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value)
                                                ? procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value;
                                            break;
                                        case AlterProcedureStatement procedureStatement:
                                            model.NodeType = SqlTreeNodeType.AlterProcedureStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value)
                                                ? procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value;
                                            break;
                                        case CreateFunctionStatement functionStatement:
                                            model.NodeType = SqlTreeNodeType.CreateFunctionStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier?.Value)
                                                ? functionStatement.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = functionStatement.Name.BaseIdentifier.Value;
                                            break;
                                        case AlterFunctionStatement functionStatement:
                                            model.NodeType = SqlTreeNodeType.AlterFunctionStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier?.Value)
                                                ? functionStatement.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = functionStatement.Name.BaseIdentifier.Value;
                                            break;
                                        case CreateTriggerStatement triggerStatement:
                                            model.NodeType = SqlTreeNodeType.CreateTriggerStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(triggerStatement.Name.SchemaIdentifier?.Value)
                                                ? triggerStatement.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = triggerStatement.Name.BaseIdentifier.Value;
                                            break;
                                        case CreateViewStatement viewStatement:
                                            model.NodeType = SqlTreeNodeType.CreateViewStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? viewStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = viewStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case CreateTableStatement tableStatement:
                                            model.NodeType = SqlTreeNodeType.CreateTableStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? tableStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = tableStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case AlterTableStatement tableStatement:
                                            model.NodeType = SqlTreeNodeType.AlterTableStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? tableStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = tableStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case AlterViewStatement viewStatement:
                                            model.NodeType = SqlTreeNodeType.AlterViewStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? viewStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = viewStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case InsertStatement insertStatement:
                                            {
                                                StringBuilder statementName = new StringBuilder();
                                                string schema = Settings.Default.DefaultSchemaName;
                                                if (insertStatement.InsertSpecification.Target is NamedTableReference tableReference)
                                                {
                                                    if (tableReference.SchemaObject.SchemaIdentifier != null)
                                                    {
                                                        schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                                    }

                                                    if (Settings.Default.PrependSchemaName)
                                                    {
                                                        statementName.Append(Settings.Default.SchemaSeparator);
                                                        statementName.Append(schema);
                                                    }

                                                    statementName.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                                }
                                                else
                                                {
                                                    statementName.Append(Settings.Default.InsertLiteral);
                                                }
                                                model.NodeType = SqlTreeNodeType.InsertStatement;
                                                model.SchemaName = schema;
                                                model.Name = statementName.ToString();
                                                break;
                                            }
                                        case ExecuteStatement executeStatement:
                                            {
                                                StringBuilder statementName = new StringBuilder();
                                                string schema = Settings.Default.DefaultSchemaName;
                                                if (executeStatement.ExecuteSpecification.ExecutableEntity is ExecutableProcedureReference procedureReference)
                                                {
                                                    if (procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier != null)
                                                    {
                                                        schema = procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier.Value;
                                                    }

                                                    if (Settings.Default.PrependSchemaName)
                                                    {
                                                        statementName.Append(Settings.Default.SchemaSeparator);
                                                        statementName.Append(schema);
                                                    }

                                                    statementName.Append(procedureReference.ProcedureReference.ProcedureReference.Name.BaseIdentifier.Value);
                                                }
                                                else
                                                {
                                                    statementName.Append(Settings.Default.ExecuteLiteral);
                                                }
                                                model.NodeType = SqlTreeNodeType.ExecuteStatement;
                                                model.SchemaName = schema;
                                                model.Name = statementName.ToString();
                                                break;
                                            }
                                        

                                        case UpdateStatement updateStatement:
                                        {
                                            StringBuilder statementName = new StringBuilder();
                                            string schema = Settings.Default.DefaultSchemaName;
                                            if (updateStatement.UpdateSpecification.Target is NamedTableReference tableReference)
                                            {
                                                if (tableReference.SchemaObject.SchemaIdentifier != null)
                                                {
                                                    schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                                }

                                                if (Settings.Default.PrependSchemaName)
                                                {
                                                    statementName.Append(Settings.Default.SchemaSeparator);
                                                    statementName.Append(schema);
                                                }

                                                statementName.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                            }
                                            else
                                            {
                                                statementName.Append(Settings.Default.UpdateLiteral);
                                            }

                                                model.NodeType = SqlTreeNodeType.UpdateStatement;
                                                model.SchemaName = schema;
                                                model.Name = statementName.ToString();
                                                break;
                                            }
                                        case PredicateSetStatement _:
                                            break;
                                        default:
                                            if (index < sqlBatch.Statements.Count - 1 || i < sqlScript.Batches.Count -1) continue;
                                            model.NodeType = SqlTreeNodeType.OtherStatement;
                                            model.SchemaName = Settings.Default.DefaultSchemaName;
                                            model.Name = Settings.Default.OtherLiteral ?? "Other";
                                            break;
                                    }
                                }
                            }

                            if (sqlScript.Batches.Count > 0 && sqlScript.Batches[sqlScript.Batches.Count - 1].Statements.Count > 0)
                            {
                                int lastToken = sqlScript.Batches[sqlScript.Batches.Count - 1]
                                    .Statements[sqlScript.Batches[sqlScript.Batches.Count - 1].Statements.Count - 1].LastTokenIndex;
                                model.ContainsGo = false;
                                while (lastToken < sqlScript.ScriptTokenStream.Count)
                                {
                                    if (sqlScript.ScriptTokenStream[lastToken].TokenType == TSqlTokenType.Go)
                                    {
                                        model.ContainsGo = true;
                                        break;
                                    }

                                    ++lastToken;
                                }
                            }
                        }
                    }

                    Model.Add(model);
                }
            }
            catch (Exception exc)
            {
                Model = new List<MergeFilesModel>();
                Model.Add(new MergeFilesModel
                {
                    FileName = _inputDirectory,
                    ParseErrors = new List<ParseFileError> { new ParseFileError(new ParseError(0, 0, 0, 0, exc.Message), null) }
                });
            }

        }

        private void WaitBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (Settings.Default.PreserveOrder && !string.IsNullOrEmpty(Settings.Default.IndexFile) &&
                    File.Exists(Path.Combine(_inputDirectory, Settings.Default.IndexFile)))
                    IndexFile = Path.Combine(_inputDirectory, Settings.Default.IndexFile);
                List<string> sqlFiles =
                    Directory.EnumerateFiles(_inputDirectory, "*.sql", SearchOption.TopDirectoryOnly).ToList();
                WaitBackgroundWorker.ReportProgress(0);

                int count = 0;
                foreach (string sqlFile in sqlFiles)
                {
                    double percent = ++count / (double)sqlFiles.Count;
                    WaitBackgroundWorker.ReportProgress((int)Math.Floor(percent * 100));
                    
                    MergeFilesModel model = new MergeFilesModel
                    {
                        FileName = sqlFile
                    };
                    TSql140Parser parser = new TSql140Parser(true);
                    using (StreamReader sreader = new StreamReader(sqlFile))
                    {
                        string sql = sreader.ReadToEnd();
                        TSqlFragment result = parser.Parse(new StringReader(sql), out var parseErrors);
                        model.ParseErrors = parseErrors.Select(p => new ParseFileError(p, sqlFile)).ToList();
                        int otherNo=0;
                        if (result is TSqlScript sqlScript)
                        {
                            for (int i = 0; i < sqlScript.Batches.Count; i++)
                            {
                                TSqlBatch sqlBatch = sqlScript.Batches[i];
                                for (var index = 0; index < sqlBatch.Statements.Count; index++)
                                {
                                    TSqlStatement sqlStatement = sqlBatch.Statements[index];
                                    switch (sqlStatement)
                                    {
                                        case CreateProcedureStatement procedureStatement:
                                            model.NodeType = SqlTreeNodeType.CreateProcedureStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value)
                                                ? procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value;
                                            break;
                                        case AlterProcedureStatement procedureStatement:
                                            model.NodeType = SqlTreeNodeType.AlterProcedureStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value)
                                                ? procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value;
                                            break;
                                        case CreateFunctionStatement functionStatement:
                                            model.NodeType = SqlTreeNodeType.CreateFunctionStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier?.Value)
                                                ? functionStatement.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = functionStatement.Name.BaseIdentifier.Value;
                                            break;
                                        case AlterFunctionStatement functionStatement:
                                            model.NodeType = SqlTreeNodeType.AlterFunctionStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier?.Value)
                                                ? functionStatement.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = functionStatement.Name.BaseIdentifier.Value;
                                            break;
                                        case CreateTriggerStatement triggerStatement:
                                            model.NodeType = SqlTreeNodeType.CreateTriggerStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(triggerStatement.Name.SchemaIdentifier?.Value)
                                                ? triggerStatement.Name.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = triggerStatement.Name.BaseIdentifier.Value;
                                            break;
                                        case CreateViewStatement viewStatement:
                                            model.NodeType = SqlTreeNodeType.CreateViewStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? viewStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = viewStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case CreateTableStatement tableStatement:
                                            model.NodeType = SqlTreeNodeType.CreateTableStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? tableStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = tableStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case AlterTableStatement tableStatement:
                                            model.NodeType = SqlTreeNodeType.AlterTableStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? tableStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = tableStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case AlterViewStatement viewStatement:
                                            model.NodeType = SqlTreeNodeType.AlterViewStatement;
                                            model.SchemaName = !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value)
                                                ? viewStatement.SchemaObjectName.SchemaIdentifier.Value
                                                : Settings.Default.DefaultSchemaName;
                                            model.Name = viewStatement.SchemaObjectName.BaseIdentifier.Value;
                                            break;
                                        case UpdateStatement updateStatement:
                                        {
                                            StringBuilder fn =
                                                new StringBuilder(Settings.Default.UpdateLiteral ?? "Update");
                                            string schema = Settings.Default.DefaultSchemaName;
                                            if (updateStatement.UpdateSpecification.Target is NamedTableReference
                                                tableReference)
                                            {
                                                if (tableReference.SchemaObject.SchemaIdentifier != null)
                                                {
                                                    schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                                }

                                                if (Settings.Default.PrependSchemaName)
                                                {
                                                    fn.Append(Settings.Default.SchemaSeparator);
                                                    fn.Append(schema);
                                                }

                                                fn.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                            }

                                            model.NodeType = SqlTreeNodeType.UpdateStatement;
                                            model.SchemaName = schema;
                                            model.Name = fn.ToString();
                                            break;
                                        }
                                        case InsertStatement insertStatement:
                                        {
                                            StringBuilder fn =
                                                new StringBuilder(Settings.Default.InsertLiteral ?? "insert");
                                            fn.Append(insertStatement.InsertSpecification.InsertOption.ToString());
                                            string schema = Settings.Default.DefaultSchemaName;
                                            if (insertStatement.InsertSpecification.Target is NamedTableReference
                                                tableReference)
                                            {
                                                if (tableReference.SchemaObject.SchemaIdentifier != null)
                                                {
                                                    schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                                }

                                                if (Settings.Default.PrependSchemaName)
                                                {
                                                    fn.Append(Settings.Default.SchemaSeparator);
                                                    fn.Append(schema);
                                                }

                                                fn.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                            }

                                            model.NodeType = SqlTreeNodeType.InsertStatement;
                                            model.SchemaName = schema;
                                            model.Name = fn.ToString();
                                            break;
                                        }
                                        case ExecuteStatement executeStatement:
                                        {
                                            StringBuilder fn = new StringBuilder(Settings.Default.ExecuteLiteral ?? "Execute");
                                            string schema = Settings.Default.DefaultSchemaName;
                                            if (executeStatement.ExecuteSpecification.ExecutableEntity is ExecutableProcedureReference procedureReference)
                                            {
                                                if (procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier != null)
                                                {
                                                    schema = procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier.Value;
                                                }

                                                if (Settings.Default.PrependSchemaName)
                                                {
                                                    fn.Append(Settings.Default.SchemaSeparator);
                                                    fn.Append(schema);
                                                }

                                                fn.Append(procedureReference.ProcedureReference.ProcedureReference.Name.BaseIdentifier.Value);
                                            }

                                            model.NodeType = SqlTreeNodeType.ExecuteStatement;
                                            model.SchemaName = schema;
                                            model.Name = fn.ToString();
                                            break;

                                        }
                                        case PredicateSetStatement _:
                                            break;
                                        default:
                                            if (index < sqlBatch.Statements.Count - 1 || i < sqlScript.Batches.Count -1 ) continue;

                                            if (index < sqlBatch.Statements.Count - 1) break;
                                            model.NodeType = SqlTreeNodeType.OtherStatement;
                                            model.SchemaName = Settings.Default.DefaultSchemaName;
                                            model.Name = Settings.Default.OtherLiteral ?? "other" + otherNo++;
                                            break;
                                    }
                                }
                            }

                            if (sqlScript.Batches.Count > 0 && sqlScript.Batches[sqlScript.Batches.Count-1].Statements.Count>0)
                            {
                                int lastToken = sqlScript.Batches[sqlScript.Batches.Count - 1]
                                    .Statements[sqlScript.Batches[sqlScript.Batches.Count - 1].Statements.Count - 1].LastTokenIndex;
                                model.ContainsGo = false;
                                while (lastToken < sqlScript.ScriptTokenStream.Count)
                                {
                                    if (sqlScript.ScriptTokenStream[lastToken].TokenType == TSqlTokenType.Go)
                                    {
                                        model.ContainsGo = true;
                                        break;
                                    }

                                    ++lastToken;
                                }
                            }
                        }
                    }

                    Model.Add(model);
                }
            }
            catch (Exception exc)
            {
                Model = new List<MergeFilesModel>
                {
                    new MergeFilesModel
                    {
                        FileName = _inputDirectory,
                        ParseErrors =
                            new List<ParseFileError> {new ParseFileError(new ParseError(0, 0, 0, 0, exc.Message), null)}
                    }
                };
            }
        }

    }
}
