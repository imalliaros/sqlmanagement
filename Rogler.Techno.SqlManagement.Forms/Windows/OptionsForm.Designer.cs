﻿namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsForm));
            this.ButtonsPanel = new System.Windows.Forms.Panel();
            this.OptionsPropertyEditor = new Syncfusion.Windows.Forms.Diagram.Controls.PropertyEditor(this.components);
            this.OkButton = new System.Windows.Forms.Button();
            this.OptionsCancelButton = new System.Windows.Forms.Button();
            this.ButtonsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonsPanel
            // 
            this.ButtonsPanel.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonsPanel.Controls.Add(this.OptionsCancelButton);
            this.ButtonsPanel.Controls.Add(this.OkButton);
            this.ButtonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonsPanel.Location = new System.Drawing.Point(0, 375);
            this.ButtonsPanel.Name = "ButtonsPanel";
            this.ButtonsPanel.Size = new System.Drawing.Size(800, 75);
            this.ButtonsPanel.TabIndex = 0;
            // 
            // OptionsPropertyEditor
            // 
            this.OptionsPropertyEditor.Diagram = null;
            this.OptionsPropertyEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionsPropertyEditor.Location = new System.Drawing.Point(0, 0);
            this.OptionsPropertyEditor.Name = "OptionsPropertyEditor";
            this.OptionsPropertyEditor.Size = new System.Drawing.Size(800, 375);
            this.OptionsPropertyEditor.TabIndex = 1;
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(582, 25);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(100, 23);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // OptionsCancelButton
            // 
            this.OptionsCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionsCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.OptionsCancelButton.Location = new System.Drawing.Point(688, 25);
            this.OptionsCancelButton.Name = "OptionsCancelButton";
            this.OptionsCancelButton.Size = new System.Drawing.Size(100, 23);
            this.OptionsCancelButton.TabIndex = 2;
            this.OptionsCancelButton.Text = "Cancel";
            this.OptionsCancelButton.UseVisualStyleBackColor = true;
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.OptionsCancelButton;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.OptionsPropertyEditor);
            this.Controls.Add(this.ButtonsPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OptionsForm";
            this.Text = "OptionsForm";
            this.ButtonsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ButtonsPanel;
        private Syncfusion.Windows.Forms.Diagram.Controls.PropertyEditor OptionsPropertyEditor;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button OptionsCancelButton;
    }
}