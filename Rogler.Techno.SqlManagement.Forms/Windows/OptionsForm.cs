﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class OptionsForm : Form
    {
        public OptionsForm()
        {
            InitializeComponent();
            OptionsPropertyEditor.PropertyGrid.SelectedObject = new OptionsModel();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Settings.Default.Save();
            Close();
        }
    }
}
