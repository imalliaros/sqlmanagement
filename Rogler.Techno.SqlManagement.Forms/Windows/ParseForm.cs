﻿using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Forms.Common;
using Rogler.Techno.SqlManagement.Forms.Models;
using Rogler.Techno.SqlManagement.Forms.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Rogler.Techno.SqlManagement.Forms.Extensions;
using Rogler.Techno.SqlManagement.Forms.Models.Db;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class ParseForm : Rogler.Techno.SqlManagement.Forms.Windows.WaitForm
    {

        public IList<ParseError> ParseErrors;
        private readonly string _inputFile, _outputDirectory;
        private readonly string _text;
        public List<MergeFilesModel> Output { get; set; } = new List<MergeFilesModel>();
        public SqlTreeNode Root { get; set; } = null;
        public Dictionary<string, DbObjectDescription> ObjectsDescription { get; set; } = new Dictionary<string, DbObjectDescription>();

        public List<ResultModel> Result
        {
            get
            {
                List<ResultModel> list = new List<ResultModel>();
                foreach (var descriptionValue in ObjectsDescription.Values)
                {
                    ResultModel model = new ResultModel
                    {
                        Name = descriptionValue.Name
                    };
                    if (!descriptionValue.Details.Any()) continue;
                    foreach (FileDescriptionModel detail in descriptionValue.Details)
                    {
                        if (detail.Create) model.Create = true;
                        if (detail.Alter) model.Alter = true;
                        if (detail.Drop) model.Drop = true;
                    }
                    list.Add(model);
                }

                return list;
            }
        }

        public ParseForm(string inputFile, string outputDirectory, string text)
        {
            InitializeComponent();
            _inputFile = inputFile;
            _outputDirectory = outputDirectory;
            _text = text;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorkerOnDoWork;
        }

        private void WaitBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                TSql140Parser parser = new TSql140Parser(true);
                StringBuilder setBuilder = new StringBuilder();
                StringBuilder otherBuilder = new StringBuilder();

                string sql = _text;
                TSqlFragment result = parser.Parse(new StringReader(sql), out ParseErrors);
                if (WaitBackgroundWorker.CancellationPending)
                {
                    e.Cancel = true;
                    e.Result = DialogResult.Cancel;
                    return;
                }

                if (Settings.Default.MarkMultipleStatementsAsError && result is TSqlScript sqlCheck)
                {
                    foreach (TSqlBatch sqlBatch in sqlCheck.Batches)
                    {
                        List<TSqlStatement> statements = new List<TSqlStatement>();
                        foreach (TSqlStatement statement in sqlBatch.Statements)
                        {
                            if (statement is CreateProcedureStatement || statement is CreateFunctionStatement)
                                statements.Add(statement);
                        }

                        for (int i = 1; i < statements.Count; ++i)
                        {

                            TSqlStatement statement = sqlBatch.Statements[i];
                            ParseErrors.Add(new ParseError(Constants.MultipleStatementsError, statement.StartOffset,
                                statement.StartLine, statement.StartColumn, Resources.MultipleStatementsError));
                        }
                    }
                }

                if (ParseErrors.Count > 0)
                {
                    e.Result = DialogResult.Abort;
                    return;
                }


                if (result is TSqlScript sqlScript)
                {
                    WaitBackgroundWorker.ReportProgress(0);
                    int currentBath = 0;

                    Root = new SqlTreeNode
                    {
                        Name = _inputFile,
                        TreeType = SqlTreeNodeType.File,
                        Script = sql
                    };

                    foreach (TSqlBatch sqlBatch in sqlScript.Batches)
                    {
                        if (WaitBackgroundWorker.CancellationPending)
                        {
                            e.Cancel = true;
                            e.Result = DialogResult.Cancel;
                            return;
                        }


                        SqlTreeNode batchNode = new SqlTreeNode
                        {
                            Name = "Batch",
                            TreeType = SqlTreeNodeType.Batch,
                            Script = Globals.GetScript(sql, result)
                        };
                        Root.Children.Add(batchNode);

                        for (int index = 0; index < sqlBatch.Statements.Count; index++)
                        {
                            TSqlStatement sqlStatement = sqlBatch.Statements[index];
                            string filename = null;
                            StringBuilder statementName = new StringBuilder();

                            switch (sqlStatement)
                            {
                                case PredicateSetStatement setStatement:
                                    {
                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        int length = -1;
                                        if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                            length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                        string content = length != -1
                                            ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                            : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                        setBuilder.Append(content);
                                        setBuilder.AppendLine();
                                        setBuilder.AppendLine("GO");
                                        statementName = new StringBuilder(Settings.Default.SetLiteral);
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = Settings.Default.SetLiteral + " " + Globals.ToTitleCaseFromPascal(setStatement.Options.ToString()) + "=" + (setStatement.IsOn ? "ON" : "OFF"),
                                            TreeType = SqlTreeNodeType.SetStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }
                                case InsertStatement insertStatement:
                                    {
                                        string schema = Settings.Default.DefaultSchemaName;
                                        if (insertStatement.InsertSpecification.Target is NamedTableReference tableReference)
                                        {
                                            if (tableReference.SchemaObject.SchemaIdentifier != null)
                                            {
                                                schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                            }

                                            if (Settings.Default.PrependSchemaName)
                                            {
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                                statementName.Append(schema);
                                            }

                                            statementName.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                            
                                            filename =  statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                                ? Settings.Default.InsertLiteral + insertStatement.InsertSpecification.InsertOption.ToString() + statementName.ToString()
                                                : Settings.Default.InsertLiteral;
                                        }
                                        else
                                        {
                                            statementName.Append(Settings.Default.InsertLiteral);
                                            filename = statementName.ToString();
                                        }
                                        
                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = statementName.ToString(),
                                            SchemaName = schema,
                                            NodeType = SqlTreeNodeType.InsertStatement,
                                            Column = insertStatement.StartColumn,
                                            Line = insertStatement.StartLine,
                                            Offset = insertStatement.StartOffset,
                                            FileName = filename.ToString(),
                                            OriginalFileName = _inputFile
                                        });

                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = Settings.Default.InsertLiteral + " (" + statementName.ToString() + ")",
                                            TreeType = SqlTreeNodeType.InsertStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }

                                        break;
                                    }

                                case CreateProcedureStatement procedureStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value))
                                        {
                                            statementName.Append(procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        statementName.Append(procedureStatement.ProcedureReference.Name.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.CreateLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value,
                                            SchemaName = procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            NodeType = SqlTreeNodeType.CreateProcedureStatement,
                                            Column = procedureStatement.StartColumn,
                                            Line = procedureStatement.StartLine,
                                            Offset = procedureStatement.StartOffset,
                                            FileName = filename,
                                            OriginalFileName = _inputFile
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Create Procedure (" + procedureStatement.ProcedureReference.Name.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.CreateProcedureStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });

                                        break;
                                    }
                                case AlterProcedureStatement procedureStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value))
                                        {
                                            statementName.Append(procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        if (!string.IsNullOrEmpty(Settings.Default.AlterLiteral)) statementName.Append(Settings.Default.AlterLiteral);
                                        statementName.Append(procedureStatement.ProcedureReference.Name.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.AlterLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));


                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value,
                                            SchemaName = procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            NodeType = SqlTreeNodeType.AlterProcedureStatement,
                                            Column = procedureStatement.StartColumn,
                                            Line = procedureStatement.StartLine,
                                            Offset = procedureStatement.StartOffset,
                                            FileName = filename,
                                            OriginalFileName = _inputFile
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Alter Procedure (" + procedureStatement.ProcedureReference.Name.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.AlterProcedureStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }

                                case CreateFunctionStatement functionStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier.Value))
                                        {
                                            statementName.Append(functionStatement.Name.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        statementName.Append(functionStatement.Name.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.CreateLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = functionStatement.Name.BaseIdentifier.Value,
                                            SchemaName = functionStatement.Name.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            FileName = filename,
                                            NodeType = SqlTreeNodeType.CreateFunctionStatement,
                                            Column = functionStatement.StartColumn,
                                            Line = functionStatement.StartLine,
                                            Offset = functionStatement.StartOffset,
                                            OriginalFileName = _inputFile
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }


                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Create Function (" + functionStatement.Name.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.CreateFunctionStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }
                                case AlterFunctionStatement functionStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier.Value))
                                        {
                                            statementName.Append(functionStatement.Name.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        if (!string.IsNullOrEmpty(Settings.Default.AlterLiteral)) statementName.Append(Settings.Default.AlterLiteral);

                                        statementName.Append(functionStatement.Name.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.AlterLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = functionStatement.Name.BaseIdentifier.Value,
                                            SchemaName = functionStatement.Name.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            FileName = filename,
                                            NodeType = SqlTreeNodeType.AlterFunctionStatement,
                                            Column = functionStatement.StartColumn,
                                            Line = functionStatement.StartLine,
                                            Offset = functionStatement.StartOffset,
                                            OriginalFileName = _inputFile
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Alter Function (" + functionStatement.Name.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.AlterFunctionStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }

                                case CreateTriggerStatement triggerStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(triggerStatement.Name.SchemaIdentifier.Value))
                                        {
                                            statementName.Append(triggerStatement.Name.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        statementName.Append(triggerStatement.Name.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.CreateLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = triggerStatement.Name.BaseIdentifier.Value,
                                            SchemaName = triggerStatement.Name.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            FileName = filename,
                                            NodeType = SqlTreeNodeType.CreateTriggerStatement,
                                            Column = triggerStatement.StartColumn,
                                            Line = triggerStatement.StartLine,
                                            Offset = triggerStatement.StartOffset,
                                            OriginalFileName = _inputFile
                                        });


                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Create Trigger (" + triggerStatement.Name.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.CreateTriggerStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }
                                case CreateViewStatement viewStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value))
                                        {
                                            statementName.Append(viewStatement.SchemaObjectName.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        statementName.Append(viewStatement.SchemaObjectName.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.CreateLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = viewStatement.SchemaObjectName.BaseIdentifier.Value,
                                            SchemaName = viewStatement.SchemaObjectName.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            NodeType = SqlTreeNodeType.CreateViewStatement,
                                            Column = viewStatement.StartColumn,
                                            Line = viewStatement.StartLine,
                                            Offset = viewStatement.StartOffset,
                                            FileName = filename,
                                            OriginalFileName = _inputFile
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Create View (" + viewStatement.SchemaObjectName.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.CreateViewStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }

                                case AlterViewStatement viewStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(viewStatement.SchemaObjectName.SchemaIdentifier?.Value))
                                        {
                                            statementName.Append(viewStatement.SchemaObjectName.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        if (!string.IsNullOrEmpty(Settings.Default.AlterLiteral)) statementName.Append(Settings.Default.AlterLiteral);

                                        statementName.Append(viewStatement.SchemaObjectName.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.AlterLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = viewStatement.SchemaObjectName.BaseIdentifier.Value,
                                            SchemaName = viewStatement.SchemaObjectName.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            NodeType = SqlTreeNodeType.AlterViewStatement,
                                            Column = viewStatement.StartColumn,
                                            Line = viewStatement.StartLine,
                                            Offset = viewStatement.StartOffset,
                                            FileName = filename,
                                            OriginalFileName = _inputFile
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Alter View (" + viewStatement.SchemaObjectName.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.AlterViewStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }

                                case CreateTableStatement tableStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value))
                                        {
                                            statementName.Append(tableStatement.SchemaObjectName.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        statementName.Append(tableStatement.SchemaObjectName.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.CreateLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));


                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = tableStatement.SchemaObjectName.BaseIdentifier.Value,
                                            SchemaName = tableStatement.SchemaObjectName.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            NodeType = SqlTreeNodeType.CreateTableStatement,
                                            Column = tableStatement.StartColumn,
                                            Line = tableStatement.StartLine,
                                            Offset = tableStatement.StartOffset,
                                            FileName = Path.Combine(_outputDirectory, filename),
                                            OriginalFileName = _inputFile
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }
                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Create Table (" + tableStatement.SchemaObjectName.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.CreateTableStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }

                                case AlterTableStatement tableStatement:
                                    {
                                        if (Settings.Default.PrependSchemaName == true &&
                                            !string.IsNullOrEmpty(tableStatement.SchemaObjectName.SchemaIdentifier?.Value))
                                        {
                                            statementName.Append(tableStatement.SchemaObjectName.SchemaIdentifier.Value);
                                            if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                        }

                                        if (!string.IsNullOrEmpty(Settings.Default.AlterLiteral)) statementName.Append(Settings.Default.AlterLiteral);
                                        statementName.Append(tableStatement.SchemaObjectName.BaseIdentifier.Value);

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.AlterLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));


                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = tableStatement.SchemaObjectName.BaseIdentifier.Value,
                                            SchemaName = tableStatement.SchemaObjectName.SchemaIdentifier?.Value ?? Settings.Default.DefaultSchemaName,
                                            NodeType = SqlTreeNodeType.AlterTableStatement,
                                            Column = tableStatement.StartColumn,
                                            Line = tableStatement.StartLine,
                                            Offset = tableStatement.StartOffset,
                                            FileName = filename,
                                            OriginalFileName = _inputFile
                                        });


                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }

                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }
                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Alter Table (" + tableStatement.SchemaObjectName.BaseIdentifier.Value + ")",
                                            TreeType = SqlTreeNodeType.AlterTableStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });
                                        break;
                                    }
                                case ExecuteStatement executeStatement:
                                    {
                                        string schema = Settings.Default.DefaultSchemaName;
                                        if (executeStatement.ExecuteSpecification.ExecutableEntity is ExecutableProcedureReference procedureReference)
                                        {
                                            if (procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier != null)
                                            {
                                                schema = procedureReference.ProcedureReference.ProcedureReference.Name.SchemaIdentifier.Value;
                                            }

                                            if (Settings.Default.PrependSchemaName)
                                            {
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                                statementName.Append(schema);
                                            }

                                            statementName.Append(procedureReference.ProcedureReference.ProcedureReference.Name.BaseIdentifier.Value);

                                            filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                                ? statementName.ToString()
                                                : Settings.Default.ExecuteLiteral;
                                        }
                                        else
                                        {
                                            statementName.Append(Settings.Default.ExecuteLiteral);
                                            filename = statementName.ToString();
                                        }
                                        

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));


                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = statementName.ToString(),
                                            SchemaName = schema,
                                            NodeType = SqlTreeNodeType.ExecuteStatement,
                                            Column = executeStatement.StartColumn,
                                            Line = executeStatement.StartLine,
                                            Offset = executeStatement.StartOffset,
                                            FileName = filename,
                                            OriginalFileName = _inputFile
                                        });

                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Execute (" + statementName.ToString() + ")",
                                            TreeType = SqlTreeNodeType.ExecuteStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });


                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile =
                                            new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset -
                                                         result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }

                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }

                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }

                                        break;
                                    }
                                case UpdateStatement updateStatement:
                                    {
                                        string schema = Settings.Default.DefaultSchemaName;
                                        if (updateStatement.UpdateSpecification.Target is NamedTableReference tableReference)
                                        {
                                            if (tableReference.SchemaObject.SchemaIdentifier != null)
                                            {
                                                schema = tableReference.SchemaObject.SchemaIdentifier.Value;
                                            }

                                            if (Settings.Default.PrependSchemaName)
                                            {
                                                statementName.Append(Settings.Default.SchemaSeparator);
                                                statementName.Append(schema);
                                            }

                                            statementName.Append(tableReference.SchemaObject.BaseIdentifier.Value);
                                            filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                                ? statementName.ToString()
                                                : Settings.Default.UpdateLiteral;
                                        }
                                        else
                                        {
                                            statementName.Append(Settings.Default.UpdateLiteral);
                                            filename = statementName.ToString();
                                        }
                                        
                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));
                                        
                                        Output.Add(new MergeFilesModel
                                        {
                                            Name = statementName.ToString(),
                                            SchemaName = schema,
                                            NodeType = SqlTreeNodeType.UpdateStatement,
                                            Column = updateStatement.StartColumn,
                                            Line = updateStatement.StartLine,
                                            Offset = updateStatement.StartOffset,
                                            FileName = filename,
                                            OriginalFileName = _inputFile
                                        });

                                        batchNode.Children.Add(new SqlTreeNode
                                        {
                                            Name = "Update (" + statementName.ToString() + ")",
                                            TreeType = SqlTreeNodeType.UpdateStatement,
                                            Script = Globals.GetScript(sql, sqlStatement)
                                        });

                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                        {
                                            int length = -1;
                                            if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                                length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                            string content = length != -1
                                                ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                                : sql.Substring(result.ScriptTokenStream[firstToken].Offset);
                                            if (setBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                setBuilder.Clear();
                                            }
                                            if (otherBuilder.Length > 0)
                                            {
                                                outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                otherBuilder.Clear();
                                            }

                                            outputFile.Write(Globals.ConvertToNewLine(content));
                                            outputFile.WriteLine();
                                            outputFile.WriteLine("GO");
                                        }

                                        break;
                                    }

                                default:
                                    {
                                        statementName.Append(Settings.Default.OtherLiteral ?? "Other");

                                        filename = statementName.ToString().IndexOfAny(Path.GetInvalidFileNameChars()) == -1
                                            ? statementName.ToString()
                                            : Settings.Default.CreateLiteral;

                                        filename = Globals.MakeUnique(Path.Combine(_outputDirectory, filename + ".sql"));

                                        if (index == sqlBatch.Statements.Count - 1)
                                        {


                                            Output.Add(new MergeFilesModel
                                            {
                                                Name = statementName.ToString(),
                                                NodeType = SqlTreeNodeType.OtherStatement,
                                                Column = sqlStatement.StartColumn,
                                                Line = sqlStatement.StartLine,
                                                Offset = sqlStatement.StartOffset,
                                                FileName = filename,
                                                OriginalFileName = _inputFile,
                                            });
                                            batchNode.Children.Add(new SqlTreeNode
                                            {
                                                Name = "Other (" + statementName.ToString() + ")",
                                                TreeType = SqlTreeNodeType.OtherStatement,
                                                Script = Globals.GetScript(sql, sqlStatement)
                                            });
                                        }



                                        int firstToken = sqlStatement.FirstTokenIndex;
                                        while (firstToken > 0)
                                        {
                                            --firstToken;
                                            if (result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.MultilineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.SingleLineComment &&
                                                result.ScriptTokenStream[firstToken].TokenType != TSqlTokenType.WhiteSpace)
                                            {
                                                if (firstToken > 0) firstToken++;
                                                break;
                                            }
                                        }

                                        int length = -1;
                                        if (result.ScriptTokenStream.Count > sqlStatement.LastTokenIndex)
                                            length = result.ScriptTokenStream[sqlStatement.LastTokenIndex + 1].Offset - result.ScriptTokenStream[firstToken].Offset;
                                        string content = length != -1
                                            ? sql.Substring(result.ScriptTokenStream[firstToken].Offset, length)
                                            : sql.Substring(result.ScriptTokenStream[firstToken].Offset);

                                        if (index == sqlBatch.Statements.Count - 1)
                                        {
                                            using (StreamWriter outputFile = new StreamWriter(filename, false, Encoding.UTF8))
                                            {
                                                if (setBuilder.Length > 0)
                                                {
                                                    outputFile.Write(Globals.ConvertToNewLine(setBuilder.ToString()));
                                                    setBuilder.Clear();
                                                }
                                                if (otherBuilder.Length > 0)
                                                {
                                                    outputFile.Write(Globals.ConvertToNewLine(otherBuilder.ToString()));
                                                    otherBuilder.Clear();
                                                }

                                                outputFile.Write(Globals.ConvertToNewLine(content));
                                                outputFile.WriteLine();
                                                outputFile.WriteLine("GO");
                                            }
                                        }
                                        else
                                        {
                                            otherBuilder.Append(content);
                                            otherBuilder.AppendLine();
                                        }


                                        break;
                                    }
                            }
                        }

                        double percent = ++currentBath / (double)sqlScript.Batches.Count;
                        WaitBackgroundWorker.ReportProgress((int)Math.Floor(percent * 100));

                    }

                    if (Output.Count > 0)
                    {
                        SerializeOutput(Path.Combine(_outputDirectory, Settings.Default.IndexFile));
                        CreateResult();
                    }
                }

            }
            catch (Exception exc)
            {
                ParseErrors.Add(new ParseError(0, 0, 0, 0, exc.Message));
                e.Result = DialogResult.Abort;
            }

            e.Result = DialogResult.OK;
        }

        private void SerializeOutput(string filename)
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<MergeFilesModel>));
            using (TextWriter writer = new StreamWriter(filename))
            {
                ser.Serialize(writer, Output);
            }
        }

        private void CreateResult()
        {
            foreach (MergeFilesModel file in Output)
            {
                if (!ObjectsDescription.ContainsKey(file.Name))
                {
                    ObjectsDescription.Add(file.Name, new DbObjectDescription
                    {
                        Name = file.Name
                    });
                }

                DbObjectDescription dbObject = ObjectsDescription[file.Name];
                if (dbObject != null)
                {
                    FileDescriptionModel fdmodel = new FileDescriptionModel
                    {
                        FileName = file.FileName
                    };
                    dbObject.ObjectType = file.NodeType.GetObjectType();
                    switch (file.NodeType)
                    {
                        case SqlTreeNodeType.CreateProcedureStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateFunctionStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateTableStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateTriggerStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.CreateViewStatement:
                            fdmodel.Create = true;
                            break;
                        case SqlTreeNodeType.AlterProcedureStatement:
                            fdmodel.Alter = true;
                            break;
                        case SqlTreeNodeType.AlterFunctionStatement:
                            fdmodel.Alter = true;
                            break;
                        case SqlTreeNodeType.AlterTableStatement:
                            fdmodel.Alter = true;
                            break;
                        case SqlTreeNodeType.AlterViewStatement:
                            fdmodel.Alter = true;
                            break;
                        default:                            
                            continue;
                    }
                   
                    if(fdmodel.Alter || fdmodel.Create || fdmodel.Drop) dbObject.Details.Add(fdmodel);
                }
            }
        }
    }
}
