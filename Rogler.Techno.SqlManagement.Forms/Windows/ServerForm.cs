﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Rogler.Techno.SqlManagement.Forms.Common;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class ServerForm : Rogler.Techno.SqlManagement.Forms.Windows.WaitForm
    {
        public  Operations Operations { get; }

        public ServerForm(Operations operations)
        {
            InitializeComponent();
            Operations = operations;
            WaitBackgroundWorker.DoWork += WaitBackgroundWorker_DoWork;
        }

        private void WaitBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Operations.LoadSchema();
                DialogResult = DialogResult.OK;
            }
            catch
            {
                DialogResult = DialogResult.Abort;
            }
        }
    }
}
