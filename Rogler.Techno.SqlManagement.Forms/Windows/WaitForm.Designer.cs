﻿namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    partial class WaitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WaitProgressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.CancelTaskButton = new System.Windows.Forms.Button();
            this.WaitBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // WaitProgressBar
            // 
            this.WaitProgressBar.Location = new System.Drawing.Point(12, 32);
            this.WaitProgressBar.Name = "WaitProgressBar";
            this.WaitProgressBar.Size = new System.Drawing.Size(677, 23);
            this.WaitProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.WaitProgressBar.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please wait for the operation to complete";
            // 
            // CancelTaskButton
            // 
            this.CancelTaskButton.Location = new System.Drawing.Point(281, 61);
            this.CancelTaskButton.Name = "CancelTaskButton";
            this.CancelTaskButton.Size = new System.Drawing.Size(75, 23);
            this.CancelTaskButton.TabIndex = 2;
            this.CancelTaskButton.Text = "Cancel";
            this.CancelTaskButton.UseVisualStyleBackColor = true;
            this.CancelTaskButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // WaitBackgroundWorker
            // 
            this.WaitBackgroundWorker.WorkerReportsProgress = true;
            this.WaitBackgroundWorker.WorkerSupportsCancellation = true;
            this.WaitBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.WaitBackgroundWorker_ProgressChanged);
            this.WaitBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WaitBackgroundWorker_RunWorkerCompleted);
            // 
            // WaitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 92);
            this.ControlBox = false;
            this.Controls.Add(this.CancelTaskButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.WaitProgressBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "WaitForm";
            this.ShowInTaskbar = false;
            this.Text = "Please Wait...";
            this.Shown += new System.EventHandler(this.WaitForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar WaitProgressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button CancelTaskButton;
        protected System.ComponentModel.BackgroundWorker WaitBackgroundWorker;
    }
}