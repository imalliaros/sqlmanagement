﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rogler.Techno.SqlManagement.Forms.Windows
{
    public partial class WaitForm : Form
    {
        public bool HasCanceled = false;
        public string ErrorMessage;

        public WaitForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            WaitBackgroundWorker.CancelAsync();
            CancelTaskButton.Enabled = false;
        }

        private void WaitBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled) DialogResult = DialogResult.Cancel;
            else if (e.Result != null && e.Result is DialogResult dr) DialogResult = dr;
            else DialogResult = DialogResult.Ignore;
            Close();
        }

        private void WaitBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (WaitProgressBar.Style == ProgressBarStyle.Marquee) WaitProgressBar.Style = ProgressBarStyle.Continuous;
            WaitProgressBar.Value = e.ProgressPercentage;
        }

        private void WaitForm_Shown(object sender, EventArgs e)
        {
            WaitBackgroundWorker.RunWorkerAsync();
        }
    }
}
