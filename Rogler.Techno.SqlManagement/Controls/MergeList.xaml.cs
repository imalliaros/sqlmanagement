﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Rogler.Techno.SqlManagement.ViewModels;

namespace Rogler.Techno.SqlManagement.Controls
{
    /// <summary>
    /// Interaction logic for MergeList.xaml
    /// </summary>
    public partial class MergeList : UserControl
    {
        public MergeList(MergeFilesViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
