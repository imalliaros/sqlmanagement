﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using Rogler.Techno.SqlManagement.ViewModels;
using Rogler.Techno.SqlManagement.Windows;
using Syncfusion.Windows.Tools.Controls;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;
using UserControl = System.Windows.Controls.UserControl;

namespace Rogler.Techno.SqlManagement.Controls
{
    /// <summary>
    /// Interaction logic for SqlEditor.xaml
    /// </summary>
    public partial class SqlEditor : UserControl
    {
        public bool _hasChanges = false;
        public SqlEditor()
        {
            InitializeComponent();
        }

        public void Load()
        {
            SyntaxEditor.LoadFile();
            _hasChanges = false;
            DocumentContainer.SetHeader(this, System.IO.Path.GetFileName(SyntaxEditor.DocumentSource));
            DocumentContainer.SetTabCaptionToolTip(this, SyntaxEditor.DocumentSource);
            SyntaxEditor.TextChanged += (o, args) =>
            {
                DocumentContainer.SetHeader(this, "*" + System.IO.Path.GetFileName(SyntaxEditor.DocumentSource));
            };
        }

        public void Save()
        {
            SyntaxEditor.SaveFile(SyntaxEditor.DocumentSource);
            DocumentContainer.SetHeader(this, System.IO.Path.GetFileName(SyntaxEditor.DocumentSource));
            _hasChanges = false;
        }

        public void SaveAs()
        {
            SyntaxEditor.SaveFile();
            DocumentContainer.SetHeader(this, System.IO.Path.GetFileName(SyntaxEditor.DocumentSource));
            DocumentContainer.SetTabCaptionToolTip(this, SyntaxEditor.DocumentSource);
            _hasChanges = false;
        }

        public void Split()
        {
            if (_hasChanges)
            {
                if (MessageBox.Show((string)Application.Current.FindResource("SaveFileBeforeSplit"),
                        (string)Application.Current.FindResource("Save"), MessageBoxButton.YesNo,
                        MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Save();
                }
                else
                {
                    return;
                }
            }
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog
            {
                Description = (string)Application.Current.FindResource("OutputDirectory")
            };
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                ParseViewModel parseViewModel = new ParseViewModel(SyntaxEditor.DocumentSource, folderBrowser.SelectedPath);
                BusyWindow parserWindow = new BusyWindow(parseViewModel);
                parserWindow.ShowDialog();
                if (parseViewModel.DialogResult == DialogResult.OK)
                {
                    OutputTab.Visibility = Visibility.Visible;
                    OutputTab.SelectedItem = OutputTabItem;
                    ErrorList.ItemsSource = null;
                    OutputList.ItemsSource = parseViewModel.Output;
                }
                else
                {
                    OutputTab.Visibility = Visibility.Visible;
                    OutputTab.SelectedItem = ErrorsTabItem;
                    ErrorList.ItemsSource = parseViewModel.ParseErrors;
                    OutputList.ItemsSource = null;
                }

            }
        }

        public void Check()
        {
            if (_hasChanges)
            {
                if (MessageBox.Show((string)Application.Current.FindResource("SaveFileBeforeSplit"),
                        (string)Application.Current.FindResource("Save"), MessageBoxButton.YesNo,
                        MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Save();
                }
                else
                {
                    return;
                }
            }
            CheckViewModel viewModel = new CheckViewModel(SyntaxEditor.DocumentSource);
            BusyWindow parserWindow = new BusyWindow(viewModel);
            parserWindow.ShowDialog();
            if (viewModel.DialogResult == DialogResult.OK)
            {
                OutputTab.Visibility = Visibility.Collapsed;
                MessageBox.Show((string) Application.Current.FindResource("NoErrors"), (string) Application.Current.FindResource("CheckTooltipTitle"), MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            else
            {
                OutputTab.Visibility = Visibility.Visible;
                OutputTab.SelectedItem = ErrorsTabItem;
                ErrorList.ItemsSource = viewModel.ParseErrors;
                OutputList.ItemsSource = null;
            }
        }

        private void SyntaxEditor_OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!_hasChanges)
            {
                DocumentContainer.SetHeader(this, "*" + System.IO.Path.GetFileName(SyntaxEditor.DocumentSource));
                _hasChanges = true;
            }
        }
    }
}
