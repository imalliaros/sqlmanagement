﻿namespace Rogler.Techno.SqlManagement.Models
{
    public enum DocumentTypeEnum
    {
        None,
        SqlParser,
        MergeList
    }
}