﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Annotations;

namespace Rogler.Techno.SqlManagement.Models
{
    public class MergeFilesModel : INotifyPropertyChanged
    {
        private string _name;
        private OutputRecordEnum _outputType;
        private string _fileName;
        private ObservableCollection<ParseError> _parseErrors;
        private string _schemaName;

        public ObservableCollection<ParseError> ParseErrors
        {
            get => _parseErrors;
            set
            {
                if (Equals(value, _parseErrors)) return;
                _parseErrors = value;
                OnPropertyChanged();
            }
        }

        public string SchemaName
        {
            get => _schemaName;
            set
            {
                if (value == _schemaName) return;
                _schemaName = value;
                OnPropertyChanged();
            }
        }

        public string Name      
        {
            get => _name;
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        
        public OutputRecordEnum OutputType
        {
            get => _outputType;
            set
            {
                if (value == _outputType) return;
                _outputType = value;
                OnPropertyChanged();
            }
        }

        public string FileName
        {
            get => _fileName;
            set
            {
                if (value == _fileName) return;
                _fileName = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}