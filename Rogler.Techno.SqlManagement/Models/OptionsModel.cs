﻿using Rogler.Techno.SqlManagement.Properties;
using System.ComponentModel;

namespace Rogler.Techno.SqlManagement.Models
{
    public class OptionsModel
    {

        [DisplayName("Prepend Schema Name")]
        [Category("Split")]
        [Description("If true prepends schema name to the function name.")]
        public bool PrependSchemaName {
            get => Settings.Default.PrependSchemaName;
            set => Settings.Default.PrependSchemaName = value;
        }

        [DisplayName("Schema Separator")]
        [Category("Split")]
        [Description("The schema and function name separator.")]
        public string SchemaSeparator
        {
            get => Settings.Default.SchemaSeparator;
            set => Settings.Default.SchemaSeparator = value;
        }

        [DisplayName("Create File Name")]
        [Category("Merge")]
        [Description("The name of the file with the create statements.")]
        public string CreateFile
        {
            get => Settings.Default.CreateFile;
            set => Settings.Default.CreateFile = value;
        }

        [DisplayName("Delete file Name")]
        [Category("Merge")]
        [Description("The name of the file with the delete statements.")]
        public string DeleteFile
        {
            get => Settings.Default.DeleteFile;
            set => Settings.Default.DeleteFile = value;
        }

        [DisplayName("Generate If Exists statements")]
        [Category("Merge")]
        [Description("Generate If Exists statements when generating the Delete File")]
        public bool GenerateIfExistsStatements
        {
            get => Settings.Default.GenerateIfExistsStatements;
            set => Settings.Default.GenerateIfExistsStatements = value;
        }

        [DisplayName("Default Schema Name")]
        [Category("Merge")]
        [Description("Default schema name for create and delete statements")]
        public string DefaultSchemaName
        {
            get => Settings.Default.DefaultSchemaName;
            set => Settings.Default.DefaultSchemaName = value;
        }
    }
}