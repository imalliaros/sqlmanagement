﻿namespace Rogler.Techno.SqlManagement.Models
{
    public class OutputRecord
    {
        public string Name { get; set; }
        public OutputRecordEnum OutputType { get; set; }
        public string FileName { get; set; }
    }
}