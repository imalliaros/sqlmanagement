﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Models;

namespace Rogler.Techno.SqlManagement.ViewModels
{
    public class CheckViewModel : JobViewModel
    {
        private readonly string _inputFile;

        private IList<ParseError> _parseErrors;
        public IList<ParseError> ParseErrors
        {
            get => _parseErrors;
            set => _parseErrors = value;
        }

        public CheckViewModel(string inputfile)
        {
            _inputFile = inputfile;
        }

        public override async Task<int> DoJob(TaskScheduler uiScheduler, CancellationTokenSource cancellationToken)
        {
            CancellationToken = cancellationToken;
            try
            {
                TSql140Parser parser = new TSql140Parser(true);
                using (StreamReader sreader = new StreamReader(_inputFile))
                {
                    string sql = await sreader.ReadToEndAsync();
                    TSqlFragment result = parser.Parse(new StringReader(sql), out _parseErrors);
                    if (_parseErrors.Count > 0)
                    {
                        DialogResult = DialogResult.Abort;

                        await Task.Factory.StartNew(() => { CloseAction?.Invoke(); }, System.Threading.CancellationToken.None, TaskCreationOptions.None, uiScheduler);
                        return _parseErrors.Count;
                    }

                    DialogResult = DialogResult.OK;
                    await Task.Factory.StartNew(() => { CloseAction?.Invoke(); }, System.Threading.CancellationToken.None, TaskCreationOptions.None, uiScheduler);
                    return 0;
                }
            }
            catch (Exception exc)
            {
                ParseErrors.Add(new ParseError(0, 0, 0, 0, exc.Message));
                DialogResult = DialogResult.Ignore;
                await Task.Factory.StartNew(() =>
                {
                    CloseAction?.Invoke();
                }, System.Threading.CancellationToken.None, TaskCreationOptions.None, uiScheduler);
                return 1;
            }

        }

    }
}