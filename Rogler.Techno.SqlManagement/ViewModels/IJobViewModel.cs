﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Syncfusion.Windows.Shared;

namespace Rogler.Techno.SqlManagement.ViewModels
{
    public interface IJobViewModel : INotifyPropertyChanged
    {
        IProgress<double> ProgressOperation { get; set; }
        bool IsIndeterminate { get; set; }
        double ProgressBarValue { get; set; }
        double ProgressBarMaxValue { get; set; }
        CancellationTokenSource CancellationToken { get; set; }
        ICommand CancelCommand { get;  }
        Action CloseAction { get; set; }

        Task<int> DoJob(TaskScheduler uiScheduler, CancellationTokenSource cancellationToken);

        DialogResult DialogResult { get; set; }
    }
}