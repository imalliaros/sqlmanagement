﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using JetBrains.Annotations;
using Syncfusion.Windows.Shared;

namespace Rogler.Techno.SqlManagement.ViewModels
{
    public abstract class JobViewModel : IJobViewModel
    {
        public bool IsIndeterminate { get; set; } = true;
        public double ProgressBarValue { get; set; }
        public IProgress<double> ProgressOperation { get; set; }

        public double ProgressBarMaxValue
        {
            get => _progressBarMaxValue;
            set
            {
                if (value.Equals(_progressBarMaxValue)) return;
                _progressBarMaxValue = value;
                OnPropertyChanged();
            }
        }

        public CancellationTokenSource CancellationToken { get; set; }

        private DelegateCommand _closeCommand;
        private double _progressBarMaxValue;

        public ICommand CancelCommand
        {
            get
            {
                return _closeCommand ?? (_closeCommand = new DelegateCommand((o) =>
                {
                    CancellationToken.Cancel();
                    CloseAction?.Invoke();
                }, (b) => true));
            }
        }

        public Action CloseAction { get; set; }

        public abstract Task<int> DoJob(TaskScheduler uiScheduler, CancellationTokenSource cancellationToken);
        public DialogResult DialogResult { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}