﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Rogler.Techno.SqlManagement.Models;

namespace Rogler.Techno.SqlManagement.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private bool _showSaveButtons = false;
        private DocumentTypeEnum _documentType = DocumentTypeEnum.None;
        private bool _showCreateButton;
        private bool _showSelectButton = false;

        public bool ShowSaveButtons
        {
            get => _showSaveButtons;
            set
            {
                if (value == _showSaveButtons) return;
                _showSaveButtons = value;
                OnPropertyChanged();
            }
        }

        public bool ShowCreateButton
        {
            get => _showCreateButton;
            set
            {
                if (value == _showCreateButton) return;
                _showCreateButton = value;
                OnPropertyChanged();
            }
        }

        public bool ShowSelectButton
        {
            get => _showSelectButton;
            set
            {
                if (value == _showSelectButton) return;
                _showSelectButton = value;
                OnPropertyChanged();
            }
        }

        public DocumentTypeEnum DocumentType
        {
            get => _documentType;
            set
            {
                if (value == _documentType) return;
                _documentType = value;
                ShowSaveButtons = DocumentType == DocumentTypeEnum.SqlParser;
                ShowCreateButton = DocumentType == DocumentTypeEnum.MergeList;
                ShowSelectButton = DocumentType == DocumentTypeEnum.MergeList;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}