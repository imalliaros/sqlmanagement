﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Models;
using Rogler.Techno.SqlManagement.Properties;

namespace Rogler.Techno.SqlManagement.ViewModels
{
    public class MergeFilesViewModel : JobViewModel
    {
        private readonly string _outputDirectory;
        private ObservableCollection<MergeFilesModel> _model = new ObservableCollection<MergeFilesModel>();

        public ObservableCollection<MergeFilesModel> Model 
        {
            get => _model;
            set
            {
                if (Equals(value, _model)) return;
                _model = value;
                OnPropertyChanged();
            }
        } 

        public MergeFilesViewModel(string outputDirectory)
        {
            _outputDirectory = outputDirectory;
            ProgressOperation = new Progress<double>(value =>
            {
                if (ProgressBarValue <= 0.1) IsIndeterminate = false;
                ProgressBarValue = value;
            });

        }
        public override async Task<int> DoJob(TaskScheduler uiScheduler, CancellationTokenSource cancellationToken)
        {
            CancellationToken = cancellationToken;

            try
            {
                List<string> sqlFiles =
                    Directory.EnumerateFiles(_outputDirectory, "*.sql", SearchOption.TopDirectoryOnly).ToList();
                ProgressBarMaxValue = sqlFiles.Count();
                ProgressOperation.Report(0);
                int count = 0;
                foreach (string sqlFile in sqlFiles)
                {
                    ProgressOperation.Report(count++);
                    MergeFilesModel model = new MergeFilesModel
                    {
                        FileName = sqlFile
                    };
                    TSql140Parser parser = new TSql140Parser(true);
                    using (StreamReader sreader = new StreamReader(sqlFile))
                    {
                        string sql = await sreader.ReadToEndAsync();
                        TSqlFragment result = parser.Parse(new StringReader(sql), out var parseErrors);
                        model.ParseErrors = new ObservableCollection<ParseError>(parseErrors);
                        //if (parseErrors.Count > 0) continue;
                        if (result is TSqlScript sqlScript)
                        {
                            foreach (TSqlBatch sqlBatch in sqlScript.Batches)
                            {
                                foreach (TSqlStatement sqlStatement in sqlBatch.Statements)
                                {
                                    if (sqlStatement is CreateProcedureStatement procedureStatement)
                                    {
                                        model.OutputType = OutputRecordEnum.Procedure;
                                        if (!string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name
                                            .SchemaIdentifier?.Value))
                                        {
                                            model.SchemaName = procedureStatement.ProcedureReference.Name
                                                .SchemaIdentifier.Value;
                                        }
                                        else model.SchemaName = Settings.Default.DefaultSchemaName;
                                        model.Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value;
                                        break;
                                    }
                                    else if (sqlStatement is CreateFunctionStatement functionStatement)
                                    {
                                        model.OutputType = OutputRecordEnum.Function;
                                        if (!string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier?.Value))
                                        {
                                            model.SchemaName = functionStatement.Name.SchemaIdentifier.Value;
                                        }
                                        else model.SchemaName = Settings.Default.DefaultSchemaName;
                                        model.Name = functionStatement.Name.BaseIdentifier.Value;
                                        break;
                                    }

                                }

                                if (!string.IsNullOrEmpty(model.Name)) break;
                            }
                        }
                    }

                    Model.Add(model);
                }

                return Model.Count;
            }
            catch (Exception exc)
            {
                _model = new ObservableCollection<MergeFilesModel>();
                List<ParseError> errors = new List<ParseError>();
                errors.Add(new ParseError(0, 0, 0, 0, exc.Message));
                _model.Add(new MergeFilesModel
                {
                    FileName = _outputDirectory,
                    ParseErrors = new ObservableCollection<ParseError>(errors)
                });
                return 0;
            }
            finally
            {
                await Task.Factory.StartNew(() =>
                {
                    CloseAction?.Invoke();
                }, System.Threading.CancellationToken.None, TaskCreationOptions.None, uiScheduler);
            }
           
        }
    }
}