﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Models;
using Rogler.Techno.SqlManagement.Properties;
using Syncfusion.Windows.Shared;

namespace Rogler.Techno.SqlManagement.ViewModels
{
    class ParseViewModel : JobViewModel
    {
        private readonly string _inputFile, _outputDirectory;

        private IList<ParseError> _parseErrors;
        public IList<ParseError> ParseErrors
        {
            get => _parseErrors;
            set => _parseErrors = value;
        }
        public List<OutputRecord> Output { get; set; } = new List<OutputRecord>();


        public ParseViewModel(string inputFile, string outputDirectory)
        {
            _inputFile = inputFile;
            _outputDirectory = outputDirectory;
            ProgressOperation = new Progress<double>(value =>
            {
                if (ProgressBarValue >= 0.1) IsIndeterminate = false;
                ProgressBarValue = value;
            });
        }


        public override async Task<int> DoJob(TaskScheduler uiScheduler, CancellationTokenSource cancellationToken)
        {
            CancellationToken = cancellationToken;
            try
            {
                TSql140Parser parser = new TSql140Parser(true);
                int lastFilenameIndex = 0, lastBatchIndex = 0;
                using (StreamReader sreader = new StreamReader(_inputFile))
                {
                    string sql = await sreader.ReadToEndAsync();
                    TSqlFragment result = parser.Parse(new StringReader(sql), out _parseErrors);
                    if (_parseErrors.Count > 0)
                    {
                        DialogResult = DialogResult.Abort;

                        await Task.Factory.StartNew(() =>
                        {
                            CloseAction?.Invoke();
                        }, System.Threading.CancellationToken.None, TaskCreationOptions.None, uiScheduler);
                        return _parseErrors.Count;
                    }

                    if (result is TSqlScript sqlScript)
                    {
                        ProgressBarMaxValue = sqlScript.Batches.Count;
                        ProgressOperation.Report(0);
                        IsIndeterminate = false;

                        foreach (TSqlBatch sqlBatch in sqlScript.Batches)
                        {
                            foreach (TSqlStatement sqlStatement in sqlBatch.Statements)
                            {
                                StringBuilder filename = new StringBuilder();
                                if (sqlStatement is CreateProcedureStatement procedureStatement)
                                {
                                    if (Settings.Default.PrependSchemaName == true &&
                                        !string.IsNullOrEmpty(procedureStatement.ProcedureReference.Name.SchemaIdentifier?.Value))
                                    {
                                        filename.Append(procedureStatement.ProcedureReference.Name.SchemaIdentifier.Value);
                                        if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                            filename.Append(Settings.Default.SchemaSeparator);
                                    }

                                    filename.Append(procedureStatement.ProcedureReference.Name.BaseIdentifier.Value);
                                    filename.Append(".sql");
                                    string fn = filename.ToString();
                                    if (fn.IndexOfAny(Path.GetInvalidFileNameChars()) != -1)
                                        fn = "script" + (lastFilenameIndex++) + ".sql";

                                    Output.Add(new OutputRecord
                                    {
                                        Name = procedureStatement.ProcedureReference.Name.BaseIdentifier.Value,
                                        FileName = fn,
                                        OutputType = OutputRecordEnum.Procedure
                                    });

                                    fn = Path.Combine(_outputDirectory, fn);
                                    int lastToken = sqlBatch.LastTokenIndex;
                                    while (result.ScriptTokenStream[lastToken].TokenType != TSqlTokenType.Go)
                                    {
                                        if (lastToken < result.ScriptTokenStream.Count) ++lastToken;
                                    }

                                    using (StreamWriter outputFile = new StreamWriter(fn))
                                    {
                                        int length = result.LastTokenIndex == lastToken
                                            ? -1
                                            : result.ScriptTokenStream[lastToken + 1].Offset;
                                        string strfile = length == 1
                                            ? sql.Substring(lastBatchIndex)
                                            : sql.Substring(lastBatchIndex, length - lastBatchIndex);
                                        if (length != -1) lastBatchIndex = length;
                                        outputFile.Write(strfile);
                                    }
                                    ProgressOperation.Report(++ProgressBarValue);

                                    break;
                                }

                                if (sqlStatement is CreateFunctionStatement functionStatement)
                                {
                                    if (Settings.Default.PrependSchemaName == true &&
                                        !string.IsNullOrEmpty(functionStatement.Name.SchemaIdentifier.Value))
                                    {
                                        filename.Append(functionStatement.Name.SchemaIdentifier.Value);
                                        if (!string.IsNullOrEmpty(Settings.Default.SchemaSeparator))
                                            filename.Append(Settings.Default.SchemaSeparator);
                                    }

                                    filename.Append(functionStatement.Name.BaseIdentifier.Value);
                                    filename.Append(".sql");
                                    string fn = filename.ToString();
                                    if (fn.IndexOfAny(Path.GetInvalidFileNameChars()) != -1)
                                        fn = "script" + (lastFilenameIndex++) + ".sql";

                                    Output.Add(new OutputRecord
                                    {
                                        Name = functionStatement.Name.BaseIdentifier.Value,
                                        FileName = fn,
                                        OutputType = OutputRecordEnum.Function
                                    });

                                    fn = Path.Combine(_outputDirectory, fn);
                                    int lastToken = sqlBatch.LastTokenIndex;
                                    while (result.ScriptTokenStream[lastToken].TokenType != TSqlTokenType.Go)
                                    {
                                        if (lastToken < result.ScriptTokenStream.Count) ++lastToken;
                                    }

                                    using (StreamWriter outputFile = new StreamWriter(fn))
                                    {
                                        int length = result.LastTokenIndex == lastToken
                                            ? -1
                                            : result.ScriptTokenStream[lastToken + 1].Offset;
                                        string strfile = length == 1
                                            ? sql.Substring(lastBatchIndex)
                                            : sql.Substring(lastBatchIndex, length - lastBatchIndex);
                                        if (length != -1) lastBatchIndex = length;
                                        outputFile.Write(strfile);
                                    }
                                    ProgressOperation.Report(++ProgressBarValue);
                                    break;

                                }
                                ProgressOperation.Report(++ProgressBarValue);
                            }
                        }
                    }
                }

                DialogResult = DialogResult.OK;
                await Task.Factory.StartNew(() =>
                {
                    CloseAction?.Invoke();
                }, System.Threading.CancellationToken.None, TaskCreationOptions.None, uiScheduler);
                return 0;

            }
            catch (Exception exc)
            {
                ParseErrors.Add(new ParseError(0, 0, 0, 0, exc.Message));
                DialogResult = DialogResult.Ignore;
                await Task.Factory.StartNew(() =>
                {
                    CloseAction?.Invoke();
                }, System.Threading.CancellationToken.None, TaskCreationOptions.None, uiScheduler);
                return 1;
            }

        }



    }
}