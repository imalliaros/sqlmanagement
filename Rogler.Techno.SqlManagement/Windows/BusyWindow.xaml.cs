﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using Rogler.Techno.SqlManagement.Models;
using Rogler.Techno.SqlManagement.ViewModels;

namespace Rogler.Techno.SqlManagement.Windows
{

    public partial class BusyWindow : Window
    {

        public BusyWindow(IJobViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            if (viewModel.CloseAction == null)
                viewModel.CloseAction = Close;
            TaskScheduler uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            Task.Factory.StartNew(() => viewModel.DoJob(uiScheduler, new CancellationTokenSource()));
        }
    }
}
