#region Copyright Syncfusion Inc. 2001-2018.
// Copyright Syncfusion Inc. 2001-2018. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using Microsoft.Win32;
using Rogler.Techno.SqlManagement.Controls;
using Rogler.Techno.SqlManagement.Models;
using Rogler.Techno.SqlManagement.Properties;
using Rogler.Techno.SqlManagement.ViewModels;
using Syncfusion.SfSkinManager;
using Syncfusion.Windows.Controls.RichTextBoxAdv;
using Syncfusion.Windows.Edit;
using Syncfusion.Windows.Shared;
using Syncfusion.Windows.Tools.Controls;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace Rogler.Techno.SqlManagement.Windows
{
    /// <summary>
    /// Interaction logic for WordStyleWindow.xaml
    /// </summary>
    public partial class MainWindow
    {

        public MainWindow()
        {
            InitializeComponent();
            MainViewModel viewModel = new MainViewModel();
            DataContext = viewModel;
        }

        private void ExitButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OptionsButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainRibbon.HideBackStage();
            OptionsWindow options = new OptionsWindow
            {
                OptionsPropertyGrid =
                {
                    SelectedObject = new OptionsModel()
                }
            };
            options.ShowDialog();
        }

        private void LoadButton_OnClick(object sender, RoutedEventArgs e)
        {
            SqlEditor editor = new SqlEditor();
            DocContainer.Items.Add(editor);
            editor.Load();
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DocContainer.ActiveDocument is SqlEditor edit)
            {
                edit.Save();
            }
        }

        private void SaveAsButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DocContainer.ActiveDocument is SqlEditor edit)
            {
                edit.SaveAs();
            }
        }

        private void SaveAllButton_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var docContainerItem in DocContainer.Items)
            {
                if(docContainerItem is SqlEditor edit)
                {
                    edit.Save();
                }
            }
        }

        private void SplitButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DocContainer.ActiveDocument is SqlEditor edit)
            {
                edit.Split();
            }
        }

        private void MergeButton_OnClick(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog
            {
                Description = (string)Application.Current.FindResource("OutputDirectory")
            };
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                MergeFilesViewModel viewModel = new MergeFilesViewModel(folderBrowser.SelectedPath);
                BusyWindow busyWindow = new BusyWindow(viewModel);
                busyWindow.ShowDialog();

                MergeList list = new MergeList(viewModel);
                DocContainer.Items.Add(list);
                DocumentContainer.SetHeader(list, new DirectoryInfo(folderBrowser.SelectedPath).Name);
                DocumentContainer.SetTabCaptionToolTip(list, folderBrowser.SelectedPath);
                ImageBrush brush = new ImageBrush();
                BitmapImage bi = new BitmapImage();
                StreamResourceInfo sri = Application.GetResourceStream( new Uri("Resources/Images/Redo16.png", UriKind.RelativeOrAbsolute));
                bi.BeginInit();
                bi.StreamSource = sri.Stream;
                bi.EndInit();
                brush.ImageSource = bi;
                DocumentContainer.SetIcon(list, brush);
            }

        }

        private void DocContainer_OnActiveDocumentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MainViewModel viewmodel = DataContext as MainViewModel;
            if (viewmodel == null) return;

            switch (DocContainer.ActiveDocument)
            {
                case null:
                    viewmodel.DocumentType = DocumentTypeEnum.None;
                    break;
                case SqlEditor _:
                    viewmodel.DocumentType = DocumentTypeEnum.SqlParser;
                    break;
                case MergeList _:
                    viewmodel.DocumentType = DocumentTypeEnum.MergeList;
                    break;
            }
        }

        private void DocContainer_OnDocumentClosing(object sender, CancelingRoutedEventArgs e)
        {
            if (!(DataContext is MainViewModel viewmodel)) return;
            viewmodel.DocumentType = DocumentTypeEnum.None;
        }

        private void CreateButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DocContainer.ActiveDocument is MergeList mlist)
            {
                FolderBrowserDialog folderBrowser = new FolderBrowserDialog
                {
                    Description = (string)Application.Current.FindResource("OutputDirectory")
                };
                if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (mlist.MergeFilesGrid.SelectedItems.Any())
                    {
                        StringBuilder createSql = new StringBuilder();
                        StringBuilder deleteSql = new StringBuilder();

                        foreach (var s in mlist.MergeFilesGrid.SelectedItems)
                        {
                            if ((s is MergeFilesModel m) && m.ParseErrors.Any())
                            {
                                string msg = (string)Application.Current.FindResource("CannotMergeIfErrors") ??
                                             string.Empty;
                                MessageBox.Show(
                                    string.Format(msg, m.FileName), (string)Application.Current.FindResource("ParsingErrors"),
                                    MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                        }
                        if (!string.IsNullOrEmpty(Settings.Default.DeleteFile))
                        {
                            foreach (var s in mlist.MergeFilesGrid.SelectedItems)
                            {
                                if (s is MergeFilesModel m)
                                {
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine(
                                            $"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[{m.SchemaName}].[{m.Name}]'))");
                                        deleteSql.AppendLine("BEGIN");
                                    }

                                    switch (m.OutputType)
                                    {
                                        case OutputRecordEnum.Function:
                                            deleteSql.AppendLine($"DROP FUNCTION [{m.SchemaName}].[{m.Name}]");
                                            break;
                                        case OutputRecordEnum.Procedure:
                                            deleteSql.AppendLine($"DROP PROCEDURE [{m.SchemaName}].[{m.Name}]");
                                            break;
                                    }
                                    if (Settings.Default.GenerateIfExistsStatements)
                                    {
                                        deleteSql.AppendLine("END");
                                    }

                                    deleteSql.AppendLine("GO");
                                }
                            }
                        }

                        foreach (var s in mlist.MergeFilesGrid.SelectedItems)
                        {
                            if (s is MergeFilesModel m)
                            {
                                using (var sr = new StreamReader(m.FileName))
                                {
                                    createSql.AppendLine(sr.ReadToEnd());
                                }
                                createSql.AppendLine("GO");

                            }
                        }

                        if (deleteSql.Length > 0)
                        {
                            using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(folderBrowser.SelectedPath, Settings.Default.DeleteFile)))
                            {
                                outputFile.Write(deleteSql.ToString());
                            }
                        }
                        using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(folderBrowser.SelectedPath, Properties.Settings.Default.CreateFile)))
                        {
                            outputFile.Write(createSql.ToString());
                        }
                    }

                }
            }
        }

        private void SelectAllButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DocContainer.ActiveDocument is MergeList m)
            {
                m.MergeFilesGrid.SelectAll();
            }
        }

        private void ClearAllButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DocContainer.ActiveDocument is MergeList m)
            {
                m.MergeFilesGrid.ClearSelections(false);
            }
        }

        private void CheckButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (DocContainer.ActiveDocument is SqlEditor edit)
            {
                edit.Check();
            }
        }
    }
}
